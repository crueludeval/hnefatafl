/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#include <assert.h>	/* assert */

#include "uix.h"
#include "uixconsole.h"	/* hnef_uix_console_* */
#include "uixfont.h"	/* hnef_uix_font_* */
#include "uixtheme.h"	/* hnef_uix_theme_* */
#include "uixutil.h"	/* hnef_uix_* */

/*@observer@*/
/*@unchecked@*/
static
const char
	* const HNEF_UIX_WM_DELETE_WINDOW	= "WM_DELETE_WINDOW",
	* const HNEF_UIX_WM_TAKE_FOCUS		= "WM_TAKE_FOCUS";

static
HNEF_BOOL
hnef_alloc_uix_xic_init (
/*@in@*/
/*@notnull@*/
	struct hnef_uix	* const HNEF_RSTR uix,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx
	)
/*@modifies * uix, * frx@*/
{
	unsigned short	i;
	XVaNestedList	args;
	XIMStyles	* style_im	= NULL;
	XIMStyle	style_client	= 0,
			style		= 0;

	assert	(NULL != uix);
	assert	(NULL != uix->display);
	assert	(NULL != uix->font);
	assert	(uix->font->use_fontset);
	assert	(NULL != uix->font->fontset);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	/*
	 * NOTE:	This gives "still reachable" leaked bytes in
	 *		Valgrind derived from a call to
	 *		`XStringToKeysym()`. `XCloseIM()` is called.
	 */
/*@i@*/	uix->xim	= XOpenIM(uix->display, NULL, NULL, NULL);
	if	(NULL == uix->xim)
	{
		* frx	= HNEF_FRX_FAIL_IM_OPEN;
		return	HNEF_FALSE;
	}

/*@i@*/	if	(NULL != XGetIMValues
/*@i@*/		(uix->xim, XNQueryInputStyle, & style_im, NULL))
	{
		* frx	= HNEF_FRX_FAIL_IM_VALUES;
		return	HNEF_FALSE;
	}
	assert	(NULL != style_im);

	style_client	= (unsigned long)XIMPreeditArea
			| (unsigned long)XIMPreeditNone
			| (unsigned long)XIMPreeditNothing
			| (unsigned long)XIMPreeditPosition
			| (unsigned long)XIMStatusArea
			| (unsigned long)XIMStatusNone
			| (unsigned long)XIMStatusNothing;

	for	(i = 0; i < style_im->count_styles; ++i)
	{
		XIMStyle style_tmp	= style_im->supported_styles[i];
		if	((style_tmp & style_client) == style_tmp)
		{
			style	= style_tmp;
		}
	}

/*@i@*/	XFree	(style_im);
	if	(0 == style)
	{
		* frx	= HNEF_FRX_FAIL_IM_STYLES;
		return	HNEF_FALSE;
	}

/*@i@*/	args		= XVaCreateNestedList(0,
				XNFontSet,	uix->font->fontset,
				XNForeground,	uix->px_white,
				XNBackground,	uix->px_black,	NULL);
/*@i@*/	uix->xic	= XCreateIC(uix->xim,
			XNInputStyle,		style,
			XNClientWindow,		uix->window,
			XNFocusWindow,		uix->window,
			XNStatusAttributes,	args,
			XNPreeditAttributes,	args,		NULL);

/*@i@*/	XFree	(args);
	if	(NULL == uix->xic)
	{
		* frx	= HNEF_FRX_FAIL_IC_CREATE;
/*@i@*/		return	HNEF_FALSE;
	}

	/*
	 * Note that `XSetICFocus()` is never necessary for this program
	 * since the window is the only component and it's always
	 * focused.
	 */

/*@i@*/	return	HNEF_TRUE;
}

/*
 * Return `NULL` on any failure. If the failure is an XLib failure, then
 * `frx` is set to some failure value. If the failure is an allocation
 * failure, then `frx` indicates success but `NULL` is returned.
 */
struct hnef_uix *
hnef_alloc_uix_init (
	const struct gleip_lang	* const GLEIP_RSTR lang,
	enum HNEF_FRX		* const HNEF_RSTR frx,
	const char		* const HNEF_RSTR setlocale_return
	)
{
	struct hnef_uix	* HNEF_RSTR uix	= NULL;
	char		nred[]		= "#ff0000",
			nflag_blue[]	= "#005293",
			nflag_yellow[]	= "#fecb00";

	assert	(NULL != lang);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	uix	= malloc(sizeof(* uix));
	if	(NULL == uix)
	{
		return	NULL;
	}

#ifdef	HNEFATAFL_UI_AIM
	uix->ai_force		= uix->ai_stop	= HNEF_FALSE;
	uix->progress		= 0;
	uix->progress_max	= 100;
	uix->progress_last	= uix->progress_max_last
				= HNEF_UIX_PROGRESS_INVALID;
#endif	/* HNEFATAFL_UI_AIM */
	uix->quit_xlib		= HNEF_FALSE;
	uix->lastmove_show	= HNEF_TRUE;
	uix->console		= NULL;
	uix->font		= NULL;
	uix->themes		= NULL;
	uix->display		= NULL;
	uix->window		= 0;
	uix->paintmap		= NULL;
	uix->paintmap_width	= uix->paintmap_height
				= HNEF_BOARDPOS_NONE;
	uix->window_width	= uix->window_height
				= uix->opt_offset_x
				= uix->opt_offset_y
				= HNEF_UIX_SIZE_UNINIT;
	uix->progress_height	= 2;
	uix->pos_select		= HNEF_BOARDPOS_NONE;
	uix->opt_lookupstring	= NULL;
	uix->opt_repaint_due	= HNEF_FALSE;

	/*
	 * The longest UTF-8 sequence requires 6 chars, so make room for
	 * 3 times that.
	 */
	uix->opt_lookupstring	= gleip_line_alloc_len((size_t)(6 * 3));
	if	(NULL == uix->opt_lookupstring)
	{
		free	(uix);
		return	NULL;
	}

	uix->console	= hnef_alloc_uix_console();
	if	(NULL == uix->console)
	{
		free	(uix->opt_lookupstring);
		free	(uix);
		return	NULL;
	}

	uix->themes	= hnef_alloc_uix_themes();
	if	(NULL == uix->themes)
	{
		hnef_free_uix_console	(uix->console);
		free			(uix->opt_lookupstring);
		free			(uix);
		return			NULL;
	}

/*@i@*/	uix->display	= XOpenDisplay(NULL);
	if	(NULL == uix->display)
	{
		* frx			= HNEF_FRX_FAIL_DISPLAY;
		hnef_free_uix_themes_fini(uix, uix->themes);
		hnef_free_uix_console	(uix->console);
		free			(uix->opt_lookupstring);
/*@i@*/		free			(uix);
		return			NULL;
	}

/*@i@*/	uix->screen	= XDefaultScreen(uix->display);
/*@i@*/	uix->px_white	= XWhitePixel	(uix->display, uix->screen);
/*@i@*/	uix->px_black	= XBlackPixel	(uix->display, uix->screen);
/*@i@*/	uix->window	= XCreateSimpleWindow(uix->display,
/*@i@*/			XDefaultRootWindow(uix->display), 0, 0,
			(unsigned int)1, (unsigned int)1, 0, 0, 0);

/*@i@*/	uix->gc		= XCreateGC
/*@i@*/			(uix->display, uix->window, 0L, NULL);
	/*
	 * There is no XLib documentation on whether `XCreateGC` can
	 * return `NULL`, so I assume that it's always `!NULL`.
	 */
	assert	(NULL != uix->gc);
	hnef_uix_gc_conf(uix, uix->gc);

	uix->font	= hnef_alloc_uix_font_init(lang, uix->display,
			uix->gc, setlocale_return, frx);
	if	(NULL == uix->font)
	{
		hnef_free_uix_themes_fini(uix, uix->themes);
/*@i@*/		(void)XFreeGC		(uix->display, uix->gc);
/*@i@*/		(void)XDestroyWindow	(uix->display, uix->window);
/*@i@*/		(void)XCloseDisplay	(uix->display);
		hnef_free_uix_console	(uix->console);
		free			(uix->opt_lookupstring);
/*@i@*/		free			(uix);
		return			NULL;
	}

	if	(uix->font->use_fontset)
	{
		if	(!hnef_alloc_uix_xic_init(uix, frx)
		||	!hnef_frx_good(* frx))
		{
			/*
			 * Maybe we could free the FontSet and fall back
			 * on Latin-1 FontStruct here, instead of
			 * returning a fatal error.
			 */
			hnef_free_uix_font_fini	(uix, uix->font);
			hnef_free_uix_themes_fini(uix, uix->themes);
/*@i@*/			(void)XFreeGC		(uix->display, uix->gc);
/*@i@*/			(void)XDestroyWindow(uix->display, uix->window);
/*@i@*/			(void)XCloseDisplay	(uix->display);
			hnef_free_uix_themes_fini(uix, uix->themes);
			hnef_free_uix_console	(uix->console);
			free			(uix->opt_lookupstring);
/*@i@*/			free			(uix);
			return			NULL;
		}
	}
	else
	{
		uix->xim	= 0;
		uix->xic	= NULL;
	}

/*@i@*/	uix->colormap	= XDefaultColormap(uix->display, uix->screen);

/*@i@*/	if (!XParseColor(uix->display, uix->colormap, nred,
						& uix->col_red)
/*@i@*/	|| !XParseColor(uix->display, uix->colormap, nflag_blue,
						& uix->col_flag_blue)
/*@i@*/	|| !XParseColor(uix->display, uix->colormap, nflag_yellow,
						& uix->col_flag_yellow)
/*@i@*/	|| !XAllocColor(uix->display, uix->colormap, & uix->col_red)
/*@i@*/	|| !XAllocColor(uix->display, uix->colormap,
						& uix->col_flag_blue)
/*@i@*/	|| !XAllocColor(uix->display, uix->colormap,
						& uix->col_flag_yellow))
	{
		* frx			= HNEF_FRX_FAIL_COLOR_CREATE;
		hnef_free_uix_font_fini	(uix, uix->font);
		hnef_free_uix_themes_fini(uix, uix->themes);
/*@i@*/		(void)XFreeGC		(uix->display, uix->gc);
/*@i@*/		(void)XDestroyWindow	(uix->display, uix->window);
/*@i@*/		(void)XCloseDisplay	(uix->display);
		hnef_free_uix_console	(uix->console);
		free			(uix->opt_lookupstring);
/*@i@*/		free			(uix);
		return			NULL;
	}

	{
		Atom	atoms[2];
		/*
		 * Should not return `None` atoms since we create them
		 * if they don't exist.
		 */
/*@i@*/		uix->window_del	= XInternAtom(uix->display,
					HNEF_UIX_WM_DELETE_WINDOW, 0);
/*@i@*/		uix->focus_take	= XInternAtom(uix->display,
					HNEF_UIX_WM_TAKE_FOCUS, 0);
		atoms[0]	= uix->window_del;
		atoms[1]	= uix->focus_take;
/*@i@*/		(void)XSetWMProtocols	(uix->display, uix->window,
					atoms, 2);
	}

	/*
	 * NOTE:	Undefined fields (will be defined by X later):
	 *		-	`event`
	 *		-	`window_atts`
	 */
	return	uix;
}

void
hnef_free_uix_fini (
	struct hnef_uix	* const HNEF_RSTR uix
	)
{
	assert	(NULL != uix);
	assert	(NULL != uix->display);
	assert	((	uix->font->use_fontset
		&&	NULL != uix->xic
		&&	NULL != uix->font->fontset
		&&	NULL == uix->font->fontstruct)
	||	(	!uix->font->use_fontset
		&&	NULL == uix->xic
		&&	NULL == uix->font->fontset
		&&	NULL != uix->font->fontstruct));

	if	(NULL != uix->themes)
	{
		hnef_free_uix_themes_fini(uix, uix->themes);
	}

	if	(NULL != uix->display)
	{
		/*
		 * This initialization could be prettier using C99, but
		 * we can't use `uix->col_*.pixel` as initializer
		 * elements in C89.
		 */
		unsigned long	pixelv[3];
		size_t		pixelc	= sizeof(pixelv)
					/ sizeof(* pixelv);
		pixelv[0]	= uix->col_red.pixel;
		pixelv[1]	= uix->col_flag_blue.pixel;
		pixelv[2]	= uix->col_flag_yellow.pixel;
/*@i@*/		XFreeColors(uix->display, uix->colormap,
				pixelv, (int)pixelc, 0);
	}

	if	(NULL != uix->xic)
	{
/*@i@*/		XDestroyIC	(uix->xic);
/*@i@*/		XCloseIM	(uix->xim);
	}

	if	(NULL != uix->font)
	{
		hnef_free_uix_font_fini	(uix, uix->font);
	}

	if	(NULL != uix->gc)
	{
/*@i@*/		(void)XFree		(uix->gc);
	}

	if	(NULL != uix->display)
	{
/*@i@*/		(void)XDestroyWindow	(uix->display, uix->window);
/*@i@*/		(void)XCloseDisplay	(uix->display);
	}

	if	(NULL != uix->paintmap)
	{
		free			(uix->paintmap);
		uix->paintmap		= NULL;
		uix->paintmap_width	= uix->paintmap_height
					= HNEF_BOARDPOS_NONE;
	}

	if	(NULL != uix->console)
	{
		hnef_free_uix_console	(uix->console);
	}

	if	(NULL != uix->opt_lookupstring)
	{
		free			(uix->opt_lookupstring);
	}

/*@i@*/	free(uix);
}

#endif	/* HNEFATAFL_UI_XLIB */

