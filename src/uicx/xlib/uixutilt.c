/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#include "uixutilt.h"

const int	HNEF_UIX_FLAG_UNITS_W	= 16,
		HNEF_UIX_FLAG_UNITS_H	= 10;

/*
 * Value indicating a dirty square (which should be repainted).
 *
 * This is a value that can never appear as a piece on the board. Every
 * piece bit on the board must be a single bit or 0. Here all bits are
 * set.
 */
const HNEF_BIT_U8	HNEF_BIT_U8_REPAINT = (HNEF_BIT_U8)
			((unsigned int)1	| (unsigned int)2
			| (unsigned int)4	| (unsigned int)8
			| (unsigned int)16	| (unsigned int)32
			| (unsigned int)64	| (unsigned int)128);

#endif	/* HNEFATAFL_UI_XLIB */

