/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#include <assert.h>	/* assert */
#include <string.h>	/* strlen */

#include "X11/xpm.h"	/* XpmReadFileToImage */

#include "gleipnir_line.h"	/* gleip_line_* */
#include "gleipnir_lineread.h"	/* gleip_lineread_* */

#include "filefind.h"	/* hnef_fopen_* */
#include "uixtheme.h"
#include "uixutil.h"	/* hnef_uix_gc_conf */

/*
 * Default memory capacity for `hnef_uix_themes->themev` and
 * `hnef_uix_theme->piecev/squarev`.
 */
/*@unchecked@*/
static
const size_t	HNEF_UIX_THEME_MEM_DEF	= (size_t)4,
		HNEF_UIX_IMG_MEM_DEF	= (size_t)4,
		HNEF_UIX_IMG_MEM_GROW	= (size_t)4;

/*@unchecked@*/
static
const double	HNEF_UIX_THEME_MEM_GROW	= 1.5;

/*@observer@*/
/*@unchecked@*/
static
const char	* const HNEF_UIX_DIR_DATA	= "xlib",
		* const HNEF_UIX_FILE_THEMES	= "themes";

/*
 * Filename identifiers.
 */
/*@unchecked@*/
static
const char	HNEF_UIX_XPM_PIECE		= 'p',
		HNEF_UIX_XPM_SQUARE		= 's';

/*
 * Returns the current theme or `NULL` if there is one, or if there are
 * no themes at all.
 */
struct hnef_uix_theme *
hnef_uix_theme_cur (
	struct hnef_uix_themes	* const HNEF_RSTR themes
	)
{
	assert	(NULL != themes);

	if	(SIZET_MAX == themes->themei_cur)
	{
		return	NULL;
	}

	assert	(themes->themei_cur < themes->themec);
	return	themes->themev[themes->themei_cur];
}

struct hnef_uix_img *
hnef_uix_img_get (
	struct hnef_uix_theme	* const HNEF_RSTR theme,
	const HNEF_BIT_U8	ui_bit,
	const HNEF_BOOL		square
	)
{
	struct hnef_uix_img	* * arrv	= NULL;
	size_t			arrc,
				i;

	assert	(NULL != theme);
	assert	(HNEF_BIT_U8_EMPTY != ui_bit);

	arrv	= square
		? theme->squarev
		: theme->piecev;
	arrc	= square
		? theme->squarec
		: theme->piecec;

	for	(i = 0; i < arrc; ++i)
	{
		struct hnef_uix_img * const HNEF_RSTR img = arrv[i];
		assert	(NULL != img);
		if	(ui_bit == img->ui_bit)
		{
			return	img;
		}
	}
	return			NULL;
}

/*
 * Upon a `NULL` return, `frx` is set if an XLib error occurs. Otherwise
 * (if `frx` doesn't indicate a failure) it's always a failure to
 * allocate.
 */
/*@in@*/
/*@null@*/
/*@only@*/
static
struct hnef_uix_img *
hnef_alloc_uix_img_init (
	struct hnef_uix		* const HNEF_RSTR uix,
	const HNEF_BIT_U8	ui_bit,
	const HNEF_BOOL		square,
/*@in@*/
/*@notnull@*/
	char			* const HNEF_RSTR datapath,
/*@in@*/
/*@notnull@*/
	enum HNEF_FRX		* const HNEF_RSTR frx
	)
/*@modifies * frx@*/
{
	struct hnef_uix_img	* HNEF_RSTR img	= NULL;
	XImage			* mask		= NULL;

	assert	(NULL != uix);
	assert	(NULL != uix->display);
	assert	(HNEF_BIT_U8_EMPTY != ui_bit);
	assert	(NULL != datapath);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;
	img	= malloc(sizeof(* img));
	if	(NULL == img)
	{
		return	NULL;
	}

	img->ui_bit	= ui_bit;
	img->square	= square;


/*@i@*/	if	(XpmReadFileToImage(uix->display, datapath,
/*@i@*/				& img->image, & mask, NULL)
/*@i@*/	||	NULL == img->image)
	{
		* frx	= HNEF_FRX_FAIL_XPM_READ;
		free	(img);
		return	NULL;
	}

	img->transparent	= NULL != mask;
	if	(img->transparent)
	{
		GC	gc	= NULL;

		assert	(NULL != mask);

		/*
		 * NOTE:	This call appears to leak memory since
		 *		the pixmap isn't freed. However, it's
		 *		freed by the X server after the program
		 *		returns, so this is definitely a false
		 *		positive (`XFreePixmap()` is called).
		 */
/*@i@*/		img->clipmask = XCreatePixmap(uix->display, uix->window,
			(unsigned int)mask->width,
			(unsigned int)mask->height,
			(unsigned int)mask->depth);

/*@i@*/		gc = XCreateGC(uix->display, img->clipmask, 0L, NULL);
		assert	(NULL != gc);
		hnef_uix_gc_conf(uix, gc);

		/*
		 * NOTE:	Valgrind detects conditional jumps which
		 *		depend on uninitialized values here for
		 *		some XPM files. I don't know what causes
		 *		this, but it is beyond my control, and
		 *		everything seems to work.
		 */
/*@i@*/		XPutImage(uix->display, img->clipmask, gc, mask,
			0, 0, 0, 0,
			(unsigned int)mask->width,
			(unsigned int)mask->height);

/*@i@*/		(void)XDestroyImage	(mask);
/*@i@*/		(void)XFreeGC		(uix->display, gc);
/*@i@*/	}

/*@i@*/	return	img;
}

static
void
hnef_free_uix_img_fini (
/*@in@*/
/*@notnull@*/
	const struct hnef_uix	* const HNEF_RSTR uix,
/*@in@*/
/*@notnull@*/
/*@owned@*/
/*@special@*/
	struct hnef_uix_img	* const HNEF_RSTR img
	)
/*@modifies img@*/
/*@releases img@*/
{
	assert	(NULL != uix);
	assert	(NULL != uix->display);
	assert	(NULL != img);
	assert	(NULL != img->image);

	if	(img->transparent)
	{
/*@i@*/		(void)XFreePixmap(uix->display, img->clipmask);
	}
/*@i@*/	(void)XDestroyImage(img->image);

/*@i@*/	free(img);
}

/*@in@*/
/*@null@*/
/*@only@*/
static
struct hnef_uix_theme *
hnef_alloc_uix_theme_init (
/*@in@*/
/*@notnull@*/
/*@unique@*/
	const char	* const HNEF_RSTR ui_theme,
/*@in@*/
/*@notnull@*/
/*@unique@*/
	const char	* const HNEF_RSTR datadir
	)
/*@modifies nothing@*/
{
	size_t			i;
	struct hnef_uix_theme	* HNEF_RSTR theme	= NULL;

	assert	(NULL != ui_theme);
	assert	(NULL != datadir);

	theme	= malloc(sizeof(* theme));
	if	(NULL == theme)
	{
		return	NULL;
	}

	assert	(HNEF_UIX_IMG_MEM_DEF > 0);

	theme->piecec		= theme->squarec	= 0;
	theme->piecec_mem	= theme->squarec_mem
					= HNEF_UIX_IMG_MEM_DEF;
	theme->square_width	= theme->square_height
					= HNEF_THEME_SIZE_INVALID;

	theme->piecev	= malloc(sizeof(* theme->piecev)
			* theme->piecec_mem);
	if	(NULL == theme->piecev)
	{
		free	(theme);
		return	NULL;
	}

	theme->squarev	= malloc(sizeof(* theme->squarev)
			* theme->squarec_mem);
	if	(NULL == theme->squarev)
	{
		free	(theme->piecev);
		free	(theme);
		return	NULL;
	}

	theme->ui_theme	= gleip_line_alloc_cpy(ui_theme);
	if	(NULL == theme->ui_theme)
	{
		free	(theme->squarev);
		free	(theme->piecev);
		free	(theme);
		return	NULL;
	}

	theme->datadir	= gleip_line_alloc_cpy(datadir);
	if	(NULL == theme->datadir)
	{
		free	(theme->ui_theme);
		free	(theme->squarev);
		free	(theme->piecev);
		free	(theme);
		return	NULL;
	}

	/*
	 * The images are loaded lazily as needed. That's not done here,
	 * so we just set them all to `NULL`
	 */
	for	(i = 0; i < theme->piecec_mem; ++i)
	{
		theme->piecev[i]	= NULL;
	}
	for	(i = 0; i < theme->squarec_mem; ++i)
	{
		theme->squarev[i]	= NULL;
	}

/* SPLint pointer to pointer */ /*@i1@*/\
	return	theme;
}

static
void
hnef_free_uix_theme_fini (
/*@in@*/
/*@notnull@*/
	const struct hnef_uix	* const HNEF_RSTR uix,
/*@in@*/
/*@notnull@*/
/*@owned@*/
/*@special@*/
	struct hnef_uix_theme	* const HNEF_RSTR theme
	)
/*@modifies theme@*/
/*@releases theme@*/
{
	size_t	i;

	assert	(NULL != uix);
	assert	(NULL != theme);
	assert	(NULL != theme->piecev);
	assert	(NULL != theme->squarev);

	for	(i = 0; i < theme->piecec_mem; ++i)
	{
		struct hnef_uix_img	* const HNEF_RSTR img =
					theme->piecev[i];
		if	(NULL != img)
		{
			assert	(NULL != uix->display);
			hnef_free_uix_img_fini(uix, img);
		}
	}
/*@i@*/	free	(theme->piecev);

	for	(i = 0; i < theme->squarec_mem; ++i)
	{
		struct hnef_uix_img	* const HNEF_RSTR img =
					theme->squarev[i];
		if	(NULL != img)
		{
			assert	(NULL != uix->display);
			hnef_free_uix_img_fini(uix, img);
		}
	}
/*@i@*/	free	(theme->squarev);

	free	(theme->ui_theme);
	free	(theme->datadir);
	free	(theme);
}

struct hnef_uix_themes *
hnef_alloc_uix_themes (void)
{
	size_t			i;
	struct hnef_uix_themes	* const HNEF_RSTR themes =
				malloc(sizeof(* themes));
	if	(NULL == themes)
	{
		return	NULL;
	}

	assert	(HNEF_UIX_THEME_MEM_DEF > 0);

	themes->themei_cur	= SIZET_MAX;
	themes->themec		= 0;
	themes->themec_mem	= HNEF_UIX_THEME_MEM_DEF;
	themes->themev		= malloc(sizeof(* themes->themev)
					* themes->themec_mem);
	if	(NULL == themes->themev)
	{
		free	(themes);
		return	NULL;
	}
	for	(i = 0; i < themes->themec_mem; ++i)
	{
		themes->themev[i]	= NULL;
	}

/* SPLint pointer to pointer */ /*@i1@*/\
	return	themes;
}

void
hnef_free_uix_themes_fini (
	const struct hnef_uix	* const HNEF_RSTR uix,
	struct hnef_uix_themes	* const HNEF_RSTR themes
	)
{
	size_t	i;

	assert	(NULL != uix);
	assert	(NULL != themes);
	assert	(NULL != themes->themev);

	for	(i = 0; i < themes->themec_mem; ++i)
	{
		struct hnef_uix_theme	* const HNEF_RSTR theme =
					themes->themev[i];
		if	(NULL != theme)
		{
			hnef_free_uix_theme_fini(uix, theme);
		}
	}
/*@i@*/	free	(themes->themev);
	free	(themes);
}

/*
 * Grows `themes->themev` by at least +1.
 */
static
enum HNEF_FR
hnef_uix_themes_grow (
/*@in@*/
/*@notnull@*/
	struct hnef_uix_themes	* const themes
	)
/*@modifies * themes@*/
{
	struct hnef_uix_theme	* * themev_new;
	size_t			mem_new,
				i;

	assert	(NULL != themes);

	mem_new	= (size_t)(themes->themec_mem
		* HNEF_UIX_THEME_MEM_GROW);
	if	(mem_new <= themes->themec_mem)
	{
		mem_new	= themes->themec_mem + 1;
	}
	assert	(mem_new > themes->themec_mem);

/* SPLint realloc */ /*@i@*/\
	themev_new	= realloc(themes->themev,
				sizeof(* themev_new) * mem_new);
	if	(NULL == themev_new)
	{
/* SPLint realloc */ /*@i2@*/\
		return	HNEF_FR_FAIL_ALLOC;
	}

	for	(i = themes->themec_mem; i < mem_new; ++i)
	{
		themev_new[i]	= NULL;
	}

	themes->themev		= themev_new;
	themes->themec_mem	= mem_new;
/* SPLint realloc */ /*@i1@*/\
	return	HNEF_FR_SUCCESS;
}

/*
 * Tries to initialize the themes in `ui->uix` by reading them from a
 * theme file. Note that XPM files are not loaded by this function -- it
 * just reads the theme file and creates the `hnef_uix_theme` structs;
 * the XPM files are read later for each ruleset when a theme is loaded,
 * so that XPM files are never loaded from disk unnecessarily.
 *
 * `ui->uix` must have opened the display and be fully initialized
 * (other than the themes that are loaded by this function). However,
 * the game does not have to be valid.
 *
 * Like other functions, return `HNEF_FR_FAIL_OTHER` for `frx` failures.
 *
 * This function returns immediately if the theme files has already been
 * loaded before. Thus you can call this function several times, even if
 * it fails to read the theme file the first time, and even if it has
 * already been successfully read (in which case this function does
 * nothing). If the theme file exists but is empty, then no themes are
 * created and this function will try to read the file and create them
 * again if this function is called again. However, if at least 1 theme
 * was successfully read from the file, then this function will do
 * nothing and return immediately if called again (unless you free all
 * the themes in `themes->themev`, set them to `NULL` and set
 * `themes->themec` to 0 -- then it will try to read the file again).
 *
 * If the file is simply not found, then `HNEF_FR_SUCCESS` is returned
 * and `success` is set to false. A `HNEF_FR` failure can be, for
 * example, failure to allocate memory. `success` is set to true if the
 * file is found and successfully read, even if the file contains no
 * themes.
 *
 * If the theme has already been read from file previously, then
 * `success` is true.
 */
enum HNEF_FR
hnef_uix_themes_init_file (
	struct hnef_ui	* const HNEF_RSTR ui,
	HNEF_BOOL	* const HNEF_RSTR success
	)
{
	enum HNEF_FR		fr		= HNEF_FR_SUCCESS;
	FILE			* HNEF_RSTR file	= NULL;
	struct hnef_uix_themes	* HNEF_RSTR themes	= NULL;
	char			* datadir		= NULL;
	struct gleip_lines	* GLEIP_RSTR lines	= NULL;
	size_t			datadir_len;
	int			retval			= 1;

	assert	(NULL != ui);
	assert	(NULL != ui->filefind);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(!hnef_line_empty(HNEF_UIX_DIR_DATA));
	assert	(!hnef_line_empty(HNEF_UIX_FILE_THEMES));

	* success	= HNEF_FALSE;
	themes		= ui->uix->themes;

	if	(themes->themec > 0)
	{
		/*
		 * Theme file has already been loaded. Maybe the file
		 * will contain more themes in the future, but if so the
		 * theme file will still never be read again (that's a
		 * limitation of this function).
		 */
		* success	= HNEF_TRUE;
		return		HNEF_FR_SUCCESS;
	}
	assert	(themes->themec < (size_t)1);
#ifndef	NDEBUG
	{
		size_t	i;
		for	(i = 0; i < themes->themec_mem; ++i)
		{
			assert	(NULL == themes->themev[i]);
		}
	}
#endif	/* NDEBUG */

	file	= hnef_fopen_dirguess(ui->filefind, HNEF_UIX_DIR_DATA,
			HNEF_UIX_FILE_THEMES, HNEF_FALSE, & fr);
	if	(NULL == file)
	{
		* success	= HNEF_FALSE;
		return		fr;
	}

	datadir		= hnef_fopen_filename(ui->filefind);
	assert	(strlen(datadir) >= strlen(HNEF_UIX_FILE_THEMES));
	datadir_len	= strlen(datadir)
			- strlen(HNEF_UIX_FILE_THEMES);
	gleip_line_rm_from(datadir, datadir_len);

	lines	= gleip_lines_alloc();
	if	(NULL == lines)
	{
		(void)fclose(file);
		return	HNEF_FR_FAIL_ALLOC;
	}

	while	(EOF != retval
/*@i1@*/\
	&&	!feof(file))
	{
/*@temp@*/
		const char	* HNEF_RSTR ui_theme	= NULL;
		gleip_lines_rm	(lines);
		retval = gleip_lineread_interp_file_def(lines, file, 0);
/*@i1@*/\
		if	(!retval)
		{
			gleip_lines_free(lines);
			(void)fclose	(file);
			return	HNEF_FR_FAIL_ALLOC;
		}
/*@i1@*/\
		if	(ferror(file))
		{
			gleip_lines_free(lines);
			(void)fclose	(file);
			return	HNEF_FR_FAIL_IO_FILE_READ;
		}

/*@i1@*/\
		if	(!gleip_lines_empty(lines)
		&&	!hnef_line_empty
				(ui_theme = gleip_lines_get(lines, 0)))
		{
			struct hnef_uix_theme * HNEF_RSTR theme = NULL;
			assert	(NULL != ui_theme);

			if	(themes->themec >= themes->themec_mem)
			{
				fr	= hnef_uix_themes_grow(themes);
				if	(!hnef_fr_good(fr))
				{
					gleip_lines_free(lines);
					(void)fclose	(file);
					return		fr;
				}
			}

			theme	= hnef_alloc_uix_theme_init
				(ui_theme, datadir);
			if	(NULL == theme)
			{
				gleip_lines_free(lines);
				(void)fclose	(file);
				return		HNEF_FR_FAIL_ALLOC;
			}
			themes->themev[themes->themec++] = theme;
		}
	}

	* success	= HNEF_TRUE;
	gleip_lines_free(lines);
	return	EOF == fclose(file)
		? HNEF_FR_FAIL_IO_FILE_CLOSE
		: HNEF_FR_SUCCESS;
}

/*
 * Gets index in `themes->themev` of `ui_theme`, if it exists, or
 * returns `SIZET_MAX` if it doesn't exist.
 */
static
size_t
hnef_uix_theme_index (
/*@in@*/
/*@notnull@*/
	const struct hnef_uix_themes	* const HNEF_RSTR themes,
/*@in@*/
/*@notnull@*/
	const char			* const HNEF_RSTR ui_theme
	)
/*@modifies nothing@*/
{
	size_t	i;

	assert	(NULL != themes);
	assert	(NULL != themes->themev);
	assert	(NULL != ui_theme);

	for	(i = 0; i < themes->themec; ++i)
	{
		struct hnef_uix_theme	* const HNEF_RSTR theme =
					themes->themev[i];
		assert	(NULL != theme);
		assert	(NULL != theme->ui_theme);
		if	(0 == strcmp(ui_theme, theme->ui_theme))
		{
			return	i;
		}
	}
	return			SIZET_MAX;
}

/*
 * Grows `theme->piecev/squarev` by at least +1.
 */
static
enum HNEF_FR
hnef_uix_theme_grow (
/*@in@*/
/*@notnull@*/
	struct hnef_uix_theme	* const theme,
	const HNEF_BOOL		square
	)
/*@modifies * theme@*/
{
	struct hnef_uix_img	* * arr_old	= NULL,
				* * arr_new	= NULL;
	size_t	mem_old,
		mem_new;

	assert	(NULL != theme);
	assert	(HNEF_UIX_IMG_MEM_GROW > 0);

	arr_old	= square
		? theme->squarev
		: theme->piecev;
	mem_old	= square
		? theme->squarec_mem
		: theme->piecec_mem;
	mem_new	= mem_old + HNEF_UIX_IMG_MEM_GROW;
	assert	(mem_new > mem_old);

/* SPLint realloc */ /*@i4@*/\
	arr_new	= realloc(arr_old, sizeof(* arr_new) * mem_new);
	if	(NULL == arr_new)
	{
/* SPLint realloc */ /*@i2@*/\
		return	HNEF_FR_FAIL_ALLOC;
	}

	if	(square)
	{
		theme->squarev		= arr_new;
		theme->squarec_mem	= mem_new;
	}
	else
	{
/* SPLint realloc */ /*@i2@*/\
		theme->piecev		= arr_new;
		theme->piecec_mem	= mem_new;
	}
/* SPLint realloc */ /*@i2@*/\
	return	HNEF_FR_SUCCESS;
}

/*
 * Helper function for `hnef_uix_themes_load_rules_themev()`.
 *
 * `theme` is the theme to load all pieces or squares for.
 *
 * `opt_datapath` is a temporary mutable string which the final path
 * will to the XPM files will be written to. It will require
 * `strlen(theme->datapath) + strlen(theme->ui_theme) + 9` characters
 * plus the '\0' char. (We could allocate the string each time this
 * function is called, but that's unnecessary. It's not
 * `NULM`-terminated.)
 */
static
enum HNEF_FR
hnef_uix_themes_load_rules_themev (
/*@in@*/
/*@notnull@*/
	struct hnef_uix		* const HNEF_RSTR uix,
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	struct hnef_uix_theme	* const HNEF_RSTR theme,
/*@out@*/
/*@notnull@*/
	char			* const HNEF_RSTR opt_datapath,
	const HNEF_BOOL		square,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX		* const HNEF_RSTR frx
	)
/*@modifies * uix, * game, * theme, * opt_datapath, * frx@*/
{
	enum HNEF_FRX	frx_tmp		= HNEF_FRX_SUCCESS;
	char		xpm_symbol;
	unsigned short	i;

	assert	(NULL != game);
/*@i2@*/\
	assert	(hnef_rvalid_good(hnef_game_valid(game)));
	assert	(NULL != uix);
	assert	(NULL != uix->display);
	assert	(NULL != theme);
	assert	(NULL != opt_datapath);

	* frx	= HNEF_FRX_SUCCESS;

	xpm_symbol	= (char)(square
			? HNEF_UIX_XPM_SQUARE
			: HNEF_UIX_XPM_PIECE);

	for	(i = 0; i < (square
			? game->rules->type_squarec
			: game->rules->type_piecec); ++i)
	{
		HNEF_BIT_U8	ui_bit;
		if	(square)
		{
			ui_bit	= hnef_game_uibit_square
				(& game->rules->type_squares[i]);
		}
		else
		{
			ui_bit	= hnef_game_uibit_piece
				(& game->rules->type_pieces[i]);
		}

		if (NULL == hnef_uix_img_get(theme, ui_bit, square))
		{
			struct hnef_uix_img * HNEF_RSTR img = NULL;

			if	((	square
				&& theme->squarec >= theme->squarec_mem)
			||	(	!square
				&& theme->piecec >= theme->piecec_mem))
			{
				const enum HNEF_FR fr =
					hnef_uix_theme_grow
					(theme, square);
				if	(!hnef_fr_good(fr))
				{
/*@i1@*/\
					return	fr;
				}
			}

			assert	((unsigned short)ui_bit
				< (unsigned short)128);
/* SPLint snprintf is not in C89 */ /*@i1@*/\
			if (sprintf(opt_datapath, "%s%s/%c%03hu.xpm",
				theme->datadir, theme->ui_theme,
				xpm_symbol, (unsigned short)ui_bit) < 0)
			{
				return	HNEF_FR_FAIL_IO_WRITE;
			}

			img	= hnef_alloc_uix_img_init(uix, ui_bit,
				square, opt_datapath, & frx_tmp);
			if	(!hnef_frx_good(frx_tmp))
			{
				assert	(NULL == img);
				* frx	= frx_tmp;
				continue;	/* Don't add */
			}
/*@i1@*/\
			else if	(NULL == img)
			{
				assert	(hnef_frx_good(frx_tmp));
				return	HNEF_FR_FAIL_ALLOC;
			}

			assert	(NULL != img);

			if	(square)
			{
				assert	(theme->squarec + 1
					<= theme->squarec_mem);
				theme->squarev[theme->squarec++] = img;
			}
			else
			{
				assert	(theme->piecec + 1
					<= theme->piecec_mem);
				theme->piecev[theme->piecec++] = img;
			}
		}
	}
	return	HNEF_FR_SUCCESS;
/*
 * SPLint doesn't know that `uix` is modified through `theme`.
 */
/*@i1@*/\
}

/*
 * Calculates square width and heights for the theme.
 */
static
void
hnef_uix_theme_size_calc (
/*@in@*/
/*@notnull@*/
	struct hnef_uix_theme	* const HNEF_RSTR theme
	)
/*@modifies * theme@*/
{
	size_t	i;
	int	width_max	= HNEF_THEME_SIZE_INVALID,
		height_max	= HNEF_THEME_SIZE_INVALID;

	assert	(NULL != theme);

	for	(i = 0; i < theme->squarec; ++i)
	{
		struct hnef_uix_img	* const HNEF_RSTR img =
					theme->squarev[i];
		assert	(NULL != img);
		if	(img->image->width > width_max)
		{
			width_max	= img->image->width;
		}
		if	(img->image->height > height_max)
		{
			height_max	= img->image->height;
		}
	}

	theme->square_width	= width_max;
	theme->square_height	= height_max;
	if	(theme->square_width < 1
	||	theme->square_height < 1)
	{
		theme->square_width	= HNEF_THEME_SIZE_INVALID;
		theme->square_height	= HNEF_THEME_SIZE_INVALID;
	}
}

static
enum HNEF_FR
hnef_uix_themes_load_rules_paintmap (
/*@in@*/
/*@notnull@*/
	struct hnef_uix		* const HNEF_RSTR uix,
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game
	)
/*@modifies * uix, * game@*/
{
	unsigned short	blen;

	assert	(NULL != uix);
	assert	(NULL != game);
/*@i2@*/\
	assert	(hnef_rvalid_good(hnef_game_valid(game)));

	if	(NULL != uix->paintmap
	&&	game->rules->bwidth == uix->paintmap_width
	&&	game->rules->bheight == uix->paintmap_height)
	{
		/*
		 * Same board dimensions, no need to allocate it again.
		 */
		hnef_uix_paintmap_clear(uix);
		return	HNEF_FR_SUCCESS;
	}

	if	(NULL != uix->paintmap)
	{
		free		(uix->paintmap);
		uix->paintmap	= NULL;
	}
	assert	(NULL == uix->paintmap);

	uix->paintmap_width	= game->rules->bwidth;
	uix->paintmap_height	= game->rules->bheight;
	blen	= (unsigned short)
		(uix->paintmap_width * uix->paintmap_height);
	assert	(blen == game->rules->opt_blen);

	uix->paintmap	= malloc(sizeof(* uix->paintmap) * blen);
	if	(NULL == uix->paintmap)
	{
		uix->paintmap_width	= uix->paintmap_height
					= HNEF_BOARDPOS_NONE;
		assert	(NULL == uix->paintmap);
/*@i1@*/\
		return	HNEF_FR_FAIL_ALLOC;
	}

	assert	(NULL != uix->paintmap);
	hnef_uix_paintmap_clear(uix);
/*@i1@*/\
	return	HNEF_FR_SUCCESS;
}

/*
 * Attempts to load the XPM files for the current ruleset in
 * `uix->themes`.
 *
 * The game must be valid and fully initialized, and `uix` must be fully
 * initialized (other than these image files).
 *
 * `ui_theme` is the desired theme, which does not have to exists among
 * `uix->themes->themev`. If `NULL`, then no theme is desired.
 *
 * If all XPM files are successfully loaded, then `success` is set to
 * true. If some XPM file can't be loaded (even if some other can), then
 * it's set to false. This is a non-fatal failure and you can still use
 * `uix->themes`, but attempting to paint pieces using it will result in
 * placeholder graphics. `success` is also false if there are no themes
 * in `uix->themes->themev` (which will also result in placeholder
 * graphics).
 *
 * Upon `HNEF_FR_FAIL_OTHER`, `frx` indicates the failure.
 *
 * This function also calculates the size of the current theme, if there
 * is at least one theme in `uix->themes`.
 */
enum HNEF_FR
hnef_uix_themes_load_rules (
	struct hnef_uix		* const HNEF_RSTR uix,
	struct hnef_game	* const HNEF_RSTR game,
	const char		* const HNEF_RSTR ui_theme,
	enum HNEF_FRX		* const HNEF_RSTR frx,
	HNEF_BOOL		* const HNEF_RSTR success
	)
{
	struct hnef_uix_themes	* HNEF_RSTR themes	= NULL;
	struct hnef_uix_theme	* HNEF_RSTR theme	= NULL;
	char			* HNEF_RSTR datapath	= NULL;
	size_t			datapath_len;
	enum HNEF_FR		fr		= HNEF_FR_SUCCESS;
	enum HNEF_FRX		frxtmp		= HNEF_FRX_SUCCESS;

	assert	(NULL != game);
	assert	(NULL != game->rules);
	assert	(NULL != game->rules->ui_theme);
/*@i2@*/\
	assert	(hnef_rvalid_good(hnef_game_valid(game)));
	assert	(NULL != uix);
	assert	(NULL != uix->display);
	assert	(NULL != uix->themes);
	assert	(NULL != frx);
	assert	(NULL != success);

	themes		= uix->themes;
	* frx		= HNEF_FRX_SUCCESS;
	* success	= HNEF_TRUE;

	fr	= hnef_uix_themes_load_rules_paintmap(uix, game);
	if	(!hnef_fr_good(fr))
	{
		* success	= HNEF_FALSE;
		return		fr;
	}

	if	(themes->themec < (size_t)1)
	{
		/*
		 * Can't set size.
		 */
		* success	= HNEF_FALSE;
		return		HNEF_FR_SUCCESS;
	}

	if	(NULL != ui_theme)
	{
		themes->themei_cur	= hnef_uix_theme_index
					(themes, ui_theme);
	}

	if	(SIZET_MAX == themes->themei_cur
	&&	!hnef_line_empty(game->rules->ui_theme))
	{
		themes->themei_cur	= hnef_uix_theme_index
					(themes, game->rules->ui_theme);
	}

	if	(SIZET_MAX == themes->themei_cur)
	{
		themes->themei_cur	= 0;
	}
	assert	(themes->themec > 0);
	assert	(themes->themei_cur < themes->themec);

	theme	= themes->themev[themes->themei_cur];
	assert	(NULL != theme);

	/*
	 * "<datadir>/<ui_theme>/p001.xpm"
	 *           0          123456789
	 *           ^- This path sep. already terminates `datadir`.
	 */
	datapath_len	= strlen(theme->datadir)
			+ strlen(theme->ui_theme) + 9;
	datapath	= malloc(sizeof(* datapath)
			* (datapath_len + 1));
	if	(NULL == datapath)
	{
		fr	= HNEF_FR_FAIL_ALLOC;
		goto	RETURN_FR;
	}

	fr	= hnef_uix_themes_load_rules_themev
		(uix, game, theme, datapath, HNEF_FALSE, frx);
	if	(!hnef_fr_good(fr))
	{
		goto	RETURN_FR;
	}
/*@i2@*/\
	else if	(!hnef_frx_good(* frx))
	{
		frxtmp		= * frx;
		* success	= HNEF_FALSE;
	}

	fr	= hnef_uix_themes_load_rules_themev
		(uix, game, theme, datapath, HNEF_TRUE, frx);
	if	(!hnef_fr_good(fr))
	{
		goto	RETURN_FR;
	}

	RETURN_FR:
	if	(!hnef_frx_good(frxtmp))
	{
		* frx		= frxtmp;
	}

	if	(!hnef_frx_good(* frx))
	{
		* success	= HNEF_FALSE;
	}

	if	(NULL != datapath)
	{
		free	(datapath);
	}

	assert	(NULL != theme);
	hnef_uix_theme_size_calc(theme);

	if	(!hnef_fr_good(fr))
	{
		return	fr;
	}
	else
	{
		return	hnef_frx_good(* frx)
			? HNEF_FR_SUCCESS
			: HNEF_FR_FAIL_OTHER;
	}
}

#endif	/* HNEFATAFL_UI_XLIB */

