/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#include <assert.h>	/* assert */
#ifndef	S_SPLINT_S /* SPLint bug: ctype.h results in parse error */
#include <ctype.h>	/* is* */
#endif	/* S_SPLINT_S */
#include <string.h>	/* strlen */

#include "gleipnir_line.h"	/* gleip_line_* */

#include "config.h"	/* HNEF_UI_ENVVAR_* */
#include "langkeyt.h"	/* HNEF_LK_* */
#include "uixfont.h"

/*
 * These functions use whatever multi-byte locale the user has defined
 * if `font->use_fontset`, or Latin-1 if `!font->use_fontset`.
 */

/*@observer@*/
/*@unchecked@*/
static
const char	* const HNEF_UIX_FONT_FALLBACK =
	"-misc-fixed-medium-r-normal-*-*-140-*-*-*-*-iso10646-*",
		* const HNEF_UIX_FONT_ANY =
	"fixed,*";

int
hnef_uix_font_textwidth (
	struct hnef_uix	* const HNEF_RSTR uix,
	const char	* const HNEF_RSTR str
	)
{
	assert	(NULL != uix);
	assert	(NULL != uix->display);
	assert	(NULL != uix->font);
	assert	(NULL != str);
	assert	((	uix->font->use_fontset
		&&	NULL != uix->font->fontset
		&&	NULL == uix->font->fontstruct)
	||	(	!uix->font->use_fontset
		&&	NULL == uix->font->fontset
		&&	NULL != uix->font->fontstruct));

	if	(uix->font->use_fontset)
	{
		assert	(NULL != uix->font->fontset);
/*@i@*/		return	XmbTextEscapement(uix->font->fontset,
				str,
				(int)strlen(str));
	}
	else
	{
		assert	(NULL != uix->font->fontstruct);
/*@i@*/		return	XTextWidth(uix->font->fontstruct,
				str,
				(int)strlen(str));
	}
}

enum HNEF_FR
hnef_uix_font_print (
	struct hnef_uix	* const HNEF_RSTR uix,
	const char	* const HNEF_RSTR str,
	const int	x,
	const int	y,
	enum HNEF_FRX	* const HNEF_RSTR frx
	)
{
	assert	(NULL != uix);
	assert	(NULL != uix->display);
	assert	(NULL != uix->font);
	assert	(NULL != str);
	assert	(NULL != frx);
	assert	((	uix->font->use_fontset
		&&	NULL != uix->font->fontset
		&&	NULL == uix->font->fontstruct)
	||	(	!uix->font->use_fontset
		&&	NULL == uix->font->fontset
		&&	NULL != uix->font->fontstruct));

	* frx	= HNEF_FRX_SUCCESS;

	if	(uix->font->use_fontset)
	{
		assert	(NULL != uix->font->fontset);
		/*
		 * NOTE:	See the notes about `XDrawString()`
		 *		below.
		 */
/*@i@*/		XmbDrawString(uix->display, uix->window,
				uix->font->fontset, uix->gc,
				x,
				y,
				str,
				(int)strlen(str));
	}
	else
	{
		assert	(NULL != uix->font->fontstruct);
		/*
		 * NOTE:	According to the XLib standard, `str` is
		 *		actually a `char *` here, not a
		 *		`const char *`. It's often defined as
		 *		const in the X headers. I assume that
		 *		it's safe to pass a read-only string
		 *		literal to this function.
		 */
/*@i@*/		XDrawString(uix->display, uix->window, uix->gc,
				x,
				y,
				str,
				(int)strlen(str));
	}

	return	HNEF_FR_SUCCESS;
}

/*
 * NOTE:	This could be a general utility function for checking if
 *		a string is empty (apart from spaces), but it's only
 *		needed here.
 */
static
HNEF_BOOL
hnef_string_empty (
/*@in@*/
/*@notnull@*/
	const char	* HNEF_RSTR str
	)
/*@modifies nothing@*/
{
	assert	(NULL != str);

	while	('\0' != * str)
	{
/*@i1@*/\
		if	(!isspace((int)* str++))
		{
			return	HNEF_FALSE;
		}
	}
	return	HNEF_TRUE;
}

/*
 * `basename` may not be a 0-length string.
 */
/*@in@*/
/*@null@*/
/*@only@*/
static
XFontSet	/* Pointer */
hnef_alloc_fontset (
/*@in@*/
/*@notnull@*/
	const struct gleip_lang	* const GLEIP_RSTR lang,
/*@in@*/
/*@notnull@*/
	Display			* const display,
/*@in@*/
/*@notnull@*/
	const char		* basename,
	const HNEF_BOOL		basename_env,
	const HNEF_BOOL		print_error
	)
/*@globals fileSystem, stderr@*/
/*@modifies fileSystem, stderr@*/
{
	XFontSet	fontset				= NULL;
	char		* * missing_charset_list_return	= NULL;
	int		missing_charset_count_return;
	char		* def_string_return		= NULL;

	assert	(NULL != lang);
	assert	(NULL != display);
	assert	(NULL != basename);

	if	(basename_env)
	{
		basename	= getenv(basename);
		if	(NULL == basename
		||	hnef_line_empty(basename))
		{
			return	NULL;
		}
	}

	/*
	 * If `basename` is all white space, then it will cause an
	 * invalid read later on for some reason.
	 */
	if	(hnef_string_empty(basename))
	{
		return	NULL;
	}

	/*
	 * NOTE:	Valgrind reports some storage, derived from
	 *		`XrmStringToQuark()`, as "still reachable"
	 *		(38684 bytes in 806 blocks, but it probably
	 *		varies depending on which font is loaded). This
	 *		may be a real leak since we have another
	 *		positive involving quarks (see
	 *		`handle_event_key()` in `xlib.c`), but we are
	 *		actually calling `XFreeFontSet()` and
	 *		`XFreeStringList()` as mandated by the standard.
	 *		There is no "definitely lost" (or other)
	 *		storage, so there is no pointer pointing to it,
	 *		meaning it's probably freed by XLib after the
	 *		program returns, like the Pixmap false positive.
	 *
	 * NOTE:	When this function fails, it leaks some
	 *		"Definitely lost" bytes through `XOpenOM()`
	 *		(which is actually `XCreateFontSet()`). However,
	 *		all returned information is freed, so there is
	 *		nothing more to do and this must either be an
	 *		XLib bug, missing XLib documentation on how to
	 *		free stuff when `XCreateFontSet()` fails, or a
	 *		false positive, i.e. beyond my control whatever
	 *		it is.
	 */
/*@i@*/	fontset	= XCreateFontSet(display, basename,
/*@i@*/		& missing_charset_list_return,
/*@i@*/		& missing_charset_count_return,
/*@i@*/		& def_string_return);

/*@i@*/	if	(NULL != missing_charset_list_return)
	{
		if	(print_error)
		{
			int	j;
/*@i@*/			for	(j = 0;
/*@i@*/				j < missing_charset_count_return; ++j)
			{
				const char * const HNEF_RSTR charset =
					missing_charset_list_return[j];
				(void)fprintf(stderr, "%s: %s\n",
					gleip_lang_getany(lang,
					HNEF_LK_XLIB_FONT_CHARSET_MISS),
					charset);
			}
		}
/*@i@*/		XFreeStringList(missing_charset_list_return);
	}

	/*
	 * `fontset` may be `NULL`.
	 */
	return	fontset;
}

struct hnef_uix_font *
hnef_alloc_uix_font_init (
	const struct gleip_lang	* const GLEIP_RSTR lang,
	Display			* const dpy,
	GC			const gc,
	const char		* const HNEF_RSTR setlocale_return,
	enum HNEF_FRX		* const HNEF_RSTR frx
	)
{
	struct hnef_uix_font	* HNEF_RSTR font	= NULL;

	assert	(NULL != lang);
	assert	(NULL != dpy);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	font	= malloc(sizeof(* font));
	if	(NULL == font)
	{
		return	NULL;
	}

	font->use_fontset	= NULL != setlocale_return;
	font->fontset		= NULL;
	font->fontstruct	= NULL;
	font->ascent		= font->descent		= 0;

	if	(font->use_fontset)
	{
		if (NULL == (font->fontset = hnef_alloc_fontset
				(lang, dpy, HNEF_UI_ENVVAR_FONT,
				HNEF_TRUE, HNEF_TRUE))
		&& NULL == (font->fontset = hnef_alloc_fontset
				(lang, dpy, HNEF_UIX_FONT_FALLBACK,
				HNEF_FALSE, HNEF_FALSE))
		&& NULL == (font->fontset = hnef_alloc_fontset
				(lang, dpy, HNEF_UIX_FONT_ANY,
				HNEF_FALSE, HNEF_TRUE)))
		{
			(void)fprintf(stderr, "%s\n",
				gleip_lang_getany(lang,
				HNEF_LK_XLIB_FONT_FALLBACK));
			font->use_fontset	= HNEF_FALSE;
		}
		else
		{
			XFontStruct	* * fontv;
			char		* * font_names;
			int		fontc,
					i;

			assert	(font->use_fontset);

/*@i@*/			fontc	= XFontsOfFontSet(font->fontset,
/*@i@*/					& fontv, & font_names);
			for	(i = 0; i < fontc; ++i)
			{
				XFontStruct	* const HNEF_RSTR f =
/*@i@*/						fontv[i];
				font->ascent = hnef_max_int
					(font->ascent, f->ascent);
				font->descent = hnef_max_int
					(font->descent, f->descent);
			}
		}

		if	(NULL != setlocale_return
		&&	!font->use_fontset)
		{
			/*
			 * The user has defined `LC_ALL`, `LC_CTYPE` or
			 * `LANG` (which was properly set by
			 * `setlocale()`), but the fontset could not be
			 * loaded and will fall back on Latin-1. This
			 * may cause the locale to expect, for example,
			 * UTF-8, while the XLib input / output
			 * functions will work use Latin-1. The
			 * solutions are to either set `LC_*` to "C" (or
			 * similar), or to fix the fontsets so they can
			 * be loaded. There is nothing more we can do
			 * than to alert the user of it.
			 */
			(void)fprintf(stderr, "%s\n",
				gleip_lang_getany(lang,
				HNEF_LK_XLIB_FONT_FALLBACK_LOCALE));
		}
	}

	/*
	 * Fallback (either because `setlocale_return` is `NULL` or
	 * because `XCreateFontSet` failed).
	 */
	if	(!font->use_fontset)
	{
		assert	(NULL == font->fontset);

/*@i@*/		font->fontstruct = XQueryFont(dpy, XGContextFromGC(gc));
		if	(NULL == font->fontstruct)
		{
			/*
			 * We won't be able to paint any text, so we
			 * must fail fatally here.
			 */
			* frx	= HNEF_FRX_FAIL_FONT_QUERY;
			free	(font);
			return	NULL;
		}
		font->ascent		= font->fontstruct->ascent;
		font->descent		= font->fontstruct->descent;
	}

	assert	((	font->use_fontset
		&&	NULL != font->fontset
		&&	NULL == font->fontstruct)
	||	(	!font->use_fontset
		&&	NULL == font->fontset
		&&	NULL != font->fontstruct));

	return	font;
}

void
hnef_free_uix_font_fini (
	struct hnef_uix		* const HNEF_RSTR uix,
	struct hnef_uix_font	* const HNEF_RSTR font
	)
{
	assert	(NULL != uix);
	assert	(NULL != font);

	if	(font->use_fontset)
	{
		assert	(NULL == font->fontstruct);
		assert	(NULL != font->fontset);
/*@i@*/		(void)XFreeFontSet	(uix->display, font->fontset);
	}
	else
	{
		assert	(NULL != font->fontstruct);
		assert	(NULL == font->fontset);
/*@i@*/		(void)XFreeFontInfo	(NULL, font->fontstruct, 1);
	}

/*@i@*/	free	(font);
}

#endif	/* HNEFATAFL_UI_XLIB */

