/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <errno.h>	/* errno */

#include "gleipnir_line.h"	/* gleip_line_* */

#include "config.h"	/* HNEF_UI_ENVVAR_* */
#include "filefind.h"

/*
 * -	`path_prefix_env` is the name of an environmental variable used
 *	as a prefix to `path`, or `NULL`. If this environmental variable
 *	can't be retrieved with `getenv`, then this function will not
 *	construct a path.
 * -	`path` is the relative path, or `NULL`.
 * -	`path_subdir` is the name of a sub-directory in path, or `NULL`.
 * -	`filename` is the name of the file in `path`.
 * -	`result` is where the path gets constructed.
 *
 * If a `path*` argument or `filename` is not `NULL`, then it must have
 * length > 0.
 *
 * `strlen(* result)` is 0 if the path is not constructed.
 */
static
enum HNEF_FR
hnef_construct_path (
/*@in@*/
/*@null@*/
	const char	* const HNEF_RSTR path_prefix_env,
/*@in@*/
/*@null@*/
	const char	* const HNEF_RSTR path,
/*@in@*/
/*@null@*/
	const char	* const HNEF_RSTR path_subdir,
/*@in@*/
/*@notnull@*/
	const char	* const HNEF_RSTR filename,
/*@in@*/
/*@notnull@*/
	char		* * const result
	)
/*@modifies * result@*/
{
	HNEF_BOOL	started	= HNEF_FALSE;

	assert	(NULL == path_prefix_env
	||	!hnef_line_empty(path_prefix_env));
	assert	(NULL == path
	||	!hnef_line_empty(path));
	assert	(NULL == path_subdir
	||	!hnef_line_empty(path_subdir));
	assert	(NULL != filename
	&&	!hnef_line_empty(filename));
	assert	(NULL != result);
	assert	(NULL != * result);

	gleip_line_rm(* result);

	if	(NULL != path_prefix_env)
	{
		const char	* const HNEF_RSTR env =
				getenv(path_prefix_env);
		if	(NULL == env
		||	hnef_line_empty(env))
		{
			/*
			 * Environmental variable given, but the
			 * variable is undefined or empty.
			 */
			return	HNEF_FR_SUCCESS;
		}
/*@i1@*/\
		if	(!gleip_line_cpy_b(result, env))
		{
			return	HNEF_FR_FAIL_ALLOC;
		}
		started	= HNEF_TRUE;
	}

	if	(NULL != path)
	{
		if	(started)
		{
/*@i1@*/\
			if	(!gleip_line_add_ch_back_b(result, '/'))
			{
				goto	RETURN_FAIL;
			}
		}
/*@i1@*/\
		if	(!gleip_line_add_str_back_b(result, path))
		{
			goto	RETURN_FAIL;
		}
		started	= HNEF_TRUE;
	}

	if	(NULL != path_subdir)
	{
		if	(started)
		{
/*@i1@*/\
			if	(!gleip_line_add_ch_back_b(result, '/'))
			{
				goto	RETURN_FAIL;
			}
		}
/*@i1@*/\
		if (!gleip_line_add_str_back_b(result, path_subdir))
		{
			goto	RETURN_FAIL;
		}
		started	= HNEF_TRUE;
	}

	if	(started)
	{
/*@i1@*/\
		if	(!gleip_line_add_ch_back_b(result, '/'))
		{
			goto	RETURN_FAIL;
		}
	}
/*@i1@*/\
	if	(!gleip_line_add_str_back_b(result, filename))
	{
		goto	RETURN_FAIL;
	}

	return	HNEF_FR_SUCCESS;

	RETURN_FAIL:
	gleip_line_rm(* result);
	return	HNEF_FR_FAIL_ALLOC;
}

/*
 * Tries to load the given `subdir/filename` in the following
 * directories. When an environment variable is used, it is not tried if
 * the environment variable is not defined.
 *
 * 1.	Treat file as absolute path	(if `try_absolute)
 * 2.	<d> command			(if strlen > 0, i.e. is set)
 * 3.	<-d>				(if -d is given)
 * 4.	$<HNEF_UI_ENVVAR_PATH>		(environment variable)
 * 5.	$HOME/hnefatafl			(environment variable)
 * 6.	$HOME/.hnefatafl		(environment variable)
 * 7.	/usr/share/hnefatafl
 * 8.	/usr/share/games/hnefatafl
 * 9.	/usr/local/share/hnefatafl
 * 10.	/usr/local/share/games/hnefatafl
 *
 * This opens the file in "r" mode. Thus it will only return a `!NULL`
 * `FILE` if the file exists on disk -- if it doesn't exist, then it
 * will fail to open it and return `NULL` (this is the behavior of "r"
 * in `fopen()`, so you can't modify this function to take an arbitrary
 * mode parameter that may be "a" or "w").
 *
 * If this function returns `!NULL`, then `errno` is not changed by this
 * function. Otherwise it may be changed. Also, on a `!NULL` return, you
 * can call `hnef_fopen_filename()` to get the filename that was passed
 * to `fopen()` (but subsequent calls to this function will reset it).
 */
FILE *
hnef_fopen_dirguess (
	struct hnef_filefind	* const HNEF_RSTR filefind,
	const char		* const HNEF_RSTR subdir,
	const char		* const HNEF_RSTR filename,
	const HNEF_BOOL		try_absolute,
	enum HNEF_FR		* const HNEF_RSTR fr
	)
{
	FILE		* HNEF_RSTR file	= NULL;
	/*
	 * The following paths are checked from top to bottom.
	 *
	 * argv[n][0] is the environment variable, which may be `NULL`.
	 *
	 * argv[n][1] is the relative path, which may be `NULL`.
	 *
	 * (SPLint doesn't like this at all, but many of the warnings
	 * are probably caused by pointer-to-pointer. This eliminates a
	 * lot of code repetition; and if you want to search more paths,
	 * just add them to the array -- the size is figured out
	 * automatically.)
	 */
/*@-dependenttrans@*/
/*@-nullassign@*/
/*@observer@*/
	const char	* const HNEF_RSTR argv[][2] =
		{

			{
				/*
				 * NOTE:	HNEF_UI_ENVVAR_PATH
				 *
				 * Can't compile as C89 if we use
				 * initializers that are not available
				 * at load time.
				 */
				"HNEFATAFL_PATH", NULL
			},

			{
				/*
				 * NOTE:	HNEF_UI_ENVVAR_SYS_HOME
				 */
				"HOME", "hnefatafl"
			},

			{
				/*
				 * NOTE:	HNEF_UI_ENVVAR_SYS_HOME
				 */
				"HOME", ".hnefatafl"
			},

			{
				NULL, "/usr/share/hnefatafl"
			},

			{
				NULL, "/usr/share/games/hnefatafl"
			},

			{
				NULL, "/usr/local/share/hnefatafl"
			},

			{
				NULL, "/usr/local/share/games/hnefatafl"
			}

		};
/*@=nullassign@*/
/*@=dependenttrans@*/
	const size_t	argc		= sizeof(argv) / sizeof(* argv);
	size_t		i;
	const int	errno_old	= errno;

	assert	(NULL != filefind);
	assert	(NULL != filefind->abs_path_cmd);
	assert	(NULL != filefind->opt_dirguess);
	assert	(NULL == subdir
	||	!hnef_line_empty(subdir));
	assert	(NULL != filename
	&&	!hnef_line_empty(filename));
	assert	(NULL != fr);

	* fr	= HNEF_FR_SUCCESS;

	if	(try_absolute)
	{
		/*
		 * 1. Try the filename itself. This is sometimes
		 * dangerous for the same reason that we don't have "."
		 * in `$PATH`.
		 */
		* fr	= hnef_construct_path(NULL, NULL, NULL,
				filename, & filefind->opt_dirguess);
		if	(!hnef_fr_good(* fr))
		{
			return	NULL;
		}
/*@i1@*/\
		else if	(!hnef_line_empty(filefind->opt_dirguess))
		{
			file	= fopen(filefind->opt_dirguess, "r");
			if	(NULL != file)
			{
				errno	= errno_old;
				return	file;
			}
		}
	}

	/*
	 * 2. Try `<filefind->abs_path_cmd>/<subdir>/<filename>`
	 *
	 * Only do this if an absolute path has been set by command (if
	 * the string is non-empty).
	 */
	if	(!hnef_line_empty(filefind->abs_path_cmd))
	{
		* fr	= hnef_construct_path(NULL,
				filefind->abs_path_cmd,
				subdir,
				filename, & filefind->opt_dirguess);
		if	(!hnef_fr_good(* fr))
		{
			return	NULL;
		}
/*@i1@*/\
		else if	(!hnef_line_empty(filefind->opt_dirguess))
		{
			file	= fopen(filefind->opt_dirguess, "r");
			if	(NULL != file)
			{
				errno	= errno_old;
				return	file;
			}
		}
	}

	/*
	 * 3. Try `<filefind->abs_path>/<subdir>/<filename>`
	 *
	 * Only do this if an absolute path is given as an invocation
	 * parameter.
	 */
	if	(NULL != filefind->abs_path)
	{
		* fr	= hnef_construct_path(NULL, filefind->abs_path,
				subdir,
				filename, & filefind->opt_dirguess);
		if	(!hnef_fr_good(* fr))
		{
			return	NULL;
		}
/*@i1@*/\
		else if	(!hnef_line_empty(filefind->opt_dirguess))
		{
			file	= fopen(filefind->opt_dirguess, "r");
			if	(NULL != file)
			{
				errno	= errno_old;
				return	file;
			}
		}
	}

	for	(i = 0; i < argc; ++i)
	{
		const char	* const HNEF_RSTR env	= argv[i][0];
		const char	* const HNEF_RSTR rel	= argv[i][1];

		* fr	= hnef_construct_path(env, rel, subdir,
				filename, & filefind->opt_dirguess);
		if	(!hnef_fr_good(* fr))
		{
			return	NULL;
		}
/*@i1@*/\
		else if	(!hnef_line_empty(filefind->opt_dirguess))
		{
			file	= fopen(filefind->opt_dirguess, "r");
			if	(NULL != file)
			{
				errno	= errno_old;
				return	file;
			}
		}
	}

	return	NULL;
}

/*
 * If `hnef_fopen_dirguess()` returned a `!NULL` file, then this
 * function returns the path that was passed to `fopen()`.
 *
 * If it returned `NULL`, then this function will return a string
 * pointing to a file that doesn't exist.
 *
 * The returned string is `NULM`-terminated and can be modified (but it
 * will be cleared on the next call to `hnef_fopen_dirguess()`).
 */
char *
hnef_fopen_filename (
	struct hnef_filefind	* const HNEF_RSTR filefind
	)
{
	assert	(NULL != filefind);

	return	filefind->opt_dirguess;
}

struct hnef_filefind *
hnef_alloc_filefind (void)
{
	struct hnef_filefind	* const HNEF_RSTR filefind =
				malloc(sizeof(* filefind));
	if	(NULL == filefind)
	{
		return	NULL;
	}

	filefind->opt_dirguess	= gleip_line_alloc();
	if	(NULL == filefind->opt_dirguess)
	{
		free	(filefind);
		return	NULL;
	}

	filefind->abs_path_cmd	= gleip_line_alloc();
	if	(NULL == filefind->abs_path_cmd)
	{
		free	(filefind->opt_dirguess);
		free	(filefind);
		return	NULL;
	}

	filefind->abs_path		= NULL;
	filefind->abs_runcom		= NULL;
	filefind->fname_lang		= NULL;
	filefind->fname_rules		= NULL;
	filefind->runcom_suppress	= HNEF_FALSE;

	return	filefind;
}

void
hnef_free_filefind (
	struct hnef_filefind	* const HNEF_RSTR filefind
	)
{
	assert	(NULL != filefind);
	assert	(NULL != filefind->opt_dirguess);
	assert	(NULL != filefind->abs_path_cmd);

	free	(filefind->abs_path_cmd);
	free	(filefind->opt_dirguess);
	free	(filefind);
}

