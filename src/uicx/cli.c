/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <errno.h>	/* errno */
#include <stdio.h>	/* printf */
#include <string.h>	/* strlen */

#include "gleipnir_line.h"	/* gleip_line_* */
#include "gleipnir_lineread.h"	/* gleip_lineread_* */
#include "gleipnir_lines.h"	/* gleip_lines_* */

#include "cli.h"
#include "cliio.h"	/* hnef_print_* */
#include "cliiot.h"	/* HNEF_CMD_* */
#include "config.h"	/* HNEF_UI_ENVVAR_* */
#include "filefind.h"	/* hnef_fopen_dirguess */
#include "funcx.h"	/* hnef_frx_good */
#include "langkeyt.h"	/* HNEF_LK_* */
#include "ui.h"		/* hnef_uic_* */
#include "uiutil.h"	/* hnef_uiutil_* */
#include "uix.h"	/* hnef_uix_* */

#ifdef	HNEFATAFL_UI_AIM
#include "uiutilt.h"	/* HNEF_AIM_DEPTH_DEF */
#endif	/* HNEFATAFL_UI_AIM */

#ifdef	HNEFATAFL_UI_XLIB
#include "xlib.h"	/* run_xlib */
#endif

/*@observer@*/
/*@unchecked@*/
static
const char	* const CLI_EOF			= "EOF",
		* const FNAME_RC_DEFAULT	= "hnefataflrc",
		* const RC_PROMPT		= "# ";

/*
 * Piece markers when printing the board.
 *
 * A square is `*_SPECIAL` if it can capture pieces or if some piece
 * type has `noreturn` for it.
 */
/*@unchecked@*/
static
const char	CLI_BOARD_LAST_POS_L		= '(',
		CLI_BOARD_LAST_POS_R		= ')',
		CLI_BOARD_LAST_DEST_L		= '[',
		CLI_BOARD_LAST_DEST_R		= ']',
		CLI_BOARD_LIST_DEST_L		= '<',
		CLI_BOARD_LIST_DEST_R		= '>',
		CLI_BOARD_ORDINARY_LR		= ' ',
		CLI_BOARD_SQUARE_EMPTY		= ' ',
		CLI_BOARD_SQUARE_ESCAPE		= '-',
		CLI_BOARD_SQUARE_ORDINARY	= '.',
		CLI_BOARD_SQUARE_SPECIAL	= ':';

#ifdef	HNEFATAFL_UI_AIM
/*
 * See the progress printing function.
 *
 * If we squeeze the multi-byte chars into strings, these are the
 * resulting three strings:
 *
 *	"ᚠᚢᚦᚨᚱᚲᚷᚹ",
 *	"ᚺᚾᛁᛃᛇᛈᛉᛊ",
 *	"ᛏᛒᛖᛗᛚᛜᛟᛞ"
 *
 * Also note the variations:
 *
 *	ᚠ	ᚢ	ᚦ	ᚨ	ᚱ	ᚲ	ᚷ	ᚹ
 *	f	u	þ	a	r	k/c	g	w
 *
 *	ᚺ/ᚻ	ᚾ	ᛁ	ᛃ	ᛇ	ᛈ	ᛉ	ᛊ/ᛋ
 *	h	n	i	j	ï/æ	p	z	s
 *
 *	ᛏ	ᛒ	ᛖ	ᛗ	ᛚ	ᛜ/ᛝ/□	ᛟ	ᛞ
 *	t	b	e	m	l	ŋ	o	d
 *
 * NOTE:	The lengths are hard-coded.
 *
 * -	3 == uic->progress_linec
 *	(Maximum 3 lines to print.)
 * -	8 == uic->chars_print_max
 *	(Maximum 8 characters to print, note that each character is a
 *	multi-byte character, id est a C-string.)
 */
/*@observer@*/
/*@unchecked@*/
static
const char	* cli_progress_mbch[3][8] =
	{
		{
			"ᚠ", "ᚢ", "ᚦ", "ᚨ", "ᚱ", "ᚲ", "ᚷ", "ᚹ"
		},

		{
			"ᚺ", "ᚾ", "ᛁ", "ᛃ", "ᛇ", "ᛈ", "ᛉ", "ᛊ"
		},

		{
			"ᛏ", "ᛒ", "ᛖ", "ᛗ", "ᛚ", "ᛜ", "ᛟ", "ᛞ"
		}
	};
#endif	/* HNEFATAFL_UI_AIM */

static
enum HNEF_FR
hnef_print_unknown_cmd (
/*@in@*/
/*@notnull@*/
	const struct gleip_lang	* const GLEIP_RSTR lang
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout@*/
{
	const char	* HNEF_RSTR value	= NULL;

	assert	(NULL != lang);

	value	= gleip_lang_get(lang, HNEF_LK_CMD_UNK);
	if	(NULL == value
	||	hnef_line_empty(value))
	{
		HNEF_BOOL	fail	= HNEF_FALSE;
		fail = EOF == fputc(HNEF_CMD_EE_HINT, stdout)	|| fail;
		fail = EOF == fputc('\n', stdout)		|| fail;
		return	fail
			? HNEF_FR_FAIL_IO_WRITE
			: HNEF_FR_SUCCESS;
	}
	else
	{
		return	EOF == puts(value)
			? HNEF_FR_FAIL_IO_WRITE
			: HNEF_FR_SUCCESS;
	}
}

/*
 * `exists` is set to true if a help chapter for the char exists and was
 * printed.
 *
 * Give a true `brief` parameter to only print one line (the most
 * detailed).
 */
static
enum HNEF_FR
hnef_print_help_cli (
/*@in@*/
/*@notnull@*/
	const struct gleip_lang	* const GLEIP_RSTR lang,
	const char		ch,
	const HNEF_BOOL		brief,
/*@out@*/
/*@notnull@*/
	HNEF_BOOL		* const HNEF_RSTR exists
	)
/*@globals fileSystem, stdout@*/
/*@modifies fileSystem, stdout, * exists@*/
{
	HNEF_BOOL	fail	= HNEF_FALSE;

	assert	(NULL != lang);

	* exists	= HNEF_TRUE;

	switch	(ch)
	{
		case	HNEF_CMD_COPY:
			fail	= printf("%c\t\t\t%s\n", ch,
				gleip_lang_getany(lang,
				HNEF_LK_HELP_CLI_COPY)) < 0
				|| fail;
			break;
		case	HNEF_CMD_ENVVAR:
			if	(!brief)
			{
				fail	= printf("%c\t\t\t%s\n", ch,
					gleip_lang_getany(lang,
					HNEF_LK_HELP_CLI_ENVVAR0)) < 0
					|| fail;
			}
			fail	= printf("%c <v>\t\t\t%s\n", ch,
				gleip_lang_getany(lang,
				HNEF_LK_HELP_CLI_ENVVAR1)) < 0
				|| fail;
			break;
		case	HNEF_CMD_GAME_LOAD:
			fail	= printf("%c <f>\t\t\t%s\n", ch,
				gleip_lang_getany(lang,
				HNEF_LK_HELP_CLI_GAME_LOAD)) < 0
				|| fail;
			break;
		case	HNEF_CMD_GAME_NEW:
			fail	= printf("%c\t\t\t%s\n", ch,
				gleip_lang_getany(lang,
				HNEF_LK_HELP_CLI_GAME_NEW)) < 0
				|| fail;
			break;
		case	HNEF_CMD_GAME_SAVE:
			fail	= printf("%c <f>\t\t\t%s\n", ch,
				gleip_lang_getany(lang,
				HNEF_LK_HELP_CLI_GAME_SAVE)) < 0
				|| fail;
			break;
		case	HNEF_CMD_GAME_UNDO:
			if	(!brief)
			{
				fail	= printf("%c\t\t\t%s\n", ch,
					gleip_lang_getany(lang,
					HNEF_LK_HELP_CLI_GAME_UNDO0))
					< 0
					|| fail;
			}
			fail	= printf("%c <n>\t\t\t%s\n", ch,
				gleip_lang_getany(lang,
				HNEF_LK_HELP_CLI_GAME_UNDO1)) < 0
				|| fail;
			break;
		case	HNEF_CMD_HELP:
			if	(!brief)
			{
				fail	= printf("%c\t\t\t%s\n", ch,
					gleip_lang_getany(lang,
					HNEF_LK_HELP_CLI_HELP0)) < 0
					|| fail;
			}
			fail	= printf("%c <c>\t\t\t%s\n", ch,
				gleip_lang_getany(lang,
				HNEF_LK_HELP_CLI_HELP1)) < 0
				|| fail;
			break;
		case	HNEF_CMD_LANG:
			fail	= printf("%c <f>\t\t\t%s\n", ch,
				gleip_lang_getany(lang,
				HNEF_LK_HELP_CLI_LANG)) < 0
				|| fail;
			break;
		case	HNEF_CMD_MOVE:
			if	(!brief)
			{
				fail	= printf("%c\t\t\t%s\n", ch,
					gleip_lang_getany(lang,
					HNEF_LK_HELP_CLI_MOVE0)) < 0
					|| fail;
				fail	= printf("%c <x> <y>\t\t%s\n",
					ch, gleip_lang_getany
					(lang, HNEF_LK_HELP_CLI_MOVE2))
					< 0
					|| fail;
			}
			fail	= printf("%c <x1> <y1> <x2> <y2>\t%s\n",
				ch, gleip_lang_getany(lang,
				HNEF_LK_HELP_CLI_MOVE4)) < 0
				|| fail;
			break;
		case	HNEF_CMD_PATH:
			if	(!brief)
			{
				fail	= printf("%c\t\t\t%s\n", ch,
					gleip_lang_getany(lang,
					HNEF_LK_HELP_CLI_PATH0)) < 0
					|| fail;
			}
			fail	= printf("%c <p>\t\t\t%s\n", ch,
				gleip_lang_getany(lang,
				HNEF_LK_HELP_CLI_PATH1)) < 0
				|| fail;
			break;
		case	HNEF_CMD_PLAYER:
			if	(!brief)
			{
				fail	= printf("%c\t\t\t%s\n", ch,
					gleip_lang_getany(lang,
					HNEF_LK_HELP_CLI_PLAYER0)) < 0
					|| fail;
				fail	= printf("%c <i>\t\t\t%s\n", ch,
					gleip_lang_getany(lang,
					HNEF_LK_HELP_CLI_PLAYER1)) < 0
					|| fail;
			}
			fail	= printf("%c <i> <t> <xyz>\t\t%s\n", ch,
				gleip_lang_getany(lang,
				HNEF_LK_HELP_CLI_PLAYER2)) < 0
				|| fail;
			if	(!brief)
			{
				fail	= printf("\t<t> = %c\t\t%s\n",
					HNEF_CMD_PLAYER_HUMAN,
					gleip_lang_getany(lang,
					HNEF_LK_HELP_CLI_PLAYER2_H))
					< 0
					|| fail;
			}
#ifdef	HNEFATAFL_UI_AIM
			if	(!brief)
			{
				fail	= printf("\t<t> = %c\t\t%s\n",
					HNEF_CMD_PLAYER_AIM,
					gleip_lang_getany(lang,
					HNEF_LK_HELP_CLI_PLAYER2_M))
					< 0
					|| fail;
				fail	= printf(
				"\t\t<x>\t%s\t[%hu, %hu]\t%c = %hu\n",
					gleip_lang_getany(lang,
					HNEF_LK_HELP_CLI_PLAYER2_M_X),
					HNEF_AIM_DEPTHMAX_MIN,
					HNEF_AIM_DEPTHMAX_MAX,
					HNEF_CMD_ARG_DEF,
					HNEF_AIM_DEPTH_DEF) < 0
					|| fail;
#ifdef	HNEFATAFL_UI_ZHASH
				fail	= printf(
				"\t\t<y>\t%s\t[%lu, ∞]\t%c = %lu\n",
					gleip_lang_getany(lang,
					HNEF_LK_HELP_CLI_PLAYER2_M_Y),
					(unsigned long)
						HNEF_ZHASH_MEM_TAB_MIN,
					HNEF_CMD_ARG_DEF,
					(unsigned long)
					hnef_zhash_mem_tab_def()) < 0
					|| fail;
				fail	= printf(
				"\t\t<z>\t%s\t[%lu, ∞]\t%c = %lu\n",
					gleip_lang_getany(lang,
					HNEF_LK_HELP_CLI_PLAYER2_M_Z),
					(unsigned long)
						HNEF_ZHASH_MEM_COL_MIN,
					HNEF_CMD_ARG_DEF,
					(unsigned long)
					hnef_zhash_mem_col_def()) < 0
					|| fail;
#endif	/* HNEFATAFL_UI_ZHASH */
			}
#endif	/* HNEFATAFL_UI_AIM */
			break;
		case	HNEF_CMD_PRINT:
			fail	= printf("%c\t\t\t%s\n", ch,
				gleip_lang_getany(lang,
				HNEF_LK_HELP_CLI_PRINT)) < 0
				|| fail;
			break;
		case	HNEF_CMD_QUIT:
			fail	= printf("%c\t\t\t%s\n", ch,
				gleip_lang_getany(lang,
				HNEF_LK_HELP_CLI_QUIT)) < 0
				|| fail;
			break;
		case	HNEF_CMD_RULES:
			fail	= printf("%c <f>\t\t\t%s\n", ch,
				gleip_lang_getany(lang,
				HNEF_LK_HELP_CLI_RULES)) < 0
				|| fail;
			break;
		case	HNEF_CMD_VERSION:
			fail	= printf("%c\t\t\t%s\n", ch,
				gleip_lang_getany(lang,
				HNEF_LK_HELP_CLI_VERSION)) < 0
				|| fail;
			break;
#ifdef	HNEFATAFL_UI_XLIB
		case	HNEF_CMD_XLIB:
			if	(!brief)
			{
				fail	= printf("%c\t\t\t%s\n", ch,
					gleip_lang_getany(lang,
					HNEF_LK_HELP_CLI_XLIB0)) < 0
					|| fail;
			}
			fail	= printf("%c <t>\t\t\t%s\n", ch,
				gleip_lang_getany(lang,
				HNEF_LK_HELP_CLI_XLIB1)) < 0
				|| fail;
			break;
#endif	/* HNEFATAFL_UI_XLIB */
		/*
		 * NOTE:	Help for these is not printed:
		 *		-	`HNEF_CMD_EE_NINE`
		 *		-	`HNEF_CMD_EE_HINT`
		 */
		default:
			* exists	= HNEF_FALSE;
			break;
	}
	return	fail
		? HNEF_FR_FAIL_IO_WRITE
		: HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_print_help_cli_all (
/*@in@*/
/*@notnull@*/
	const struct gleip_lang	* const GLEIP_RSTR lang
	)
/*@globals fileSystem, stdout@*/
/*@modifies fileSystem, stdout@*/
{
	const char	charv[] =
		{
			HNEF_CMD_PRINT,
			HNEF_CMD_PATH,
			HNEF_CMD_GAME_LOAD,
			HNEF_CMD_GAME_NEW,
			HNEF_CMD_HELP,
			HNEF_CMD_COPY,
			HNEF_CMD_LANG,
			HNEF_CMD_MOVE,
			HNEF_CMD_ENVVAR,
			HNEF_CMD_PLAYER,
			HNEF_CMD_QUIT,
			HNEF_CMD_RULES,
			HNEF_CMD_GAME_UNDO,
			HNEF_CMD_VERSION,
			HNEF_CMD_GAME_SAVE
#ifdef	HNEFATAFL_UI_XLIB
			,
			HNEF_CMD_XLIB
#endif	/* HNEFATAFL_UI_XLIB */
		};
	const size_t	charc	= sizeof(charv) / sizeof(* charv);
	HNEF_BOOL	fail	= HNEF_FALSE;
	size_t		i;

	assert	(NULL != lang);

	for	(i = 0; i < charc; ++i)
	{
		const char		ch	= charv[i];
		const HNEF_BOOL		brief	= HNEF_CMD_HELP != ch;
		HNEF_BOOL		exists	= HNEF_TRUE;
		const enum HNEF_FR	fr	= hnef_print_help_cli
					(lang, ch, brief, & exists);
		assert	(exists);
		if	(!hnef_fr_good(fr))
		{
			fail	= HNEF_TRUE;
		}
	}

	return	fail
		? HNEF_FR_FAIL_IO_WRITE
		: HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_parseline_help (
/*@in@*/
/*@notnull@*/
	const struct gleip_lang		* const GLEIP_RSTR lang,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line
	)
/*@globals fileSystem, stdout@*/
/*@modifies fileSystem, stdout@*/
{
	assert	(NULL != lang);
	assert	(NULL != line);

	if	(gleip_lines_size(line) > (size_t)1
	&&	!hnef_line_empty(gleip_lines_get_ro(line, (size_t)1)))
	{
		const char	* const GLEIP_RSTR str =
				gleip_lines_get_ro(line, (size_t)1);
		const char	ch = str[0];
		HNEF_BOOL	exists;
		(void)hnef_print_help_cli
				(lang, ch, HNEF_FALSE, & exists);
		if	(!exists)
		{
			(void)hnef_print_help_cli_all(lang);
		}
	}
	else
	{
		(void)hnef_print_help_cli_all	(lang);
	}
	return	HNEF_FR_SUCCESS;
}

/*
 * `pbit` / `sqbit` are the actual bits, not the UI bits. This function
 * handles conversion to UI bits before returning.
 */
static
char
hnef_print_board_piecech (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR game,
	const HNEF_BIT_U8	pbit,
	const HNEF_BIT_U8	sqbit
	)
/*@modifies nothing@*/
{
	unsigned short			index,
					index_ui;
	char				ch;
	const struct hnef_type_piece	* HNEF_RSTR tp	= NULL;
	HNEF_BIT_U8			pbit_ui;

	assert	(NULL != game);

	if	(HNEF_BIT_U8_EMPTY == pbit)
	{
		if	(HNEF_BIT_U8_EMPTY == sqbit)
		{
			return	CLI_BOARD_SQUARE_EMPTY;
		}
		else
		{
			const struct hnef_type_square
				* HNEF_RSTR ts	= hnef_type_square_get
						(game->rules, sqbit);
			const HNEF_BIT_U8
				sqbit_ui	= hnef_game_uibit_square
						(ts);
			ts			= hnef_type_square_get
						(game->rules, sqbit_ui);
			if	(ts->escape)
			{
				return	CLI_BOARD_SQUARE_ESCAPE;
			}
			else if	(HNEF_BIT_U8_EMPTY != ts->captures)
			{
				return	CLI_BOARD_SQUARE_SPECIAL;
			}
			else
			{
				unsigned short i;
				for	(i = 0; i < HNEF_TYPE_MAX; ++i)
				{
					const struct hnef_type_piece
						* const HNEF_RSTR tmp =
					& game->rules->type_pieces[i];
					if (((unsigned int)sqbit_ui
					& (unsigned int)tmp->noreturn)
					== (unsigned int)sqbit_ui)
					{
						return
					CLI_BOARD_SQUARE_SPECIAL;
					}
				}
				return	CLI_BOARD_SQUARE_ORDINARY;
			}
		}
	}

	index		= hnef_type_index_get(pbit);
	tp		= & game->rules->type_pieces[index];
	pbit_ui		= hnef_game_uibit_piece(tp);
	index_ui	= hnef_type_index_get(pbit_ui);

	/*
	 * NOTE:	We use ASCII code points to turn the piece's
	 *		index (in `rules->type_pieces`) to a unique
	 *		symbol. The indices are 0-7 since the first
	 *		index is 0 and there are a maximum of 8 pieces.
	 *		Then you just add the decimal value of the ASCII
	 *		code point.
	 *
	 *		`index_ui + 65` displays capital letters [A-H]
	 *		for a player.
	 *
	 *		`index_ui + 65 + 32` displays lower case letters
	 *		[a-h] for a player.
	 *
	 *		`index_ui + 49` displays numbers 1-8 for a
	 *		player.
	 */
	if	(0 == tp->owner)
	{
		ch	= (char)((int)index_ui + 65);
	}
	else
	{
		ch	= (char)((int)index_ui + 49);
	}

	return	ch;
}

static
HNEF_BOOL
hnef_listm_pos_exists (
/*@in@*/
/*@notnull@*/
	const struct hnef_listm	* const HNEF_RSTR moves,
	const unsigned short	pos
	)
/*@modifies nothing@*/
{
	size_t	i;

	assert	(NULL != moves);

	for	(i = 0; i < moves->elemc; ++i)
	{
		if	(moves->elems[i].dest == pos)
		{
			return	HNEF_TRUE;
		}
	}
	return	HNEF_FALSE;
}

/*
 * If `moves` is `NULL`, it prints the board showing the last move.
 *
 * If `moves` is `!NULL`, then it prints where the indicated piece can
 * move to. All `hnef_move->pos` in `moves` must equal each other, id
 * est they must be moves of the same piece. All the destinations in
 * `moves` are printed.
 *
 * The game must be valid.
 *
 * If `plain`, then `moves` and last move are ignored.
 */
static
enum HNEF_FR
hnef_print_board_moves (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game,
/*@in@*/
/*@null@*/
	const struct hnef_listm	* const HNEF_RSTR moves,
	const HNEF_BOOL		plain
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout, * game@*/
{
	HNEF_BOOL		fail			= HNEF_FALSE;
	struct hnef_ruleset	* HNEF_RSTR rules	= NULL;
	struct hnef_board	* HNEF_RSTR board	= NULL;
	struct hnef_moveh	* HNEF_RSTR last	= NULL;
	unsigned short		x,
				y,
				pos_piece;

	last	= game->movehist->elemc > 0
		? (& game->movehist->elems[game->movehist->elemc - 1])
		: NULL;

	assert	(NULL != game);
	assert	(NULL != game->board);
	assert	(NULL != game->rules);
	assert	(NULL != game->rules->pieces);
	assert	(NULL != game->rules->squares);
/*@i2@*/\
	assert	(hnef_rvalid_good(hnef_game_valid(game)));

	rules	= game->rules;
	board	= game->board;

#ifndef	NDEBUG
	/*
	 * All `hnef_move->pos` in `moves` must equal each other (not
	 * because this function fails otherwise, but because if not,
	 * then it's a bug in `hnef_game_moves_get_pos()`).
	 */
	if	(NULL != moves)
	{
		unsigned short	pos	= HNEF_BOARDPOS_NONE;
		size_t		i;
		for	(i = 0; i < moves->elemc; ++i)
		{
			struct hnef_move	* const HNEF_RSTR move =
						& moves->elems[i];
			if	(HNEF_BOARDPOS_NONE == pos)
			{
				pos	= move->pos;
			}
			else
			{
				assert	(move->pos == pos);
			}
		}
	}
#endif	/* NDEBUG */

	pos_piece	= (unsigned short)
			((	NULL != moves
			&&	moves->elemc > 0)
			? moves->elems[0].pos
			: HNEF_BOARDPOS_NONE);

	fail	= EOF == fputs("    ", stdout)		|| fail;
	for	(x = 0; x < rules->bwidth; ++x)
	{
		fail	= printf("%2hu ", x) < 0	|| fail;
	}
	fail	= EOF == puts("")			|| fail;

	fail	= EOF == fputs("   +", stdout)		|| fail;
	for	(x = 0; x < rules->bwidth; ++x)
	{
		fail	= EOF == fputs("---", stdout)	|| fail;
	}
	fail	= EOF == puts("+")			|| fail;

	for	(y = 0; y < rules->bheight; ++y)
	{
		fail	= printf("%2hu |", y) < 0	|| fail;
		for	(x = 0; x < rules->bwidth; ++x)
		{
			const unsigned short	pos = (unsigned short)
						(y * rules->bwidth + x);
			const HNEF_BIT_U8	pbit =
						board->pieces[pos];
			char	ch_l,
				ch_r,
				ch	= hnef_print_board_piecech(game,
					pbit,
					rules->squares[pos]);

			if	(plain)
			{
				ch_l	= ch_r	= CLI_BOARD_ORDINARY_LR;
			}
			else if	(NULL == moves)
			{
				if	(NULL == last)
				{
					ch_l	= ch_r	=
						CLI_BOARD_ORDINARY_LR;
				}
				else
				{
					if	(pos == last->pos)
					{
						ch_l =
						CLI_BOARD_LAST_POS_L;
						ch_r =
						CLI_BOARD_LAST_POS_R;
					}
					else if	(pos == last->dest)
					{
						ch_l =
						CLI_BOARD_LAST_DEST_L;
						ch_r =
						CLI_BOARD_LAST_DEST_R;
					}
					else
					{
						ch_l	= ch_r	=
						CLI_BOARD_ORDINARY_LR;
					}
				}
			}
			else
			{
				if	(pos_piece == pos)
				{
					ch_l	= CLI_BOARD_LAST_DEST_L;
					ch_r	= CLI_BOARD_LAST_DEST_R;
				}
				else if	(hnef_listm_pos_exists
							(moves, pos))
				{
					ch_l	= CLI_BOARD_LIST_DEST_L;
					ch_r	= CLI_BOARD_LIST_DEST_R;
				}
				else
				{
					ch_l	= ch_r	=
						CLI_BOARD_ORDINARY_LR;
				}
			}
			fail	= printf("%c%c%c", ch_l, ch, ch_r) < 0
				|| fail;
		}
		fail		= EOF == puts("|")
				|| fail;
	}

	/*
	 * This little code repetition isn't worth its own function.
	 */
	fail	= EOF == fputs("   +", stdout)		|| fail;
	for	(x = 0; x < rules->bwidth; ++x)
	{
		fail	= EOF == fputs("---", stdout)	|| fail;
	}
	fail	= EOF == puts("+")			|| fail;

	return	fail
		? HNEF_FR_FAIL_IO_WRITE
		: HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_print_board (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game,
	const HNEF_BOOL		plain
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout, * game@*/
{
	assert	(NULL != game);
	return	hnef_print_board_moves(game, NULL, plain);
}

static
enum HNEF_FR
hnef_parseline_print_board (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout, * ui@*/
{
	assert	(NULL != ui);

	if	(!hnef_rvalid_good(hnef_game_valid(ui->game)))
	{
		(void)hnef_print_langkey(ui->lang,
				HNEF_LK_CMD_GAME_INVALID, stdout);
		return	HNEF_FR_SUCCESS;
	}

	return	hnef_print_board_moves(ui->game, NULL, HNEF_TRUE);
}

/*
 * If XLib fails to open the display, and `quit_upon_success` is true,
 * then the program does not quit. In all other cases, the program
 * quits.
 *
 * Errors are printed to `out`.
 */
static
enum HNEF_FR
hnef_run_xlib_try (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@in@*/
/*@null@*/
	const char	* const HNEF_RSTR ui_theme,
/*@in@*/
/*@notnull@*/
	FILE		* const HNEF_RSTR out,
	const HNEF_BOOL	quit_upon_success
	)
#ifdef	HNEFATAFL_UI_XLIB
/*@globals errno, fileSystem, internalState, stderr, stdout@*/
/*@modifies errno, fileSystem, internalState, stderr, stdout@*/
#else	/* HNEFATAFL_UI_XLIB */
/*@globals fileSystem@*/
/*@modifies fileSystem@*/
#endif	/* HNEFATAFL_UI_XLIB */
/*@modifies * ui, * out@*/
{
	assert	(NULL != ui);
	assert	(NULL != out);

#ifdef	HNEFATAFL_UI_XLIB

	{
		enum HNEF_FRX		frx = HNEF_FRX_SUCCESS;
		const enum HNEF_FR	fr = hnef_run_xlib
					(ui, ui_theme, & frx);
		if	(HNEF_FR_FAIL_OTHER == fr)
		{
			assert	(!hnef_frx_good(frx));
			if	(quit_upon_success
			&&	HNEF_FRX_FAIL_DISPLAY != frx)
			{
				ui->quit	= HNEF_TRUE;
			}
			/*
			 * If XLib fails it's probably not fatal, so
			 * just print it and move on. Note that `frx`
			 * must have been set if `HNEF_FR_FAIL_OTHER`.
			 */
			(void)hnef_print_fail_frx(ui->lang, frx, out);
			return	HNEF_FR_SUCCESS;
		}
/*@i1@*/\
		else if	(!hnef_fr_good(fr))
		{
			return	fr;
		}
	}

#else	/* HNEFATAFL_UI_XLIB */

	if	(NULL == ui_theme) {}	/* GCC unused parameter */

	(void)hnef_print_langkey(ui->lang, HNEF_LK_CMD_XLIB_COMP, out);

#endif	/* HNEFATAFL_UI_XLIB */

	if	(quit_upon_success)
	{
		ui->quit	= HNEF_TRUE;
	}

	return	HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_parseline_xlib (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line
	)
#ifdef	HNEFATAFL_UI_XLIB
/*@globals errno, fileSystem, internalState, stderr, stdout@*/
/*@modifies errno, fileSystem, internalState, stderr, stdout@*/
#else	/* HNEFATAFL_UI_XLIB */
/*@globals fileSystem, stdout@*/
/*@modifies fileSystem, stdout@*/
#endif	/* HNEFATAFL_UI_XLIB */
/*@modifies * ui@*/
{
	const char	* const HNEF_RSTR ui_theme =
			gleip_lines_size(line) > (size_t)1
			? gleip_lines_get_ro(line, (size_t)1)
			: NULL;

	assert	(NULL != ui);
	assert	(NULL != line);

	return	hnef_run_xlib_try(ui, ui_theme, stdout, HNEF_FALSE);
}

static
enum HNEF_FR
hnef_print_lines (
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR lines,
	const HNEF_BOOL			markers
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout@*/
{
	HNEF_BOOL	fail	= HNEF_FALSE;
	size_t		i;

	assert	(NULL != lines);

	for	(i = 0; i < gleip_lines_size(lines); ++i)
	{
		if	(markers)
		{
			fail	= EOF == fputc('[', stdout)	|| fail;
		}
		fail	= EOF == fputs
			(gleip_lines_get_ro(lines, i), stdout)	|| fail;
		if	(markers)
		{
			fail	= EOF == fputc(']', stdout)	|| fail;
		}
		fail	= EOF == fputc
		(i + 1 < gleip_lines_size(lines) ? ' ' : '\n', stdout)
			|| fail;
	}
	return	fail
		? HNEF_FR_FAIL_IO_WRITE
		: HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_parseline_move (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout, * ui@*/
{
	const struct hnef_ruleset	* HNEF_RSTR rules	= NULL;
	enum HNEF_FR			fr	= HNEF_FR_SUCCESS;
	HNEF_BOOL			legal;
	unsigned short			pos;

	assert	(NULL != ui);
	assert	(NULL != line);

	if	(!hnef_rvalid_good(hnef_game_valid(ui->game)))
	{
		(void)hnef_print_langkey(ui->lang,
				HNEF_LK_CMD_GAME_INVALID, stdout);
		return	HNEF_FR_SUCCESS;
	}

	rules	= ui->game->rules;

	if	(gleip_lines_size(line) >= (size_t)5)
	{
		unsigned short	x1	= HNEF_BOARDPOS_NONE,
				y1	= HNEF_BOARDPOS_NONE,
				x2	= HNEF_BOARDPOS_NONE,
				y2	= HNEF_BOARDPOS_NONE,
				dest;
		if	(!hnef_texttoushort(gleip_lines_get_ro(line,
						(size_t)1), & x1)
		||	!hnef_texttoushort(gleip_lines_get_ro(line,
						(size_t)2), & y1)
		||	!hnef_texttoushort(gleip_lines_get_ro(line,
						(size_t)3), & x2)
		||	!hnef_texttoushort(gleip_lines_get_ro(line,
						(size_t)4), & y2)
		||	x1 >= rules->bwidth
		||	y1 >= rules->bheight
		||	x2 >= rules->bwidth
		||	y2 >= rules->bheight)
		{
			(void)hnef_print_langkey(ui->lang,
					HNEF_LK_CMD_MOVE_COORD, stdout);
			return	HNEF_FR_SUCCESS;
		}

		pos	= (unsigned short)(y1 * rules->bwidth + x1);
		dest	= (unsigned short)(y2 * rules->bwidth + x2);
		assert	(pos < rules->opt_blen);
		assert	(dest < rules->opt_blen);

		fr	= hnef_game_move(ui->game, pos, dest, & legal);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}

		if	(legal)
		{
			(void)hnef_print_board(ui->game, HNEF_FALSE);
		}
		else
		{
			(void)hnef_print_langkey(ui->lang,
				HNEF_LK_CMD_MOVE_ILLEGAL, stdout);
		}
		return	HNEF_FR_SUCCESS;
	}
	else if	(gleip_lines_size(line) >= (size_t)3)
	{
		unsigned short	x	= HNEF_BOARDPOS_NONE,
				y	= HNEF_BOARDPOS_NONE,
				ignored	= HNEF_PLAYER_UNINIT;

		if	(hnef_game_over(ui->game, & ignored))
		{
			(void)hnef_print_langkey(ui->lang,
					HNEF_LK_CMD_GAME_OVER, stdout);
			return	HNEF_FR_SUCCESS;
		}

		if	(!hnef_texttoushort(gleip_lines_get_ro(line,
							(size_t)1), & x)
		||	!hnef_texttoushort(gleip_lines_get_ro(line,
							(size_t)2), & y)
		||	x >= rules->bwidth
		||	y >= rules->bheight)
		{
			(void)hnef_print_langkey(ui->lang,
					HNEF_LK_CMD_MOVE_COORD, stdout);
			return	HNEF_FR_SUCCESS;
		}
		pos	= (unsigned short)(y * rules->bwidth + x);
		assert	(pos < rules->opt_blen);

		assert	(ui->opt_movelist->elemc < (size_t)1);
		fr	= hnef_game_moves_get_pos(ui->game,
				ui->opt_movelist, pos);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}

		(void)hnef_print_board_moves(ui->game, ui->opt_movelist,
							HNEF_FALSE);
		hnef_listm_clear	(ui->opt_movelist);

		return	HNEF_FR_SUCCESS;
	}
	else
	{
		/*
		 * Print board with last move.
		 */
		(void)hnef_print_board(ui->game, HNEF_FALSE);
		return	HNEF_FR_SUCCESS;
	}
}

/*
 * Callback function. `frx` is always ignored and should be `NULL`.
 * Always returns success.
 */
static
enum HNEF_FR
hnef_callback_print_langkey (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const ui,
/*@in@*/
/*@null@*/
	enum HNEF_FRX	* const frx,
/*@in@*/
/*@notnull@*/
	const char	* const key
	)
/*@globals fileSystem, stdout@*/
/*@modifies fileSystem, stdout@*/
{
	assert	(NULL != ui);
	assert	(NULL == frx);
	assert	(NULL != key);

	(void)hnef_print_langkey(ui->lang, key, stdout);
	return	HNEF_FR_SUCCESS;
}

/*
 * Callback function. `frx` is always ignored and should be `NULL`.
 * Always returns success.
 */
static
enum HNEF_FR
hnef_callback_print_fr (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const ui,
/*@in@*/
/*@null@*/
	enum HNEF_FRX		* const frx,
	const enum HNEF_FR	fr
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout@*/
{
	assert	(NULL != ui);
	assert	(NULL == frx);
	assert	(HNEF_FR_SUCCESS != fr);

	(void)hnef_print_fail_fr(ui->lang, fr, stdout);
	return	HNEF_FR_SUCCESS;
}

/*
 * Callback function. `frx` is always ignored and should be `NULL`.
 * Always returns success.
 */
static
enum HNEF_FR
hnef_callback_print_fail_rread (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
/*@in@*/
/*@null@*/
	enum HNEF_FRX		* const HNEF_RSTR frx,
	const enum HNEF_RREAD	rread
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout@*/
{
	assert	(NULL != ui);
	assert	(NULL == frx);
	assert	(HNEF_RREAD_SUCCESS != rread);

	(void)hnef_print_fail_rread(ui->lang, rread, stdout);
	return	HNEF_FR_SUCCESS;
}

/*
 * Callback function. `frx` is always ignored and should be `NULL`.
 * Always returns success.
 */
static
enum HNEF_FR
hnef_callback_print_fail_rvalid (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
/*@in@*/
/*@null@*/
	enum HNEF_FRX		* const HNEF_RSTR frx,
	const enum HNEF_RVALID	rvalid
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout@*/
{
	assert	(NULL != ui);
	assert	(NULL == frx);
	assert	(HNEF_RVALID_SUCCESS != rvalid);

	(void)hnef_print_fail_rvalid(ui->lang, rvalid, stdout);
	return	HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_uic_parseline_game_load (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line
	)
/*@globals errno, fileSystem, internalState, stdout@*/
/*@modifies errno, fileSystem, internalState, stdout, * ui@*/
{
	enum HNEF_FR	fr	= HNEF_FR_SUCCESS;
	HNEF_BOOL	success;

	assert	(NULL != ui);
	assert	(NULL != line);

	fr	= hnef_ui_parseline_game_load(ui, line, & success,
			hnef_callback_print_langkey,
			hnef_callback_print_fr,
			NULL);
	if	(!hnef_fr_good(fr))
	{
		return	fr;
	}

	if	(success)
	{
		(void)hnef_print_board(ui->game, HNEF_FALSE);
	}

	return	HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
/* SPLint stdout is used *//*@i1@*/\
hnef_uic_parseline_game_save (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout, * ui@*/
{
	assert	(NULL != ui);
	assert	(NULL != line);
	return	hnef_ui_parseline_game_save(ui, line,
			hnef_callback_print_langkey,
			hnef_callback_print_fr,
			NULL);
}

static
enum HNEF_FR
hnef_uic_parseline_game_undo (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout, * ui@*/
{
	enum HNEF_FR	fr	= HNEF_FR_SUCCESS;
	HNEF_BOOL	success;

	assert	(NULL != ui);
	assert	(NULL != line);

	fr	= hnef_ui_parseline_game_undo(ui, line, & success,
			hnef_callback_print_langkey,
			NULL);
	if	(!hnef_fr_good(fr))
	{
		return	fr;
	}

	if	(success)
	{
		(void)hnef_print_board(ui->game, HNEF_FALSE);
	}

	return	HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_uic_parseline_game_new (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout, * ui@*/
{
	enum HNEF_FR	fr	= HNEF_FR_SUCCESS;

	assert	(NULL != ui);

	fr	= hnef_ui_parseline_game_new(ui,
			hnef_callback_print_langkey,
			NULL);
	if	(!hnef_fr_good(fr))
	{
		return	fr;
	}

	if	(hnef_rvalid_good(hnef_game_valid(ui->game)))
	{
		(void)hnef_print_board(ui->game, HNEF_FALSE);
	}

	return	HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_callback_print_str (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@in@*/
/*@null@*/
	enum HNEF_FRX	* const HNEF_RSTR frx,
/*@in@*/
/*@notnull@*/
	const char	* const HNEF_RSTR msg
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout@*/
{
	assert	(NULL != ui);
	assert	(NULL != msg);
	assert	(NULL == frx);

	(void)puts(msg);

	return	HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
/* SPLint stdout is used *//*@i1@*/\
hnef_uic_parseline_lang_filename (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const char	* const HNEF_RSTR filename,
	const HNEF_BOOL	try_absolute
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout, * ui@*/
{
	assert	(NULL != ui);
	assert	(NULL != filename);

	return	hnef_ui_parseline_lang_filename
			(ui, filename, try_absolute,
			hnef_callback_print_langkey,
			hnef_callback_print_fr,
			hnef_callback_print_str,
			NULL);
}

static
enum HNEF_FR
/* SPLint stdout is used *//*@i1@*/\
hnef_uic_parseline_lang (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
	const HNEF_BOOL			try_absolute
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout, * ui@*/
{
	assert	(NULL != ui);
	assert	(NULL != line);

	return	hnef_ui_parseline_lang(ui, line, try_absolute,
			hnef_callback_print_langkey,
			hnef_callback_print_fr,
			hnef_callback_print_str,
			NULL);
}

static
enum HNEF_FR
hnef_callback_print_lines (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@null@*/
	enum HNEF_FRX			* const HNEF_RSTR frx,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR lines
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout@*/
{
	assert	(NULL != ui);
	assert	(NULL != lines);
	assert	(NULL == frx);

	(void)hnef_print_lines(lines, HNEF_TRUE);

	return	HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
/* SPLint stdout is used *//*@i1@*/\
hnef_uic_parseline_rules_filename (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const char		* const HNEF_RSTR filename,
	const HNEF_BOOL		try_absolute
	)
/*@globals errno, fileSystem, internalState, stdout@*/
/*@modifies errno, fileSystem, internalState, stdout, * ui@*/
{
	assert	(NULL != ui);
	assert	(NULL != filename);

	return	hnef_ui_parseline_rules_filename
			(ui, filename, try_absolute,
			hnef_callback_print_langkey,
			hnef_callback_print_fr,
			hnef_callback_print_fail_rread,
			hnef_callback_print_fail_rvalid,
			hnef_callback_print_lines,
			NULL);
}

static
enum HNEF_FR
/* SPLint stdout is used *//*@i1@*/\
hnef_uic_parseline_rules (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
	const HNEF_BOOL			try_absolute
	)
/*@globals errno, fileSystem, internalState, stdout@*/
/*@modifies errno, fileSystem, internalState, stdout, * ui@*/
{
	assert	(NULL != ui);
	assert	(NULL != line);

	return	hnef_ui_parseline_rules(ui, line, try_absolute,
			hnef_callback_print_langkey,
			hnef_callback_print_fr,
			hnef_callback_print_fail_rread,
			hnef_callback_print_fail_rvalid,
			hnef_callback_print_lines,
			NULL);
}

static
enum HNEF_FR
hnef_print_player (
/*@in@*/
/*@notnull@*/
	const struct hnef_ui	* const HNEF_RSTR ui,
	const unsigned short	player_index
	)
/*@globals fileSystem, stdout@*/
/*@modifies fileSystem, stdout@*/
{
	struct hnef_control	* HNEF_RSTR control	= NULL;
	HNEF_BOOL		fail			= HNEF_FALSE;

	assert	(NULL != ui);
	assert	(player_index < ui->game->playerc);

	control	= ui->controlv[player_index];

	switch	(control->type)
	{
		case	HNEF_CONTROL_TYPE_HUMAN:
			fail	= printf("%hu\t%c\n", player_index,
					HNEF_CMD_PLAYER_HUMAN) < 0;
			break;
#ifdef	HNEFATAFL_UI_AIM
		case	HNEF_CONTROL_TYPE_AIM:
#ifdef	HNEFATAFL_UI_ZHASH
			fail	= printf("%hu\t%c\t%hu\t%lu\t%lu\n",
					player_index,
					HNEF_CMD_PLAYER_AIM,
					control->aiminimax_depth,
			(unsigned long)control->aiminimax_mem_tab,
			(unsigned long)control->aiminimax_mem_col) < 0;
#else	/* HNEFATAFL_UI_ZHASH */
			fail	= printf("%hu\t%c\t%hu\n", player_index,
					HNEF_CMD_PLAYER_AIM,
					control->aiminimax_depth) < 0;
#endif	/* HNEFATAFL_UI_ZHASH */
			break;
#endif	/* HNEFATAFL_UI_AIM */
		default:
			assert	(HNEF_FALSE);
			break;
	}
	return	fail
		? HNEF_FR_FAIL_IO_WRITE
		: HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_print_players (
/*@in@*/
/*@notnull@*/
	const struct hnef_ui	* const HNEF_RSTR ui
	)
/*@globals fileSystem, stdout@*/
/*@modifies fileSystem, stdout@*/
{
	unsigned short	i;
	enum HNEF_FR	fr	= HNEF_FR_SUCCESS;

	assert	(NULL != ui);

	for	(i = 0; i < ui->game->playerc; ++i)
	{
		const enum HNEF_FR	fr2 = hnef_print_player(ui, i);
		if	(!hnef_fr_good(fr2)
		&&	hnef_fr_good(fr))
		{
			fr	= fr2;
		}
	}
	return	fr;
}

static
enum HNEF_FR
hnef_callback_print_player (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const ui,
	const unsigned short	player_index,
/*@in@*/
/*@null@*/
	enum HNEF_FRX		* const frx
	)
/*@globals fileSystem, stdout@*/
/*@modifies fileSystem, stdout@*/
{
	assert	(NULL != ui);
	assert	(player_index < ui->game->playerc);
	assert	(NULL == frx);

	(void)hnef_print_player(ui, player_index);

	return	HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_callback_print_players (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const ui,
/*@in@*/
/*@null@*/
	enum HNEF_FRX	* const frx
	)
/*@globals fileSystem, stdout@*/
/*@modifies fileSystem, stdout@*/
{
	assert	(NULL != ui);
	assert	(NULL == frx);

	(void)hnef_print_players(ui);

	return	HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
/* SPLint stdout is used *//*@i2@*/\
hnef_uic_parseline_player (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line
	)
/*@globals errno, fileSystem, internalState, stdout@*/

/*@modifies errno, fileSystem, stdout, * ui@*/
#ifdef	HNEFATAFL_UI_ZHASH
/*@modifies internalState@*/
#endif	/* HNEFATAFL_UI_ZHASH */
{
	assert	(NULL != ui);
	assert	(NULL != line);

	return	hnef_ui_parseline_player(ui, line,
			hnef_callback_print_langkey,
			hnef_callback_print_player,
			hnef_callback_print_players,
			NULL);
}

static
enum HNEF_FR
hnef_uic_parseline_envvar (
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout@*/
{
	assert	(NULL != line);

	if	(gleip_lines_size(line) > (size_t)1)
	{
		(void)hnef_print_envvar_single
			(gleip_lines_get_ro(line, (size_t)1),
			SIZET_MAX);
	}
	else
	{
		(void)hnef_print_envvar();
	}
	return	HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_uic_print_path (
	const unsigned short	pathnum,
/*@in@*/
/*@notnull@*/
/*@temp@*/
	const char		* const HNEF_RSTR path_descr,
/*@in@*/
/*@null@*/
/*@temp@*/
	const char		* const HNEF_RSTR path,
	const size_t		maxlen
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout@*/
{
	HNEF_BOOL	fail	= HNEF_FALSE;
	int		len;

	assert	(NULL != path_descr);

	fail	= printf("%hu.\t", pathnum) < 0			|| fail;
	len	= printf("%s", path_descr);
	if	(len >= 0)
	{
		size_t	tabs	= (size_t)hnef_textlen_tabs
				((size_t)len, maxlen);

		while	(tabs-- > 0)
		{
			fail	= EOF == fputc('\t', stdout)	|| fail;
		}
	}
	else
	{
		fail	= HNEF_TRUE;
	}

	fail		= EOF == fputs("= ", stdout)		|| fail;
	if	(NULL == path)
	{
		fail	= EOF == puts(HNEF_UI_VAR_UNSET)	|| fail;
	}
	else
	{
		fail	= printf("\"%s\"\n", path) < 0		|| fail;
	}

	return	fail
		? HNEF_FR_FAIL_IO_WRITE
		: HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_uic_parseline_path (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout, * ui@*/
{
	const char	* HNEF_RSTR patharg	= NULL;

	assert	(NULL != ui);
	assert	(NULL != line);

	if	(gleip_lines_size(line) < (size_t)2
	||	hnef_line_empty(patharg =
				gleip_lines_get_ro(line, (size_t)1)))
	{
		/*
		 * Print all the paths from highest to lowest priority.
		 * It would be unnecessary to have a dynamically
		 * allocated string as short as `pathcmdstr` in `ui`, so
		 * just do it like this.
		 */
		const size_t	maxlen	= strlen(HNEF_UI_ENVVAR_PATH);
		char		pathcmdstr[]	= "\0\0";

		pathcmdstr[0]		= HNEF_CMD_PATH;
		(void)hnef_uic_print_path((unsigned short)1,
					pathcmdstr,
			hnef_line_empty(ui->filefind->abs_path_cmd)
				? NULL
				: ui->filefind->abs_path_cmd,
					maxlen);

		pathcmdstr[0]		= '-';
		pathcmdstr[1]		= HNEF_CMD_PATH;
		(void)hnef_uic_print_path((unsigned short)2,
					pathcmdstr,
					ui->filefind->abs_path,
					maxlen);

		(void)hnef_uic_print_path((unsigned short)3,
					HNEF_UI_ENVVAR_PATH,
					getenv(HNEF_UI_ENVVAR_PATH),
					maxlen);
		return	HNEF_FR_SUCCESS;
	}

	assert	(NULL != patharg);
	assert	(!hnef_line_empty(patharg));

/*@i1@*/\
	return	gleip_line_cpy_b(& ui->filefind->abs_path_cmd, patharg)
		? HNEF_FR_SUCCESS
		: HNEF_FR_FAIL_ALLOC;
}

/*
 * Reads a line from file and parses it.
 *
 * Whatever the file is, it's human input (an RC file or `stdin`).
 *
 * The game object may be invalid here due to a ruleset not having been
 * read yet.
 *
 * -	If `restricted`, some commands are not allowed. This is used
 *	when reading RC files.
 * -	If `suppress_lang`, the `l` command is ignored.
 * -	If `suppress_rules`, the `r` command is ignored.
 * -	If `suppress_path`, the `d` command is ignored.
 */
static
enum HNEF_FR
hnef_parseline (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	struct gleip_lines	* const GLEIP_RSTR line,
/*@in@*/
/*@notnull@*/
	FILE			* const HNEF_RSTR file,
	const HNEF_BOOL		restricted,
	const HNEF_BOOL		suppress_lang,
	const HNEF_BOOL		suppress_rules,
	const HNEF_BOOL		suppress_path,
	const HNEF_BOOL		suppress_game_load
	)
#ifdef	HNEFATAFL_UI_XLIB
/*@globals errno, fileSystem, internalState, stderr, stdin, stdout@*/
/*@modifies errno, fileSystem, internalState, stderr, stdout@*/
#else	/* HNEFATAFL_UI_XLIB */
/*@globals errno, fileSystem, internalState, stdin, stdout@*/
/*@modifies errno, fileSystem, internalState, stdout@*/
#endif	/* HNEFATAFL_UI_XLIB */

/*@modifies * ui, * line, * file@*/
{
	int	retval	= 1;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->lang);
	assert	(NULL != line);
	assert	(NULL != file);

	if	(stdin == file)
	{
		/*
		 * Don't print prompt if reading RC file.
		 */
		(void)fputs	(HNEF_UI_PROMPT, stdout);
		(void)fflush	(stdout);
	}

	gleip_lines_rm(line);
	retval	= gleip_lineread_interp_file_def(line, file, 0);
/*@i1@*/\
	if	(!retval)
	{
		return	HNEF_FR_FAIL_ALLOC;
	}
	if	(EOF == retval
/*@i1@*/\
	&&	ferror(file))
	{
		return	stdin == file
			?	HNEF_FR_FAIL_IO_READ
			:	HNEF_FR_FAIL_IO_FILE_READ;
	}

	if	(gleip_lines_size(line) < (size_t)1)
	{
		return	HNEF_FR_SUCCESS;
	}

	if	(stdin != file)
	{
		(void)fputs		(RC_PROMPT, stdout);
		(void)hnef_print_lines	(line, HNEF_TRUE);
	}

	if	(EOF == retval
/*@i1@*/\
	||	feof(file))
	{
		/*
		 * Setting `quit` here is inappropriate if we are
		 * reading an RC file, because they will always reach
		 * EOF and thus cause the program to quit.
		 */
		return	HNEF_FR_SUCCESS;
	}

	if	(!hnef_line_empty(gleip_lines_get(line, 0)))
	{
		switch	(gleip_lines_get(line, 0)[0])
		{
			case	HNEF_CMD_COPY:
				(void)hnef_print_copy();
				return	HNEF_FR_SUCCESS;
			case	HNEF_CMD_EE_NINE:
				(void)hnef_nine_print
				(gleip_lines_size(line) > (size_t)1
					? gleip_lines_get_ro
						(line, (size_t)1)
					: NULL);
				return	HNEF_FR_SUCCESS;
			case	HNEF_CMD_EE_HINT:
				(void)puts(hnef_nine_hint());
				return	HNEF_FR_SUCCESS;
			case	HNEF_CMD_ENVVAR:
				return	hnef_uic_parseline_envvar(line);
			case	HNEF_CMD_GAME_LOAD:
				if	(suppress_game_load)
				{
					(void)hnef_print_langkey
						(ui->lang,
						HNEF_LK_CMD_SUPPRESS,
						stdout);
					return	HNEF_FR_SUCCESS;
				}
				else
				{
					return
					hnef_uic_parseline_game_load
						(ui, line);
				}
			case	HNEF_CMD_GAME_NEW:
				return	hnef_uic_parseline_game_new(ui);
			case	HNEF_CMD_GAME_SAVE:
				if	(restricted)
				{
					(void)hnef_print_langkey
						(ui->lang,
						HNEF_LK_CMD_RESTRICT,
						stdout);
					return	HNEF_FR_SUCCESS;
				}
				else
				{
					return
					hnef_uic_parseline_game_save
						(ui, line);
				}
			case	HNEF_CMD_GAME_UNDO:
				return	hnef_uic_parseline_game_undo
						(ui, line);
			case	HNEF_CMD_HELP:
				return	hnef_parseline_help
					(ui->lang, line);
			case	HNEF_CMD_LANG:
				if	(suppress_lang)
				{
					(void)hnef_print_langkey
						(ui->lang,
						HNEF_LK_CMD_SUPPRESS,
						stdout);
					return	HNEF_FR_SUCCESS;
				}
				else
				{
					return	hnef_uic_parseline_lang
						(ui, line, !restricted);
				}
			case	HNEF_CMD_MOVE:
				return	hnef_parseline_move(ui, line);
			case	HNEF_CMD_PATH:
				if	(suppress_path)
				{
					(void)hnef_print_langkey
						(ui->lang,
						HNEF_LK_CMD_SUPPRESS,
						stdout);
					return	HNEF_FR_SUCCESS;
				}
				else
				{
					return	hnef_uic_parseline_path
						(ui, line);
				}
			case	HNEF_CMD_PLAYER:
				return	hnef_uic_parseline_player
						(ui, line);
			case	HNEF_CMD_PRINT:
				(void)hnef_parseline_print_board(ui);
				return	HNEF_FR_SUCCESS;
			case	HNEF_CMD_QUIT:
				ui->quit = HNEF_TRUE;
				return	HNEF_FR_SUCCESS;
			case	HNEF_CMD_RULES:
				if	(suppress_rules)
				{
					(void)hnef_print_langkey
						(ui->lang,
						HNEF_LK_CMD_SUPPRESS,
						stdout);
					return	HNEF_FR_SUCCESS;
				}
				else
				{
					return	hnef_uic_parseline_rules
						(ui, line, !restricted);
				}
			case	HNEF_CMD_VERSION:
				(void)hnef_print_version();
				return	HNEF_FR_SUCCESS;
			case	HNEF_CMD_XLIB:
				return	hnef_parseline_xlib(ui, line);
			default:
				(void)hnef_print_unknown_cmd(ui->lang);
				return	HNEF_FR_SUCCESS;
		}
	}
	return	HNEF_FR_SUCCESS;
}

/*
 * `parseline` as input from keyboard.
 */
static
enum HNEF_FR
hnef_parseline_stdin (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	struct gleip_lines	* const GLEIP_RSTR line
	)
#ifdef	HNEFATAFL_UI_XLIB
/*@globals errno, fileSystem, internalState, stderr, stdin, stdout@*/
/*@modifies errno, fileSystem, internalState, stderr, stdin, stdout@*/
#else	/* HNEFATAFL_UI_XLIB */
/*@globals errno, fileSystem, internalState, stdin, stdout@*/
/*@modifies errno, fileSystem, internalState, stdin, stdout@*/
#endif	/* HNEFATAFL_UI_XLIB */
/*@modifies * ui, * line@*/
{
	assert	(NULL != ui);
	assert	(NULL != line);
	return	hnef_parseline(ui, line, stdin, HNEF_FALSE,
			HNEF_FALSE, HNEF_FALSE, HNEF_FALSE, HNEF_FALSE);
}

#ifdef	HNEFATAFL_UI_AIM

/*
 * Prints the Elder Futhark, repeating itself after 3 lines (3 search
 * depth levels).
 *
 * This is passed as a function pointer to `aiminimax_command()`; a
 * `hnef_uic` struct is the `data`.
 */
static
enum HNEF_FR
hnef_callback_cli_progress (
/*@null@*/
	void	* data,
	int	progress,
	int	progress_max
	)
/*@globals errno, fileSystem, stdout@*/
/*@modifies errno, fileSystem, stdout@*/
{
	struct hnef_uic	* uic	= NULL;

	int	chars_to_print;

	assert	(NULL != data);
	assert	(progress >= 0);
	assert	(progress <= progress_max);
	assert	(100 == progress_max);

	uic	= (struct hnef_uic * HNEF_RSTR)data;

	/*
	 * Progress 0-100% becomes 0-`chars_print_max` chars to print.
	 */
	chars_to_print	= (int)(progress /
		(progress_max / (double)uic->chars_print_max))
			- uic->chars_printed;

	if	(progress_max == progress)
	{
		chars_to_print	= uic->chars_print_max
				- uic->chars_printed;
	}

	/*
	 * NOTE:	Capacity of `cli_progress_mbch` is 3.
	 */
	assert	(uic->progress_line < (unsigned int)3);
	assert	((unsigned int)	3 == uic->progress_linec);
	assert	(		8 == uic->chars_print_max);

	while	(chars_to_print-- > 0)
	{
		/*
		 * Multi-byte char (maybe it will be single byte, but
		 * there's a good chance it will be UTF-8).
		 */
		const char	* HNEF_RSTR mbch	= NULL;

		/*
		 * NOTE:	Capacity of `cli_progress_mbch` is 8.
		 */
		assert	(uic->chars_printed >= 0
		&&	uic->chars_printed < 8);

		/*
		 * The line is `uic->progress_line`. This is 0-2, which
		 * is the 3 parts of the old Futhark. Each part consists
		 * of 8 characters (which is 0-7, or
		 * `uic->chars_printed`).
		 */
		mbch	= cli_progress_mbch	[uic->progress_line]
						[uic->chars_printed];
		/*
		 * Vikingar kräva UTF-8!
		 */
		(void)fputs	(mbch, stdout);
		++uic->chars_printed;
	}

	if	(progress_max == progress)
	{
		if	(++uic->progress_line >= uic->progress_linec)
		{
			uic->progress_line = 0;
		}
		uic->chars_printed	= 0;
		(void)putchar	('\n');
	}
	else
	{
		(void)fflush	(stdout);
	}

	return	HNEF_FR_SUCCESS;
}

#endif	/* HNEFATAFL_UI_AIM */

/*
 * 1.	Tries `-l` and `-r` invocation parameters. These dir-guess with
 *	the absolute filename.
 * 2.	Tries `HNEFATAFL_LANG` and `HNEFATAFL_RULES` environment
 *	variables. These dir-guess without the absolute filename.
 *	-	If `-l` or `-r` are given, then the corresponding
 *		environment variable is never tried, even if it fails to
 *		open the files specified by the invocation parameters.
 * 3.	`-c` or `HNEFATAFL_RC` (only the absolute filename is tried) is
 *	run as an RC file. If they are not defined, then `hnefataflrc`
 *	is dir-guessed without the absolute filename. The RC file is
 *	never run if `-C` is given, even if `-c` is also given.
 *	-	If `-c` or `HNEFATAFL_RC` are defined but can't be
 *		opened, then FNAME_RC_DEFAULT is not dir-guessed. So you
 *		can indicate an invalid file with `-c` or `HNEFATAFL_RC`
 *		to suppress ruleset file reading (but `-C` is easier).
 *	-	The RC file language / ruleset commands are never
 *		suppressed if the language or ruleset are invalid when
 *		the RC file is run. If language (or ruleset) is not
 *		suppressed, then you can load language files any amount
 *		of times in the RC file (it does not get suppressed once
 *		successfully loaded).
 *
 * NOTE:	Any `lang` or `rules` commands given in the CLI will try
 *		the absolute filename, unlike when RC files are read.
 *		This is because invocation parameters and CLI commands
 *		are given by the user knowingly, so it's safe to try the
 *		filename itself. But it's not safe to do so when opening
 *		a file from an environment variable or an RC file,
 *		because that's done automatically without the user's
 *		knowledge.
 */
static
enum HNEF_FR
hnef_run_cli_init (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	struct gleip_lines	* const GLEIP_RSTR line,
/*@in@*/
/*@null@*/
	const char		* const HNEF_RSTR game_load_fname
	)
#ifdef	HNEFATAFL_UI_XLIB
/*@globals errno, fileSystem, internalState, stderr, stdin, stdout@*/
/*@modifies errno, fileSystem, internalState, stderr, stdin, stdout@*/
#else	/* HNEFATAFL_UI_XLIB */
/*@globals errno, fileSystem, internalState, stdin, stdout@*/
/*@modifies errno, fileSystem, internalState, stdin, stdout@*/
#endif	/* HNEFATAFL_UI_XLIB */
/*@modifies * ui, * line@*/
{
	enum HNEF_FR	fr			= HNEF_FR_SUCCESS;
	const char	* HNEF_RSTR fname_lang	= NULL,
			* HNEF_RSTR fname_rules	= NULL;
	HNEF_BOOL	lang_loaded_attempt	= HNEF_FALSE,
			rules_loaded_attempt	= HNEF_FALSE,
			suppress_game_load = NULL != game_load_fname;

	/*
	 * `suppress_game_load` must always be true if a filename was
	 * given as `-e` parameter, regardless if the game was
	 * successfully loaded by `-e`. This is because the `-e`
	 * parameter (`game_load_fname`) should really be loaded after
	 * the RC file has been read, because very likely the ruleset is
	 * loaded in the RC file. So the game will be invalid until
	 * after the RC file is read. Thus we suppress the `game_load`
	 * command if `-e` is given, even if `-e` doesn't even indicate
	 * an existing file.
	 */

	assert	(NULL != ui);
	assert	(NULL != line);

	fname_lang	= NULL == ui->filefind->fname_lang
			? getenv(HNEF_UI_ENVVAR_LANG)
			: ui->filefind->fname_lang;
	if	(NULL != fname_lang)
	{
		fr	= hnef_uic_parseline_lang_filename(ui,
			fname_lang, NULL != ui->filefind->fname_lang);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}
		lang_loaded_attempt	= HNEF_TRUE;
	}

	fname_rules	= NULL == ui->filefind->fname_rules
			? getenv(HNEF_UI_ENVVAR_RULES)
			: ui->filefind->fname_rules;
	if	(NULL != fname_rules)
	{
		fr	= hnef_uic_parseline_rules_filename(ui,
			fname_rules, NULL != ui->filefind->fname_rules);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}
		rules_loaded_attempt	= HNEF_TRUE;
	}

	if	(!ui->filefind->runcom_suppress)
	{
		const HNEF_BOOL
			suppress_lang	= lang_loaded_attempt
/*@i1@*/\
					&& gleip_lang_valid(ui->lang),
			suppress_rules	= rules_loaded_attempt
					&& hnef_rvalid_good
					(hnef_game_valid(ui->game)),
			suppress_path	= NULL != ui->filefind->abs_path
					|| NULL
					!= getenv(HNEF_UI_ENVVAR_PATH);
		const char	* HNEF_RSTR fname_rc	= NULL;
		FILE		* HNEF_RSTR file_rc	= NULL;

		if	(NULL != ui->filefind->abs_runcom)
		{
			file_rc	= fopen(ui->filefind->abs_runcom, "r");
		}
		else if	(NULL != (fname_rc = getenv(HNEF_UI_ENVVAR_RC)))
		{
			file_rc	= fopen(fname_rc, "r");
		}
		else
		{
			assert	(!hnef_line_empty(FNAME_RC_DEFAULT));
			file_rc	= hnef_fopen_dirguess(ui->filefind,
				NULL, FNAME_RC_DEFAULT, HNEF_FALSE,
				& fr);
			if	(!hnef_fr_good(fr))
			{
				return	fr;
			}
		}

		if	(NULL != file_rc)
		{
			while	(!ui->quit
/*@i1@*/\
			&&	!feof(file_rc))
			{
				fr = hnef_parseline(ui, line, file_rc,
						HNEF_TRUE,
						suppress_lang,
						suppress_rules,
						suppress_path,
						suppress_game_load);
				if	(!hnef_fr_good(fr))
				{
					(void)fclose(file_rc);
					return	fr;
				}
			}
			/*
			 * `ferror()` is already checked.
			 */
			if	(EOF == fclose(file_rc))
			{
				return	HNEF_FR_FAIL_IO_FILE_CLOSE;
			}
		}
	}

	if	(NULL != game_load_fname)
	{
		/*
		 * Note that this is done after reading the RC file, and
		 * that we suppressed loading in the RC file.
		 */
		HNEF_BOOL	success;
		fr	= hnef_ui_parseline_game_load_filename(ui,
				game_load_fname, & success,
				hnef_callback_print_langkey,
				hnef_callback_print_fr,
				NULL);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}
		if	(success)
		{
/*@i2@*/\
			assert	(hnef_rvalid_good
				(hnef_game_valid(ui->game)));
			(void)hnef_print_board(ui->game, HNEF_FALSE);
		}
	}

	return	HNEF_FR_SUCCESS;
}

/*
 * If `game_load_fname` is `!NULL`, the indicated filename is loaded
 * and, if the load is successful, it suppresses `e` in the RC file.
 *
 * If `run_x_then_quit`, then X is run (with `xui_theme` as a theme
 * argument) and if it can open the display, the program quits
 * afterwards. `xui_theme` is ignored if `!run_x_then_quit`.
 */
enum HNEF_FR
hnef_run_cli (
	struct hnef_ui	* const HNEF_RSTR ui,
	const char	* const HNEF_RSTR game_load_fname,
	const HNEF_BOOL	run_x_then_quit,
	const char	* const HNEF_RSTR xui_theme
	)
{
	enum HNEF_FR		fr		= HNEF_FR_SUCCESS;
	struct gleip_lines	* GLEIP_RSTR input	= NULL;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->lang);

	input	= gleip_lines_alloc();
	if	(NULL == input)
	{
		return	HNEF_FR_FAIL_ALLOC;
	}

	fr	= hnef_run_cli_init(ui, input, game_load_fname);
	if	(!hnef_fr_good(fr))
	{
		goto	RETURN_FR;
	}

	if	(!ui->quit
	&&	!hnef_rvalid_good(hnef_game_valid(ui->game))
/*@i1@*/\
	&&	gleip_lang_valid(ui->lang))
	{
		(void)hnef_print_langkey(ui->lang,
				HNEF_LK_CMD_RULES_LOADHINT, stdout);
	}

	if	(run_x_then_quit)
	{
		fr	= hnef_run_xlib_try
			(ui, xui_theme, stderr, HNEF_TRUE);
		if	(!hnef_fr_good(fr))
		{
			goto	RETURN_FR;
		}
	}

	while	(!ui->quit
/*@i1@*/\
	&&	!feof(stdin))
	{
		enum HNEF_CONTROL_TYPE type = hnef_control_type_get(ui);

		if	(HNEF_CONTROL_TYPE_HUMAN == type)
		{
			fr	= hnef_parseline_stdin(ui, input);
			if	(!hnef_fr_good(fr))
			{
				goto	RETURN_FR;
			}
		}
#ifdef	HNEFATAFL_UI_AIM
/*@i2@*/\
		else if	(HNEF_CONTROL_TYPE_AIM == type)
		{
			HNEF_BOOL	success;

			hnef_uic_reset(& ui->uic);

			/*
			 * `func_interrupt` is `NULL`: it's not possible
			 * to interrupt using only C89. Therefore we
			 * don't need to reset `force` or `stop`
			 * parameters.
			 */
			fr	= hnef_computer_move(ui,
					& ui->uic,
					NULL,
					hnef_callback_cli_progress,
					& success);
			if	(!hnef_fr_good(fr))
			{
				goto	RETURN_FR;
			}

			if	(success)
			{
				/*
				 * Actually success is always true here
				 * since it's only false when there is
				 * an interrupt callback, and there is
				 * no interrupt callback here.
				 */
				(void)hnef_print_board
					(ui->game, HNEF_FALSE);
			}
		}
#endif
	}

/*@i1@*/\
	if	(feof(stdin))
	{
		(void)puts(CLI_EOF);
	}

	RETURN_FR:

	gleip_lines_free(input);
	if	(hnef_fr_good(fr))
	{
/*@i1@*/\
		return	ferror(stdin)
			? HNEF_FR_FAIL_IO_READ
			: HNEF_FR_SUCCESS;
	}
	else
	{
		return	fr;
	}
}

