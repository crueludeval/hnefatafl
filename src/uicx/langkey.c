/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <string.h>	/* strlen */

#include "cliio.h"	/* hnef_ui_digitc */
#include "langkey.h"
#include "langkeyt.h"	/* HNEF_LK_* */

/*
 * This is the maximum length of a lang key constructed from one of the
 * `HNEF_LANGKEY_PREFIX_*` variables and an enum digit. This enum digit
 * will be at most 3 letters long because an enum can't contain more
 * than 127 members in C89 (and all of the enums that we're going to use
 * start with 0, so there won't be minus signs).
 *
 * The longest langkey prefix is "rvalid_", +3 digits = 10. We also need
 * to add +1 for '\0', but that's not done here.
 */
#define		HNEF_LANGKEY_CONSTRUCTED_MAXLEN		10

/*
 * Minimum max amount of enum members in C89.
 */
/*@unchecked@*/
static
const int	HNEF_ENUM_MEMBERS_MAX			= 127;

/*@observer@*/
/*@unchecked@*/
static
const char	* const HNEF_LANGKEY_PREFIX_FR		= "fr_",
		* const HNEF_LANGKEY_PREFIX_RREAD	= "rread_",
		* const HNEF_LANGKEY_PREFIX_RVALID	= "rvalid_",
		* const HNEF_LANGKEY_PREFIX_FRX		= "frx_";

/*
 * The maximum enum integer value of `HNEF_FR`.
 */
static
enum HNEF_FR
hnef_langkey_fr_max (void)
/*@modifies nothing@*/
{
	return	HNEF_FR_FAIL_OTHER;
}

/*
 * The maximum enum integer value of `HNEF_RREAD`.
 */
static
enum HNEF_RREAD
hnef_langkey_rread_max (void)
/*@modifies nothing@*/
{
	return	HNEF_RREAD_FAIL_TYPE_UNDEF;
}

/*
 * The maximum enum integer value of `HNEF_RVALID`.
 */
static
enum HNEF_RVALID
hnef_langkey_rvalid_max (void)
/*@modifies nothing@*/
{
	return	HNEF_RVALID_FAIL_SQUARE_UNUSED;
}

/*
 * The maximum enum integer value of `HNEF_FRX`.
 */
static
enum HNEF_FRX
hnef_langkey_frx_max (void)
/*@modifies nothing@*/
{
	return	HNEF_FRX_FAIL_XPM_READ;
}

/*
 * `key` must be a pointer to a `NULM`-terminated string.
 *
 * `key` is defined iff this function succeeds. Else it may remain
 * undefined.
 */
static
enum HNEF_FR
hnef_langkey_create (
/*@in@*/
/*@notnull@*/
	const char	* const HNEF_RSTR prefix,
	int		number,
/*@out@*/
/*@notnull@*/
	char		* const HNEF_RSTR key
	)
/*@globals errno@*/
/*@modifies errno, * key@*/
{
	int	digits;

	assert	(NULL != prefix);
	assert	(number > 0);
	assert	(number <= HNEF_ENUM_MEMBERS_MAX);

	digits	= hnef_ui_digitc((long)number);
	if	(digits < 0)
	{
/*@i1@*/\
		return	HNEF_FR_FAIL_MATH;
	}
	assert	(digits > 0);
	assert	(strlen(prefix) + (size_t)digits
		<= (size_t)HNEF_LANGKEY_CONSTRUCTED_MAXLEN);

/* SPLint snprintf not in C89 */ /*@i1@*/\
	return	sprintf(key, "%s%d", prefix, number) < 0
		? HNEF_FR_FAIL_IO_WRITE
		: HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_langkey_create_fr (
	const enum HNEF_FR	fr,
/*@out@*/
/*@notnull@*/
	char			* const HNEF_RSTR key
	)
/*@globals errno@*/
/*@modifies errno, * key@*/
{
	assert	(HNEF_FR_SUCCESS != fr);
	assert	((int)fr > 0);
	assert	(fr <= hnef_langkey_fr_max());
	assert	(NULL != key);
	return	hnef_langkey_create	(HNEF_LANGKEY_PREFIX_FR,
					(int)fr, key);
}

static
enum HNEF_FR
hnef_langkey_create_rread (
	const enum HNEF_RREAD	rread,
/*@out@*/
/*@notnull@*/
	char			* const HNEF_RSTR key
	)
/*@globals errno@*/
/*@modifies errno, * key@*/
{
	assert	(HNEF_RREAD_SUCCESS != rread);
	assert	((int)rread > 0);
	assert	(rread <= hnef_langkey_rread_max());
	assert	(NULL != key);
	return	hnef_langkey_create	(HNEF_LANGKEY_PREFIX_RREAD,
					(int)rread, key);
}

static
enum HNEF_FR
hnef_langkey_create_rvalid (
	const enum HNEF_RVALID	rvalid,
/*@out@*/
/*@notnull@*/
	char			* const HNEF_RSTR key
	)
/*@globals errno@*/
/*@modifies errno, * key@*/
{
	assert	(HNEF_RVALID_SUCCESS != rvalid);
	assert	((int)rvalid > 0);
	assert	(rvalid <= hnef_langkey_rvalid_max());
	assert	(NULL != key);
	return	hnef_langkey_create	(HNEF_LANGKEY_PREFIX_RVALID,
					(int)rvalid, key);
}

static
enum HNEF_FR
hnef_langkey_create_frx (
	const enum HNEF_FRX	frx,
/*@out@*/
/*@notnull@*/
	char			* const HNEF_RSTR key
	)
/*@globals errno@*/
/*@modifies errno, * key@*/
{
	assert	(HNEF_FRX_SUCCESS != frx);
	assert	((int)frx > 0);
	assert	(frx <= hnef_langkey_frx_max());
	assert	(NULL != key);
	return	hnef_langkey_create	(HNEF_LANGKEY_PREFIX_FRX,
					(int)frx, key);
}

/*
 * Failure to create the language key returns `NULL`.
 *
 * If the language key is successfully created, this function delegates
 * `gleip_lang_get()`.
 */
const char *
hnef_langkey_get_fr (
	const struct gleip_lang	* GLEIP_RSTR lang,
	const enum HNEF_FR	fr
	)
{
	char	key[HNEF_LANGKEY_CONSTRUCTED_MAXLEN + 1];

	assert	(NULL != lang);
	assert	(HNEF_FR_SUCCESS != fr);
	assert	((int)fr > 0);
	assert	(fr <= hnef_langkey_fr_max());

	if	(!hnef_fr_good(hnef_langkey_create_fr(fr, key)))
	{
		return	NULL;
	}

	return	gleip_lang_get(lang, key);
}

/*
 * Works like `hnef_langkey_get_fr()`.
 */
const char *
hnef_langkey_get_rread (
	const struct gleip_lang	* GLEIP_RSTR lang,
	const enum HNEF_RREAD	rread
	)
{
	char	key[HNEF_LANGKEY_CONSTRUCTED_MAXLEN + 1];

	assert	(NULL != lang);
	assert	(HNEF_RREAD_SUCCESS != rread);
	assert	((int)rread > 0);
	assert	(rread <= hnef_langkey_rread_max());

	if	(!hnef_fr_good(hnef_langkey_create_rread(rread, key)))
	{
		return	NULL;
	}

	return	gleip_lang_get(lang, key);
}

/*
 * Works like `hnef_langkey_get_fr()`.
 */
const char *
hnef_langkey_get_rvalid (
	const struct gleip_lang	* GLEIP_RSTR lang,
	const enum HNEF_RVALID	rvalid
	)
{
	char	key[HNEF_LANGKEY_CONSTRUCTED_MAXLEN + 1];

	assert	(NULL != lang);
	assert	(HNEF_RVALID_SUCCESS != rvalid);
	assert	((int)rvalid > 0);
	assert	(rvalid <= hnef_langkey_rvalid_max());

	if	(!hnef_fr_good(hnef_langkey_create_rvalid(rvalid, key)))
	{
		/*
		 * HNEF_FR_FAIL_ALLOC or HNEF_FR_FAIL_IO_WRITE are the
		 * only possible return values, and only *_ALLOC is
		 * likely to ever happen, so this is probably memory.
		 */
		return	NULL;
	}

	return	gleip_lang_get(lang, key);
}

/*
 * Works like `hnef_langkey_get_fr()`.
 */
const char *
hnef_langkey_get_frx (
	const struct gleip_lang	* GLEIP_RSTR lang,
	const enum HNEF_FRX	frx
	)
{
	char	key[HNEF_LANGKEY_CONSTRUCTED_MAXLEN + 1];

	assert	(NULL != lang);
	assert	(HNEF_FRX_SUCCESS != frx);
	assert	((int)frx > 0);
	assert	(frx <= hnef_langkey_frx_max());

	if	(!hnef_fr_good(hnef_langkey_create_frx(frx, key)))
	{
		/*
		 * HNEF_FR_FAIL_ALLOC or HNEF_FR_FAIL_IO_WRITE are the
		 * only possible return values, and only *_ALLOC is
		 * likely to ever happen, so this is probably memory.
		 */
		return	NULL;
	}

	return	gleip_lang_get(lang, key);
}

static
enum HNEF_FR
hnef_langkey_register_interval (
/*@in@*/
/*@notnull@*/
	struct gleip_lang	* GLEIP_RSTR lang,
/*@in@*/
/*@notnull@*/
	const char		* const prefix,
	const int		first,
	const int		last
	)
/*@globals errno@*/
/*@modifies errno, * lang@*/
{
	/*
	 * This is an ordinary line since it doesn't have to grow, i.e.
	 * it's not NULM-terminated.
	 */
	char	* HNEF_RSTR key	= NULL;
	int	digits,
		i;

	assert	(NULL != lang);
	assert	(NULL != prefix);
	assert	(first >= 0);
	assert	(last >= first);
	assert	(last <= HNEF_ENUM_MEMBERS_MAX);
	assert	(last > 0);	/* Implied; required for log10 */

	digits	= hnef_ui_digitc((long)last);
	if	(last < 1)
	{
		return	HNEF_FR_FAIL_MATH;
	}
	assert	(digits > 0);

	/*
	 * The maximum amount of digits is the amount of digits in
	 * `last`, since we know that it is higher than `first`, an
	 * integer (no decimals) and > 0 (no minus sign).
	 */
	key	= malloc(sizeof(* key)
		* (strlen(prefix) + (size_t)digits + (size_t)1));
	if	(NULL == key)
	{
		return	HNEF_FR_FAIL_ALLOC;
	}

	for	(i = first; i <= last; ++i)
	{
/* SPLint snprintf not in C89 */ /*@i1@*/\
		if	(sprintf(key, "%s%d", prefix, i) < 0)
		{
			free	(key);
			return	HNEF_FR_FAIL_IO_WRITE;
		}

/*@i1@*/\
		if	(!gleip_lang_regcpy(lang, key))
		{
			free	(key);
			return	HNEF_FR_FAIL_ALLOC;
		}
	}

	free	(key);
	return	HNEF_FR_SUCCESS;
}

enum HNEF_FR
hnef_langkey_init (
	struct gleip_lang	* GLEIP_RSTR lang
	)
{
	enum HNEF_FR	fr		= HNEF_FR_SUCCESS,
			fr_max		= hnef_langkey_fr_max();
	enum HNEF_RREAD	rread_max	= hnef_langkey_rread_max();
	enum HNEF_RVALID rvalid_max	= hnef_langkey_rvalid_max();
	enum HNEF_FRX	frx_max		= hnef_langkey_frx_max();

	assert	(NULL != lang);

/*@-boolops@*/
	if	(!gleip_lang_regcpy(lang, HNEF_LK_CMD_GAME_INVALID)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_GAME_LOAD_ID)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_GAME_LOAD_MISS)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_GAME_OVER)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_GAME_SAVE_MISS)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_GAME_UNDO_NUM)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_GAME_UNDO_START)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_LANG_MALF)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_LANG_MISS)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_MOVE_COORD)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_MOVE_ILLEGAL)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_PLAYER_AIM_DEPTH)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_PLAYER_AIM_MEM)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_PLAYER_INDEX)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_PLAYER_TYPE)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_RESTRICT)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_RULES_LOADHINT)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_RULES_MISS)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_SUPPRESS)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_UNK)
	||	!gleip_lang_regcpy(lang, HNEF_LK_CMD_XLIB_COMP)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_COPY)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_ENVVAR0)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_ENVVAR1)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_GAME_LOAD)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_GAME_NEW)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_GAME_SAVE)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_GAME_UNDO0)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_GAME_UNDO1)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_HELP0)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_HELP1)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_LANG)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_MOVE0)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_MOVE2)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_MOVE4)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_PATH0)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_PATH1)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_PLAYER0)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_PLAYER1)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_PLAYER2)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_PLAYER2_H)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_PLAYER2_M)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_PLAYER2_M_X)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_PLAYER2_M_Y)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_PLAYER2_M_Z)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_PRINT)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_QUIT)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_RULES)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_VERSION)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_XLIB0)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_CLI_XLIB1)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_XLIB_MOVE)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_XLIB_PRINT)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_XLIB_RULES)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_XLIB_THEME)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_XLIB_FORCE)
	||	!gleip_lang_regcpy(lang, HNEF_LK_HELP_XLIB_XLIB)
	||	!gleip_lang_regcpy(lang,
					HNEF_LK_XLIB_FONT_CHARSET_MISS)
	||	!gleip_lang_regcpy(lang,
					HNEF_LK_XLIB_FONT_FALLBACK)
	||	!gleip_lang_regcpy(lang,
				HNEF_LK_XLIB_FONT_FALLBACK_LOCALE)
	||	!gleip_lang_regcpy(lang,
					HNEF_LK_XLIB_THEMES_NOT_FOUND))
	{
		return	HNEF_FR_FAIL_ALLOC;
	}
/*@=boolops@*/

	fr	= hnef_langkey_register_interval(lang,
		HNEF_LANGKEY_PREFIX_FR,
		(int)((int)HNEF_FR_SUCCESS + 1),
		(int)fr_max);
	if	(!hnef_fr_good(fr))
	{
		return	fr;
	}

	fr	= hnef_langkey_register_interval(lang,
		HNEF_LANGKEY_PREFIX_RREAD,
		(int)((int)HNEF_RREAD_SUCCESS + 1),
		(int)rread_max);
	if	(!hnef_fr_good(fr))
	{
		return	fr;
	}

	fr	= hnef_langkey_register_interval(lang,
		HNEF_LANGKEY_PREFIX_RVALID,
		(int)((int)HNEF_RVALID_SUCCESS + 1),
		(int)rvalid_max);
	if	(!hnef_fr_good(fr))
	{
		return	fr;
	}

	fr	= hnef_langkey_register_interval(lang,
		HNEF_LANGKEY_PREFIX_FRX,
		(int)((int)HNEF_FRX_SUCCESS + 1),
		(int)frx_max);
	if	(!hnef_fr_good(fr))
	{
		return	fr;
	}

	return	HNEF_FR_SUCCESS;
}

