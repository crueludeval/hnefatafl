/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */

#include "control.h"	/* hnef_control_* */
#include "filefind.h"	/* hnef_filefind_* */
#include "langkey.h"	/* hnef_langkey_* */
#include "ui.h"
#include "uix.h"	/* hnef_uix_* */

#ifdef	HNEFATAFL_UI_AIM

void
hnef_uic_reset (
	struct hnef_uic	* const HNEF_RSTR uic
	)
{
	assert	(NULL != uic);

	/*
	 * NOTE:	If you change `progress_linec` or
	 *		`chars_print_max`, you also have to change the
	 *		`cli_progress_mbch` array and the
	 *		`func_cli_progress` function in `cli.c`.
	 */
	uic->progress_line	= (unsigned int)0;
	uic->progress_linec	= (unsigned int)3;
	uic->chars_printed	= 0;
	uic->chars_print_max	= 8;
}

#endif	/* HNEFATAFL_UI_AIM */

void
hnef_free_ui (
	struct hnef_ui	* const HNEF_RSTR ui
	)
{
	if	(NULL != ui->opt_rulesetline)
	{
		gleip_lines_free	(ui->opt_rulesetline);
	}

	if	(NULL != ui->opt_movelist)
	{
		hnef_free_listm		(ui->opt_movelist);
	}

#ifdef	HNEFATAFL_UI_XLIB
	if	(NULL != ui->uix)
	{
		hnef_free_uix_fini	(ui->uix);
	}
#endif	/* HNEFATAFL_UI_XLIB */

	if	(NULL != ui->filefind)
	{
		hnef_free_filefind	(ui->filefind);
	}

	if	(NULL != ui->lang_tmp)
	{
		gleip_lang_free		(ui->lang_tmp);
	}

	if	(NULL != ui->lang)
	{
		gleip_lang_free		(ui->lang);
	}

	if	(NULL != ui->controlv)
	{
		unsigned short	i;

		for	(i = 0; i < ui->controlc; ++i)
		{
			if	(NULL != ui->controlv[i])
			{
				hnef_free_control(ui->controlv[i]);
			}
		}
		free		(ui->controlv);
/*@i7@*/\
	}

	if	(NULL != ui->game_tmp)
	{
		hnef_free_game	(ui->game_tmp);
	}

	if	(NULL != ui->game)
	{
		hnef_free_game	(ui->game);
	}

	free(ui);
}

struct hnef_ui *
hnef_alloc_ui (
	enum HNEF_FR	* const HNEF_RSTR fr
	)
{
	unsigned short	i;
	struct hnef_ui	* HNEF_RSTR ui	= NULL;

	assert	(NULL != fr);

	* fr	= HNEF_FR_SUCCESS;

	ui	= malloc(sizeof(* ui));
	if	(NULL == ui)
	{
		return	NULL;
	}

	/*
	 * May be set later in `main()` (it's inappropriate to have
	 * `setlocale()` here because it's program-global):
	 */
	ui->setlocale_return	= NULL;

	ui->controlc		= HNEF_PLAYERS_MAX;
	ui->game		= NULL;
	ui->game_tmp		= NULL;
	ui->controlv		= NULL;
	ui->lang		= NULL;
	ui->lang_tmp		= NULL;
	ui->filefind		= NULL;
	ui->quit		= HNEF_FALSE;
	ui->opt_movelist	= NULL;
	ui->opt_rulesetline	= NULL;

#ifdef	HNEFATAFL_UI_XLIB
	ui->uix			= NULL;
#endif	/* HNEFATAFL_UI_XLIB */

#ifdef	HNEFATAFL_UI_AIM
	hnef_uic_reset		(& ui->uic);
#endif	/* HNEFATAFL_UI_AIM */

	ui->game	= hnef_alloc_game();
	if	(NULL == ui->game)
	{
		hnef_free_ui	(ui);
		return		NULL;
	}

	ui->game_tmp	= hnef_alloc_game();
	if	(NULL == ui->game_tmp)
	{
		hnef_free_ui	(ui);
		return		NULL;
	}

	ui->controlv	= malloc(sizeof(* ui->controlv) * ui->controlc);
	if	(NULL == ui->controlv)
	{
		hnef_free_ui	(ui);
		return		NULL;
	}

	for	(i = 0; i < ui->controlc; ++i)
	{
		/*
		 * Set all references to `NULL` for the free function.
		 */
		ui->controlv[i]	= NULL;
	}

	for	(i = 0; i < ui->controlc; ++i)
	{
		ui->controlv[i]	= hnef_alloc_control();
		if	(NULL == ui->controlv[i])
		{
			hnef_free_ui	(ui);
			return		NULL;
		}
	}

	ui->lang	= gleip_lang_alloc();
	if	(NULL == ui->lang)
	{
		hnef_free_ui	(ui);
		return		NULL;
	}
	* fr	= hnef_langkey_init(ui->lang);
	if	(!hnef_fr_good(* fr))
	{
		hnef_free_ui(ui);
		return	NULL;
	}

	ui->lang_tmp	= gleip_lang_alloc();
	if	(NULL == ui->lang_tmp)
	{
		hnef_free_ui	(ui);
		return		NULL;
	}
	* fr	= hnef_langkey_init(ui->lang_tmp);
	if	(!hnef_fr_good(* fr))
	{
		hnef_free_ui(ui);
		return	NULL;
	}

	ui->filefind	= hnef_alloc_filefind();
	if	(NULL == ui->filefind)
	{
		hnef_free_ui	(ui);
		return		NULL;
	}

	ui->opt_movelist	= hnef_alloc_listm(HNEF_LISTMH_CAP_DEF);
	if	(NULL == ui->opt_movelist)
	{
		hnef_free_ui	(ui);
		return		NULL;
	}

	ui->opt_rulesetline	= gleip_lines_alloc();
	if	(NULL == ui->opt_rulesetline)
	{
		hnef_free_ui	(ui);
		return		NULL;
	}

/* SPLint pointer to pointer */ /*@i1@*/\
	return	ui;
}

