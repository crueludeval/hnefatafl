/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>		/* assert */
#include <stdio.h>		/* fputc, fputs, fprintf */
#include <stdlib.h>		/* free */
#include <string.h>		/* strlen */

#include "gleipnir_line.h"	/* gleip_line_* */

#include "messages.h"		/* print_fail_* */
#include "test_line.h"

/*@+boolint@*/

static
int
check_line_form (
/*@in@*/
/*@notnull@*/
	const char	* const GLEIP_RSTR line
	)
/*@globals errno, fileSystem, stderr@*/
/*@modifies errno, fileSystem, stderr@*/
{
	const char	* const GLEIP_RSTR prefix	= "--form--";
	int		end				= 0;
	size_t		i,
			mem;

	assert	(NULL != line);

	mem	= gleip_line_mem(line);

	for	(i = 0; i < mem; ++i)
	{
		const char	ch	= line[i];
		if	('\0' == ch)
		{
			end	= 1;
			if	(i + 1 >= mem)
			{
				print_line_full(line, prefix, stderr);
				(void)fputc('\n', stderr);
				return	0;
			}
		}
		else if	('\n' == ch)
		{
			if	(i + 1 < mem)
			{
				print_line_full(line, prefix, stderr);
				(void)fputc('\n', stderr);
				return	0;
			}
		}
		else
		{
			if	(end)
			{
				print_line_full(line, prefix, stderr);
				(void)fputc('\n', stderr);
				return	0;
			}
			if	(i + 2 >= mem)
			{
				print_line_full(line, prefix, stderr);
				(void)fputc('\n', stderr);
				return	0;
			}
		}
	}
	return	1;
}

/*
 * -	`gleip_line_add_str`
 * -	`gleip_line_add_str_back`
 * -	`gleip_line_add_ch`
 * -	`gleip_line_add_ch_back`
 */
static
int
test_line_add_str (
	const int	verbose,
	const int	trim
	)
/*@globals errno, fileSystem, stderr, stdout@*/
/*@modifies errno, fileSystem, stderr, stdout@*/
{
	int		retval			= 1;
	char		* line			= NULL,
			* tmp			= NULL;
	const char	* GLEIP_RSTR add1	= "ghi",
			* GLEIP_RSTR add2	= "def",
			* GLEIP_RSTR add3	= "abc",
			* GLEIP_RSTR add4	= "345",
			addch1			= '1',
			addch2			= '2',
			addch3			= '6';

	if	(verbose)
	{
		(void)puts("test_line_add_str() INIT");
	}

	line	= gleip_line_alloc();
	if	(NULL == line)
	{
		print_fail_alloc();
		return	0;
	}
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, ""))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "")
		||	!check_line_mem(line, (size_t)2))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
/*@i1@*/\
	}

	if	(verbose)
	{
		(void)printf("\tadding [%s]\n", add1);
	}
	tmp	= gleip_line_add_str(line, add1, 0);
	if	(NULL == tmp)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	line	= tmp;
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "ghi"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "ghi")
		||	!check_line_mem(line, (size_t)5))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tadding [%s]\n", add2);
	}
	tmp	= gleip_line_add_str(line, add2, 0);
	if	(NULL == tmp)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	line	= tmp;
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "defghi"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "defghi")
		||	!check_line_mem(line, (size_t)8))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tadding [%s]\n", add3);
	}
	tmp	= gleip_line_add_str(line, add3, 0);
	if	(NULL == tmp)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	line	= tmp;
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "abcdefghi"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "abcdefghi")
		||	!check_line_mem(line, (size_t)11))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tadding [%s]\n", add4);
	}
	tmp	= gleip_line_add_str_back(line, add4);
	if	(NULL == tmp)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	line	= tmp;
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "abcdefghi345"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "abcdefghi345")
		||	!check_line_mem(line, (size_t)14))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tadding [%c]\n", addch1);
	}
	tmp	= gleip_line_add_ch(line, addch1, (strlen(line) - 3));
	if	(NULL == tmp)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	line	= tmp;
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "abcdefghi1345"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "abcdefghi1345")
		||	!check_line_mem(line, (size_t)15))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tadding [%c]\n", addch2);
	}
	tmp	= gleip_line_add_ch(line, addch2, (strlen(line) - 3));
	if	(NULL == tmp)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	line	= tmp;
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "abcdefghi12345"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "abcdefghi12345")
		||	!check_line_mem(line, (size_t)16))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tadding [%c]\n", addch3);
	}
	tmp	= gleip_line_add_ch_back(line, addch3);
	if	(NULL == tmp)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	line	= tmp;
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "abcdefghi123456"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "abcdefghi123456")
		||	!check_line_mem(line, (size_t)17))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	RETURN_VAL:
	if	(verbose)
	{
		(void)puts("test_line_add_str() FINI");
	}
	free	(line);
	return	retval;
}

/*
 * -	`gleip_line_add_str_b`
 * -	`gleip_line_add_str_back_b`
 * -	`gleip_line_add_ch_b`
 * -	`gleip_line_add_ch_back_b`
 */
static
int
test_line_add_str_b (
	const int	verbose,
	const int	trim
	)
/*@globals errno, fileSystem, stderr, stdout@*/
/*@modifies errno, fileSystem, stderr, stdout@*/
{
	int		retval			= 1;
	char		* line			= NULL;
	const char	* GLEIP_RSTR add1	= "ghi",
			* GLEIP_RSTR add2	= "def",
			* GLEIP_RSTR add3	= "abc",
			* GLEIP_RSTR add4	= "345",
			addch1			= '1',
			addch2			= '2',
			addch3			= '6';

	if	(verbose)
	{
		(void)puts("test_line_add_str_b() INIT");
	}

	line	= gleip_line_alloc();
	if	(NULL == line)
	{
		print_fail_alloc();
		return	0;
	}
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, ""))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_line_mem_trim_b(& line))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_line_form(line)
		||	!check_line_str(line, "")
		||	!check_line_mem(line, (size_t)2))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tadding [%s]\n", add1);
	}
	if	(!gleip_line_add_str_b(& line, add1, 0))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "ghi"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_line_mem_trim_b(& line))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_line_form(line)
		||	!check_line_str(line, "ghi")
		||	!check_line_mem(line, (size_t)5))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tadding [%s]\n", add2);
	}
	if	(!gleip_line_add_str_b(& line, add2, 0))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "defghi"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_line_mem_trim_b(& line))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_line_form(line)
		||	!check_line_str(line, "defghi")
		||	!check_line_mem(line, (size_t)8))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tadding [%s]\n", add3);
	}
	if	(!gleip_line_add_str_b(& line, add3, 0))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "abcdefghi"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_line_mem_trim_b(& line))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_line_form(line)
		||	!check_line_str(line, "abcdefghi")
		||	!check_line_mem(line, (size_t)11))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tadding [%s]\n", add4);
	}
	if	(!gleip_line_add_str_back_b(& line, add4))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "abcdefghi345"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_line_mem_trim_b(& line))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_line_form(line)
		||	!check_line_str(line, "abcdefghi345")
		||	!check_line_mem(line, (size_t)14))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tadding [%c]\n", addch1);
	}
	if (!gleip_line_add_ch_b(& line, addch1, (strlen(line) - 3)))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "abcdefghi1345"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_line_mem_trim_b(& line))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_line_form(line)
		||	!check_line_str(line, "abcdefghi1345")
		||	!check_line_mem(line, (size_t)15))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tadding [%c]\n", addch2);
	}
	if (!gleip_line_add_ch_b(& line, addch2, (strlen(line) - 3)))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "abcdefghi12345"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_line_mem_trim_b(& line))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_line_form(line)
		||	!check_line_str(line, "abcdefghi12345")
		||	!check_line_mem(line, (size_t)16))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tadding [%c]\n", addch3);
	}
	if	(!gleip_line_add_ch_back_b(& line, addch3))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "abcdefghi123456"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_line_mem_trim_b(& line))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_line_form(line)
		||	!check_line_str(line, "abcdefghi123456")
		||	!check_line_mem(line, (size_t)17))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	RETURN_VAL:
	if	(verbose)
	{
		(void)puts("test_line_add_str_b() FINI");
	}
	free	(line);
	return	retval;
}

/*
 * -	`gleip_line_cpy`
 */
static
int
test_line_cpy (
	const int	verbose,
	const int	trim
	)
/*@globals errno, fileSystem, stderr, stdout@*/
/*@modifies errno, fileSystem, stderr, stdout@*/
{
	int		retval			= 1;
	char		* line			= NULL,
			* tmp			= NULL;
	const char	* GLEIP_RSTR cpy1	= "123",
			* GLEIP_RSTR cpy2	= "abcdefghijklmno",
			* GLEIP_RSTR cpy3	= "456",
			* GLEIP_RSTR cpy4	= "pqrstuvwxyzaaaaaa";

	if	(verbose)
	{
		(void)puts("test_line_cpy() INIT");
	}

	line	= gleip_line_alloc();
	if	(NULL == line)
	{
		print_fail_alloc();
		return	0;
	}

	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, ""))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "")
		||	!check_line_mem(line, (size_t)2))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
/*@i1@*/\
	}

	if	(verbose)
	{
		(void)printf("\tcopying [%s]\n", cpy1);
	}
	tmp	= gleip_line_cpy(line, cpy1);
	if	(NULL == tmp)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	line	= tmp;
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "123"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "123")
		||	!check_line_mem(line, (size_t)5))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tcopying [%s]\n", cpy2);
	}
	tmp	= gleip_line_cpy(line, cpy2);
	if	(NULL == tmp)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	line	= tmp;
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "abcdefghijklmno"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "abcdefghijklmno")
		||	!check_line_mem(line, (size_t)17))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tcopying [%s]\n", cpy3);
	}
	tmp	= gleip_line_cpy(line, cpy3);
	if	(NULL == tmp)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	line	= tmp;
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "456"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "456")
		||	!check_line_mem(line, (size_t)5))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tcopying [%s]\n", cpy4);
	}
	tmp	= gleip_line_cpy(line, cpy4);
	if	(NULL == tmp)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	line	= tmp;
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "pqrstuvwxyzaaaaaa"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "pqrstuvwxyzaaaaaa")
		||	!check_line_mem(line, (size_t)19))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	RETURN_VAL:
	if	(verbose)
	{
		(void)puts("test_line_cpy() FINI");
	}
	free	(line);
	return	retval;
}

/*
 * -	`gleip_line_cpy_b`
 */
static
int
test_line_cpy_b (
	const int	verbose,
	const int	trim
	)
/*@globals errno, fileSystem, stderr, stdout@*/
/*@modifies errno, fileSystem, stderr, stdout@*/
{
	int		retval			= 1;
	char		* line			= NULL;
	const char	* GLEIP_RSTR cpy1	= "123",
			* GLEIP_RSTR cpy2	= "abcdefghijklmno",
			* GLEIP_RSTR cpy3	= "456",
			* GLEIP_RSTR cpy4	= "pqrstuvwxyzaaaaaa";

	if	(verbose)
	{
		(void)puts("test_line_cpy_b() INIT");
	}

	line	= gleip_line_alloc();
	if	(NULL == line)
	{
		print_fail_alloc();
		return	0;
	}

	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, ""))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_line_mem_trim_b(& line))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_line_form(line)
		||	!check_line_str(line, "")
		||	!check_line_mem(line, (size_t)2))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tcopying [%s]\n", cpy1);
	}
	if	(!gleip_line_cpy_b(& line, cpy1))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "123"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_line_mem_trim_b(& line))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_line_form(line)
		||	!check_line_str(line, "123")
		||	!check_line_mem(line, (size_t)5))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tcopying [%s]\n", cpy2);
	}
	if	(!gleip_line_cpy_b(& line, cpy2))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "abcdefghijklmno"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_line_mem_trim_b(& line))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_line_form(line)
		||	!check_line_str(line, "abcdefghijklmno")
		||	!check_line_mem(line, (size_t)17))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tcopying [%s]\n", cpy3);
	}
	if	(!gleip_line_cpy_b(& line, cpy3))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "456"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_line_mem_trim_b(& line))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_line_form(line)
		||	!check_line_str(line, "456")
		||	!check_line_mem(line, (size_t)5))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tcopying [%s]\n", cpy4);
	}
	if	(!gleip_line_cpy_b(& line, cpy4))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "pqrstuvwxyzaaaaaa"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_line_mem_trim_b(& line))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_line_form(line)
		||	!check_line_str(line, "pqrstuvwxyzaaaaaa")
		||	!check_line_mem(line, (size_t)19))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	RETURN_VAL:
	if	(verbose)
	{
		(void)puts("test_line_cpy_b() FINI");
	}
	free	(line);
	return	retval;
}

/*
 * -	`gleip_line_rm_str`
 * -	`gleip_line_rm_str_last`
 * -	`gleip_line_rm_ch`
 * -	`gleip_line_rm_ch_last`
 * -	`gleip_line_rm_from`
 * -	`gleip_line_rm_to`
 * -	`gleip_line_rm`
 */
static
int
test_line_rm (
	const int	verbose,
	const int	trim
	)
/*@globals errno, fileSystem, stderr, stdout@*/
/*@modifies errno, fileSystem, stderr, stdout@*/
{
	int		retval	= 1;
	char		* line	= NULL,
			* tmp	= NULL;
	const size_t	rm1i	= (size_t)3,
			rm1len	= (size_t)3,
			rm2i	= (size_t)3,
			rm2len	= (size_t)3,
			rm3len	= (size_t)3,
			rm4i	= (size_t)3,
			rm5i	= (size_t)0,
			rm6i	= (size_t)4,
			rm7i	= (size_t)2;

	if	(verbose)
	{
		(void)puts("test_line_rm() INIT");
	}

	line	= gleip_line_alloc_cpy("123456789abcdefghi");
	if	(NULL == line)
	{
		print_fail_alloc();
		return	0;
	}

	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "123456789abcdefghi"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "123456789abcdefghi")
		||	!check_line_mem(line, (size_t)20))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
/*@i1@*/\
	}

	if	(verbose)
	{
		(void)printf("\tremoving i=%lu len=%lu\n",
			(unsigned long)rm1i, (unsigned long)rm1len);
	}
	gleip_line_rm_str(line, rm1i, rm1len);
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "123789abcdefghi"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "123789abcdefghi")
		||	!check_line_mem(line, (size_t)17))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tremoving i=%lu len=%lu\n",
			(unsigned long)rm2i, (unsigned long)rm2len);
	}
	gleip_line_rm_str(line, rm2i, rm2len);
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "123abcdefghi"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "123abcdefghi")
		||	!check_line_mem(line, (size_t)14))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tremoving last len=%lu\n",
				(unsigned long)rm3len);
	}
	gleip_line_rm_str_last(line, rm3len);
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "123abcdef"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "123abcdef")
		||	!check_line_mem(line, (size_t)11))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tremoving char i=%lu\n",
				(unsigned long)rm4i);
	}
	gleip_line_rm_ch(line, rm4i);
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "123bcdef"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "123bcdef")
		||	!check_line_mem(line, (size_t)10))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tremoving char i=%lu\n",
				(unsigned long)rm5i);
	}
	gleip_line_rm_ch(line, rm5i);
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "23bcdef"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "23bcdef")
		||	!check_line_mem(line, (size_t)9))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)puts("\tremoving last char");
	}
	gleip_line_rm_ch_last(line);
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "23bcde"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "23bcde")
		||	!check_line_mem(line, (size_t)8))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tremoving from i=%lu\n",
				(unsigned long)rm6i);
	}
	gleip_line_rm_from(line, rm6i);
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "23bc"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "23bc")
		||	!check_line_mem(line, (size_t)6))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)printf("\tremoving to i=%lu\n",
				(unsigned long)rm7i);
	}
	gleip_line_rm_to(line, rm7i);
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, "bc"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "bc")
		||	!check_line_mem(line, (size_t)4))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	if	(verbose)
	{
		(void)puts("\tremoving all");
	}
	gleip_line_rm(line);
	if	(verbose)
	{
		print_line_full(line, "\t\t", stdout);
		(void)puts("");
	}

	if	(!check_line_form(line)
	||	!check_line_str(line, ""))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		tmp	= gleip_line_mem_trim(line);
		if	(NULL == tmp)
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		line	= tmp;
		if	(!check_line_form(line)
		||	!check_line_str(line, "")
		||	!check_line_mem(line, (size_t)2))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_line_full(line, "\t\t", stdout);
			(void)puts("");
		}
	}

	RETURN_VAL:
	if	(verbose)
	{
		(void)puts("test_line_rm() FINI");
	}
	free	(line);
	return	retval;
}

int
test_line (
	const int	verbose
	)
{
	return	test_line_add_str	(verbose, 0)
	&&	test_line_add_str	(verbose, 1)
	&&	test_line_add_str_b	(verbose, 0)
	&&	test_line_add_str_b	(verbose, 1)
	&&	test_line_cpy		(verbose, 0)
	&&	test_line_cpy		(verbose, 1)
	&&	test_line_cpy_b		(verbose, 0)
	&&	test_line_cpy_b		(verbose, 1)
	&&	test_line_rm		(verbose, 0)
	&&	test_line_rm		(verbose, 1);
}

/*@=boolint@*/

