/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef TEST_MESSAGES_H
#define TEST_MESSAGES_H

#include "gleipnir_line.h"	/* GLEIP_RSTR */
#include "gleipnir_lines.h"	/* gleip_lines */

extern
void
print_line_full (
/*@in@*/
/*@notnull@*/
	const char	* const GLEIP_RSTR,
/*@in@*/
/*@null@*/
	const char	* const GLEIP_RSTR,
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	FILE		* const out
/*@=protoparamname@*/
	)
/*@globals errno, fileSystem@*/
/*@modifies errno, fileSystem, out@*/
;

extern
void
print_lines_full (
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR,
/*@in@*/
/*@null@*/
	const char			* const GLEIP_RSTR,
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	FILE				* const GLEIP_RSTR out
/*@=protoparamname@*/
	)
/*@globals errno, fileSystem@*/
/*@modifies errno, fileSystem, out@*/
;

extern
int
check_line_mem (
/*@in@*/
/*@notnull@*/
	const char	* const GLEIP_RSTR,
	const size_t
	)
/*@globals fileSystem, stderr@*/
/*@modifies fileSystem, stderr@*/
;

extern
int
check_line_str (
/*@in@*/
/*@notnull@*/
	const char	* const GLEIP_RSTR,
/*@in@*/
/*@notnull@*/
	const char	* const GLEIP_RSTR
	)
/*@globals fileSystem, stderr@*/
/*@modifies fileSystem, stderr@*/
;

extern
int
check_lines_integrity (
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR,
	const size_t
	)
/*@globals errno, fileSystem, stderr@*/
/*@modifies errno, fileSystem, stderr@*/
;

extern
int
check_lines_str (
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR,
	const size_t,
/*@in@*/
/*@notnull@*/
	const char			* const GLEIP_RSTR
	)
/*@globals errno, fileSystem, stderr@*/
/*@modifies errno, fileSystem, stderr@*/
;

/*@unused@*/	/* false positive: used in `test_line.c` */
extern
int
check_lines_mem (
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR,
	const size_t,
	const size_t
	)
/*@globals errno, fileSystem, stderr@*/
/*@modifies errno, fileSystem, stderr@*/
;

extern
int
check_lines_trimmed (
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR
	)
/*@globals errno, fileSystem, stderr@*/
/*@modifies errno, fileSystem, stderr@*/
;

extern
int
check_lines_equal (
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR,
	const int
	)
/*@globals errno, fileSystem, stderr@*/
/*@modifies errno, fileSystem, stderr@*/
;

extern
void
print_fail_file_close (
/*@in@*/
/*@notnull@*/
	const char	* const GLEIP_RSTR
	)
/*@globals fileSystem, stderr@*/
/*@modifies fileSystem, stderr@*/
;

extern
void
print_fail_file_error (void)
/*@globals fileSystem, stderr@*/
/*@modifies fileSystem, stderr@*/
;

extern
void
print_fail_file_open (
/*@in@*/
/*@notnull@*/
	const char	* const GLEIP_RSTR
	)
/*@globals fileSystem, stderr@*/
/*@modifies fileSystem, stderr@*/
;

extern
void
print_fail_file_seek (
/*@in@*/
/*@notnull@*/
	const char	* const GLEIP_RSTR
	)
/*@globals fileSystem, stderr@*/
/*@modifies fileSystem, stderr@*/
;

extern
void
print_fail_alloc (void)
/*@globals fileSystem, stderr@*/
/*@modifies fileSystem, stderr@*/
;

#endif

