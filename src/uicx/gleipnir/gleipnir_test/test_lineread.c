/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#ifndef	S_SPLINT_S	/* SPLint bug */
#include <ctype.h>	/* isspace */
#endif	/* S_SPLINT_S */
#include <stdlib.h>	/* free */
#include <string.h>	/* strcmp */

#include "gleipnir_lineread.h"	/* gleip_lineread_* */
#include "gleipnir_lines.h"	/* gleip_lines_* */

#include "config.h"		/* FNAME_* */
#include "messages.h"		/* print_* */
#include "test_lineread.h"

/*@+boolint@*/

static
int
test_lineread_raw_eof_cstr_str (
/*@in@*/
/*@notnull@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@in@*/
/*@notnull@*/
	FILE			* const GLEIP_RSTR file
	)
/*@globals errno, fileSystem, stderr@*/
/*@modifies errno, fileSystem, stderr@*/
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem, * file@*/
{
	const char	* ptr	= NULL;
	char		* src	= NULL;
	int		chi,
			retval	= 1;

	assert	(NULL != lines);
	assert	(NULL != file);

	src	= gleip_line_alloc();
	if	(NULL == src)
	{
		print_fail_alloc();
		return	0;
	}

	while	(EOF != (chi = fgetc(file)))
	{
		if	('\0' == (char)chi)
		{
			continue;
		}
		if	(!gleip_line_add_ch_back_b(& src, (char)chi))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
	}
	if	(ferror(file))
	{
		print_fail_file_error();
		retval	= 0;
		goto	RETURN_VAL;
	}

	ptr	= src;
	if	(!gleip_lineread_raw_eof_cstr_nl(lines, & ptr))
	{
		ptr	= NULL;
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	ptr	= NULL;

	RETURN_VAL:
	if	(NULL != src)
	{
		free(src);
	}
	return	retval;
}

static
int
test_lineread_raw_eof_cstr_file (
/*@in@*/
/*@notnull@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@in@*/
/*@notnull@*/
	FILE			* const GLEIP_RSTR file
	)
/*@globals errno, fileSystem, stderr@*/
/*@modifies errno, fileSystem, stderr@*/
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem, * file@*/
{
	assert	(NULL != lines);
	assert	(NULL != file);

	if	(!gleip_lineread_raw_eof_file_nl(lines, file))
	{
		print_fail_alloc();
		return	0;
	}
	if	(ferror(file))
	{
		print_fail_file_error();
		return	0;
	}

	return	1;
}

/*
 * -	gleip_lineread_raw_eof_cstr_nl
 * -	gleip_lineread_raw_eof_file_nl
 */
static
int
test_lineread_raw (
	const int	verbose,
	const int	trim
	)
/*@globals errno, fileSystem, stderr, stdout@*/
/*@modifies errno, fileSystem, stderr, stdout@*/
{
	int			retval		= 1;
	struct gleip_lines	* lines_str	= NULL,
				* lines_file	= NULL;
	FILE			* file		= NULL;

	if	(verbose)
	{
		(void)puts("test_lineread_raw() INIT");
	}

	lines_str	= gleip_lines_alloc();
	if	(NULL == lines_str)
	{
		print_fail_alloc();
		return	0;
	}

	lines_file	= gleip_lines_alloc();
	if	(NULL == lines_file)
	{
		gleip_lines_free(lines_str);
		print_fail_alloc();
		return	0;
	}

	if	(!check_lines_integrity(lines_str, 0)
	||	!check_lines_integrity(lines_file, 0))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines_str, "\tcstr:\t", stdout);
		print_lines_full(lines_file, "\tfile:\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines_str)
		||	!gleip_lines_mem_trim(lines_file))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines_str, 0)
		||	!check_lines_trimmed	(lines_str)
		||	!check_lines_integrity	(lines_file, 0)
		||	!check_lines_trimmed	(lines_file))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines_str, "\tcstr:\t",
					stdout);
			print_lines_full(lines_file, "\tfile:\t",
					stdout);
		}
	}

	file	= fopen(FNAME_LINEREAD_RAW, "r");
	if	(NULL == file)
	{
		print_fail_file_open(FNAME_LINEREAD_RAW);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!test_lineread_raw_eof_cstr_str(lines_str, file))
	{
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(ferror(file))
	{
		print_fail_file_error();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(0 != fseek(file, 0L, SEEK_SET))
	{
		print_fail_file_seek(FNAME_LINEREAD_RAW);
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(!test_lineread_raw_eof_cstr_file(lines_file, file))
	{
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(ferror(file))
	{
		print_fail_file_error();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(EOF == fclose(file))
	{
		print_fail_file_close(FNAME_LINEREAD_RAW);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(verbose)
	{
		print_lines_full(lines_str, "\tcstr:\t", stdout);
		print_lines_full(lines_file, "\tfile:\t", stdout);
	}
	if	(!check_lines_equal(lines_str, lines_file, 1))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if (!check_lines_integrity(lines_str, (size_t)7)
	|| !check_lines_str(lines_str, (size_t)0, "rad1")
	|| !check_lines_str(lines_str, (size_t)1, " ")
	|| !check_lines_str(lines_str, (size_t)2, "rad2 ")
	|| !check_lines_str(lines_str, (size_t)3, " rad3")
	|| !check_lines_str(lines_str, (size_t)4, " rad4 ")
	|| !check_lines_str(lines_str, (size_t)5, "	rad5")
	|| !check_lines_str(lines_str, (size_t)6, "	  rad6")
	|| !check_lines_integrity(lines_file, (size_t)7)
	|| !check_lines_str(lines_file, (size_t)0, "rad1")
	|| !check_lines_str(lines_file, (size_t)1, " ")
	|| !check_lines_str(lines_file, (size_t)2, "rad2 ")
	|| !check_lines_str(lines_file, (size_t)3, " rad3")
	|| !check_lines_str(lines_file, (size_t)4, " rad4 ")
	|| !check_lines_str(lines_file, (size_t)5, "	rad5")
	|| !check_lines_str(lines_file, (size_t)6, "	  rad6"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	file	= fopen(FNAME_LINEREAD_RAW2, "r");
	if	(NULL == file)
	{
		print_fail_file_open(FNAME_LINEREAD_RAW2);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!test_lineread_raw_eof_cstr_str(lines_str, file))
	{
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(ferror(file))
	{
		print_fail_file_error();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(0 != fseek(file, 0L, SEEK_SET))
	{
		print_fail_file_seek(FNAME_LINEREAD_RAW);
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(!test_lineread_raw_eof_cstr_file(lines_file, file))
	{
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(ferror(file))
	{
		print_fail_file_error();
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(EOF == fclose(file))
	{
		print_fail_file_close(FNAME_LINEREAD_RAW);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(verbose)
	{
		print_lines_full(lines_str, "\tcstr:\t", stdout);
		print_lines_full(lines_file, "\tfile:\t", stdout);
	}
	if	(!check_lines_equal(lines_str, lines_file, 1))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	RETURN_VAL:
	if	(verbose)
	{
		(void)puts("test_lineread_raw() FINI");
	}
	if	(NULL != lines_str)
	{
		gleip_lines_free(lines_str);
	}
	if	(NULL != lines_file)
	{
		gleip_lines_free(lines_file);
	}
	return	retval;
}

/*
 * -	gleip_lineread_interp_file_def
 */
static
int
test_lineread_interp (
	const int	verbose,
	const int	trim
	)
/*@globals errno, fileSystem, stderr, stdout@*/
/*@modifies errno, fileSystem, stderr, stdout@*/
{
	int			retval	= 1;
	struct gleip_lines	* lines	= NULL;
	FILE			* file	= NULL;
	size_t			i;

	if	(verbose)
	{
		(void)puts("test_lineread_interp() INIT");
	}

	lines	= gleip_lines_alloc();
	if	(NULL == lines)
	{
		print_fail_alloc();
		return	0;
	}

	if	(!check_lines_integrity(lines, 0))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, 0)
		||	!check_lines_trimmed	(lines))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	/*
	 * File 1.
	 */

	file	= fopen(FNAME_LINEREAD_INTERP, "r");
	if	(NULL == file)
	{
		print_fail_file_open(FNAME_LINEREAD_INTERP);
		retval	= 0;
		goto	RETURN_VAL;
	}

	for	(i = 0; i < (size_t)11; ++i)
	{
		int	ret;
		gleip_lines_rm(lines);
		ret	= gleip_lineread_interp_file_def
			(lines, file, 0);
		if	(EOF == ret
		&&	i < (size_t)10)
		{
			(void)fclose(file);
			(void)fprintf(stderr, "Unexpected EOF (%lu)\n",
					(unsigned long)i);
			print_lines_full(lines, "\t\t", stderr);
			retval	= 0;
			goto	RETURN_VAL;
		}
/*@+tmpcomments@*/
/*@t3@*/\
		else if	(!ret)
/*@=tmpcomments@*/
		{
			(void)fclose	(file);
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		switch	(i)
		{
			case	0:
			case	3:
			case	4:
			case	5:
			case	7:
				if (!check_lines_integrity(lines, 0))
				{
					(void)fclose(file);
					retval	= 0;
					goto	RETURN_VAL;
				}
/*@switchbreak@*/
				break;
			case	1:
				if	(!check_lines_integrity
						(lines, (size_t)5)
				||	!check_lines_str(lines,
						(size_t)0, "abc")
				||	!check_lines_str(lines,
						(size_t)1, "def")
				||	!check_lines_str(lines,
						(size_t)2, "ghi")
				||	!check_lines_str(lines,
						(size_t)3, "jkl")
				||	!check_lines_str(lines,
						(size_t)4, "mno"))
				{
					(void)fclose(file);
					retval	= 0;
					goto	RETURN_VAL;
				}
/*@switchbreak@*/
				break;
			case	2:
				if	(!check_lines_integrity
						(lines, (size_t)5)
				||	!check_lines_str(lines,
						(size_t)0, "ABC")
				||	!check_lines_str(lines,
						(size_t)1, "DEF")
				||	!check_lines_str(lines,
						(size_t)2, "GHI")
				||	!check_lines_str(lines,
						(size_t)3, "JKL")
				||	!check_lines_str(lines,
						(size_t)4, "MNO"))
				{
					(void)fclose(file);
					retval	= 0;
					goto	RETURN_VAL;
				}
/*@switchbreak@*/
				break;
			case	6:
				if	(!check_lines_integrity
						(lines, (size_t)1)
				||	!check_lines_str(lines,
					(size_t)0, "\t999\n\t333\t"))
				{
					(void)fclose(file);
					retval	= 0;
					goto	RETURN_VAL;
				}
/*@switchbreak@*/
				break;
			case	8:
				if	(!check_lines_integrity
						(lines, (size_t)1)
				||	!check_lines_str(lines,
					(size_t)0, "pq\vr\ts\nt"))
				{
					(void)fclose(file);
					retval	= 0;
					goto	RETURN_VAL;
				}
/*@switchbreak@*/
				break;
			case	9:
				if	(!check_lines_integrity
						(lines, (size_t)1)
				||	!check_lines_str(lines,
					(size_t)0, "PQ\vR\tS\nT"))
				{
					(void)fclose(file);
					retval	= 0;
					goto	RETURN_VAL;
				}
/*@switchbreak@*/
				break;
			case	10:
				if	(!check_lines_integrity
						(lines, (size_t)3)
				||	!check_lines_str(lines,
						(size_t)0, "123456")
				||	!check_lines_str(lines,
						(size_t)1, "789")
				||	!check_lines_str(lines,
						(size_t)2, "0"))
				{
					(void)fclose(file);
					retval	= 0;
					goto	RETURN_VAL;
				}
/*@switchbreak@*/
				break;
			default:
				assert	(0);
/*@switchbreak@*/
				break;
		}
	}

	if	(ferror(file))
	{
		print_fail_file_error();
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(EOF == fclose(file))
	{
		print_fail_file_close(FNAME_LINEREAD_RAW);
		retval	= 0;
		goto	RETURN_VAL;
	}

	/*
	 * File 2.
	 */

	file	= fopen(FNAME_LINEREAD_INTERP2, "r");
	if	(NULL == file)
	{
		print_fail_file_open(FNAME_LINEREAD_INTERP2);
		retval	= 0;
		goto	RETURN_VAL;
	}

	for	(i = 0; i < (size_t)4; ++i)
	{
		int	ret;
		gleip_lines_rm(lines);
		ret	= gleip_lineread_interp_file_def
			(lines, file, (size_t)2);
		if	(EOF == ret
		&&	i < (size_t)3)
		{
			(void)fclose(file);
			(void)fprintf(stderr, "Unexpected EOF (%lu)\n",
					(unsigned long)i);
			print_lines_full(lines, "\t\t", stderr);
			retval	= 0;
			goto	RETURN_VAL;
		}
/*@+tmpcomments@*/
/*@t3@*/\
		else if	(!ret)
/*@=tmpcomments@*/
		{
			(void)fclose	(file);
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		switch	(i)
		{
			case	0:
				if	(!check_lines_integrity
						(lines, (size_t)2)
				||	!check_lines_str(lines,
						(size_t)0, "key1")
				||	!check_lines_str(lines,
						(size_t)1, "value1"))
				{
					(void)fclose(file);
					retval	= 0;
					goto	RETURN_VAL;
				}
/*@switchbreak@*/
				break;
			case	1:
				if	(!check_lines_integrity
						(lines, (size_t)2)
				||	!check_lines_str(lines,
						(size_t)0, "key2")
				||	!check_lines_str(lines,
						(size_t)1,
						"value2 text text"
						" text text"))
				{
					(void)fclose(file);
					retval	= 0;
					goto	RETURN_VAL;
				}
/*@switchbreak@*/
				break;
			case	2:
				if	(!check_lines_integrity
						(lines, (size_t)2)
				||	!check_lines_str(lines,
						(size_t)0, "key3")
				||	!check_lines_str(lines,
						(size_t)1,
						"value3 text text"
						" text text"))
				{
					(void)fclose(file);
					retval	= 0;
					goto	RETURN_VAL;
				}
/*@switchbreak@*/
				break;
			case	3:
				if	(!check_lines_integrity
						(lines, (size_t)2)
				||	!check_lines_str(lines,
						(size_t)0, "key4")
				||	!check_lines_str(lines,
						(size_t)1,
						"\"   value4   text"
						"         text   \""))
				{
					(void)fclose(file);
					retval	= 0;
					goto	RETURN_VAL;
				}
/*@switchbreak@*/
				break;
			default:
				assert	(0);
/*@switchbreak@*/
				break;
		}
	}

	if	(ferror(file))
	{
		print_fail_file_error();
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(EOF == fclose(file))
	{
		print_fail_file_close(FNAME_LINEREAD_RAW);
		retval	= 0;
		goto	RETURN_VAL;
	}

	RETURN_VAL:
	if	(verbose)
	{
		(void)puts("test_lineread_interp() FINI");
	}
	if	(NULL != lines)
	{
		gleip_lines_free(lines);
	}
	return	retval;
}

static
int
test_lineread_token (
	const int	verbose,
	const int	trim
	)
/*@globals errno, fileSystem, stderr, stdout@*/
/*@modifies errno, fileSystem, stderr, stdout@*/
{
	int			retval		= 1;
	struct gleip_lines	* lines		= NULL;
	const char
		* const src	= " Det var en gång en \t bäver  \t  ",
		* ptr		= NULL;

	if	(verbose)
	{
		(void)puts("test_lineread_token() INIT");
	}

	lines	= gleip_lines_alloc();
	if	(NULL == lines)
	{
		print_fail_alloc();
		return	0;
	}

	if	(!check_lines_integrity	(lines, (size_t)0))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}
	if	(verbose)
	{
		print_lines_full(lines, "\t\t", stdout);
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lines, (size_t)0)
		||	!check_lines_trimmed	(lines))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	ptr	= src;
/*@+tmpcomments@*/
/*@t6@*/\
	if	(!gleip_lineread_raw_eof_cstr(lines, & ptr, isspace))
/*@=tmpcomments@*/
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}
	assert(0 == strcmp(" Det var en gång en \t bäver  \t  ", src));
	assert(gleip_line_empty(ptr));

	if	(!check_lines_integrity(lines, (size_t)6)
	||	!check_lines_str(lines, (size_t)0, "Det")
	||	!check_lines_str(lines, (size_t)1, "var")
	||	!check_lines_str(lines, (size_t)2, "en")
	||	!check_lines_str(lines, (size_t)3, "gång")
	||	!check_lines_str(lines, (size_t)4, "en")
	||	!check_lines_str(lines, (size_t)5, "bäver"))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lines_mem_trim(lines))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * Content check omitted.
		 */
		if	(!check_lines_integrity	(lines, (size_t)6)
		||	!check_lines_trimmed	(lines))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lines, "\t\t", stdout);
		}
	}

	RETURN_VAL:
	if	(verbose)
	{
		(void)puts("test_lineread_token() FINI");
	}
	gleip_lines_free(lines);
	return	retval;
}

int
test_lineread (
	const int	verbose
	)
{
	return	test_lineread_raw	(verbose, 0)
	&&	test_lineread_raw	(verbose, 1)
	&&	test_lineread_interp	(verbose, 0)
	&&	test_lineread_interp	(verbose, 1)
	&&	test_lineread_token	(verbose, 0)
	&&	test_lineread_token	(verbose, 1);
}

/*@=boolint@*/

