/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <string.h>	/* strcmp */

#include "test_lang.h"

#include "gleipnir_lineread.h"	/* gleip_lineread_* */
#include "gleipnir_lang.h"	/* gleip_lang_* */

#include "config.h"		/* FNAME_* */
#include "messages.h"		/* print_* */

/*@+boolint@*/

static
void
print_lang (
/*@in@*/
/*@notnull@*/
	const struct gleip_lang	* const GLEIP_RSTR lang,
/*@in@*/
/*@notnull@*/
	FILE			* const out
	)
/*@globals errno, fileSystem@*/
/*@modifies errno, fileSystem, out@*/
{
	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(NULL != out);

	print_lines_full(lang->keys,	"keys:\t",	out);
	print_lines_full(lang->values,	"values:\t",	out);
}

static
int
check_lang_valid (
/*@in@*/
/*@notnull@*/
	const struct gleip_lang	* const GLEIP_RSTR lang,
	const int		valid
	)
/*@globals errno, fileSystem, stderr@*/
/*@modifies errno, fileSystem, stderr@*/
{
	size_t	i;

	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);

	if	(lang->keys->size != lang->values->size)
	{
		(void)fprintf(stderr, "Key size=%lu, value size=%lu\n",
			(unsigned long)lang->keys->size,
			(unsigned long)lang->values->size);
		print_lang(lang, stderr);
		return	0;
	}

	for	(i = 0; i < gleip_lang_size(lang); ++i)
	{
		const char	* const GLEIP_RSTR key =
						lang->keys->arr[i],
				* const GLEIP_RSTR value =
						lang->values->arr[i],
				* GLEIP_RSTR value_get = NULL;
		assert	(NULL != key);
		assert	(NULL != value);
		assert	(0 == strcmp(key, gleip_lang_key(lang, i)));
		if	(gleip_line_empty(key))
		{
			(void)fprintf(stderr,
				"Key i=%lu [%s] is empty\n",
				(unsigned long)i,
				key);
			print_lang(lang, stderr);
			return	0;
		}
		if	(gleip_line_empty(value))
		{
			(void)fprintf(stderr,
				"Value for key i=%lu [%s] is empty\n",
				(unsigned long)i,
				key);
			print_lang(lang, stderr);
			return	0;
		}

		value_get = gleip_lang_get(lang, key);
		if	(NULL == value_get)
		{
			(void)fprintf(stderr,
				"Get for key i=%lu [%s] not found\n",
				(unsigned long)i,
				key);
			print_lang(lang, stderr);
			return	0;
		}
		else
		{
			if	(0 != strcmp(value, value_get))
			{
				(void)fprintf(stderr,
					"get[%s] != value[%s] for "
					"key i=%lu [%s]\n",
					value_get, value,
					(unsigned long)i,
					key);
				print_lang(lang, stderr);
				return	0;
			}
			assert	(0 == strcmp(value,
					gleip_lang_value(lang, i)));
		}
	}

	if	(!valid)
	{
		const char	* const GLEIP_RSTR why =
				gleip_lang_invalid_why(lang);
		if	(NULL == why)
		{
			(void)fputs("Invalid; why is NULL. "
				"Possible reasons: "
				"key found in file not registered; ",
				stderr);
			(void)fputs("duplicate key found in file; "
				"empty key/value (unlikely)\n", stderr);
		}
		else
		{
			(void)fprintf(stderr, "Invalid; why=[%s]\n",
				why);
		}
		print_lang(lang, stderr);
		return	0;
	}

	return	1;
}

/*
 * -	gleip_lang_regcpy
 * -	gleip_lang_read_file
 * -	gleip_lang_alloc_cpy
 */
static
int
test_lang_read (
	const int	verbose,
	const int	trim
	)
/*@globals errno, fileSystem, stderr, stdout@*/
/*@modifies errno, fileSystem, stderr, stdout@*/
{
	int			retval	= 1,
				valid	= 1;
	FILE			* file	= NULL;
	struct gleip_lang	* lang	= NULL,
				* cpy	= NULL;
	struct gleip_lines	* buf	= NULL;

	if	(verbose)
	{
		(void)puts("test_lang_read() INIT");
	}

	lang	= gleip_lang_alloc();
	if	(NULL == lang)
	{
		print_fail_alloc();
		return	0;
	}

	buf	= gleip_lines_alloc();
	if	(NULL == buf)
	{
		print_fail_alloc();
		gleip_lang_free	(lang);
		return	0;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lang_mem_trim(lang)
		||	!gleip_lines_mem_trim(buf))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(!check_lines_integrity	(lang->keys, 0)
		||	!check_lines_trimmed	(lang->keys)
		||	!check_lines_integrity	(lang->values, 0)
		||	!check_lines_trimmed	(lang->values)
		||	!check_lines_integrity	(buf, 0)
		||	!check_lines_trimmed	(buf))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lang->keys,
						"\tkeys:\t", stdout);
			print_lines_full(lang->values,
						"\tvalues:\t", stdout);
			print_lines_full(buf,	"\tbuf:\t", stdout);
		}
	}

	if	(!gleip_lang_regcpy(lang, "key1")
	||	!gleip_lang_regcpy(lang, "key2")
	||	!gleip_lang_regcpy(lang, "key3")
	||	!gleip_lang_regcpy(lang, "key4")
	||	!gleip_lang_regcpy(lang, " key5 ")
	||	!gleip_lang_regcpy(lang, "key6")
	||	!gleip_lang_regcpy(lang, "key7\tkey7"))
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity(lang->keys, (size_t)7)
	||	!check_lines_str(lang->keys, (size_t)0, "key1")
	||	!check_lines_str(lang->keys, (size_t)1, "key2")
	||	!check_lines_str(lang->keys, (size_t)2, "key3")
	||	!check_lines_str(lang->keys, (size_t)3, "key4")
	||	!check_lines_str(lang->keys, (size_t)4, " key5 ")
	||	!check_lines_str(lang->keys, (size_t)5, "key6")
	||	!check_lines_str(lang->keys, (size_t)6, "key7\tkey7")
	||	!check_lines_integrity(lang->values, (size_t)7)
	||	!check_lines_str(lang->values, (size_t)0, "")
	||	!check_lines_str(lang->values, (size_t)1, "")
	||	!check_lines_str(lang->values, (size_t)2, "")
	||	!check_lines_str(lang->values, (size_t)3, "")
	||	!check_lines_str(lang->values, (size_t)4, "")
	||	!check_lines_str(lang->values, (size_t)5, "")
	||	!check_lines_str(lang->values, (size_t)6, ""))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lang_mem_trim(lang)
		||	!gleip_lines_mem_trim(buf))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * Content check omitted.
		 */
		if	(!check_lines_integrity(lang->keys, (size_t)7)
		||	!check_lines_trimmed(lang->keys)
		||	!check_lines_integrity(lang->values, (size_t)7)
		||	!check_lines_trimmed(lang->values)
		||	!check_lines_integrity(buf, 0)
		||	!check_lines_trimmed(buf))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lang->keys,
						"\tkeys:\t", stdout);
			print_lines_full(lang->values,
						"\tvalues:\t", stdout);
			print_lines_full(buf,	"\tbuf:\t", stdout);
		}
	}

	file	= fopen(FNAME_LANG, "r");
	if	(NULL == file)
	{
		print_fail_file_open(FNAME_LINEREAD_RAW);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!gleip_lang_read_file(lang, file, NULL, & valid))
	{
		print_fail_alloc();
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(ferror(file))
	{
		print_fail_file_error();
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lang_valid(lang, valid))
	{
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity(lang->keys, (size_t)7)
	||	!check_lines_str(lang->keys, (size_t)0, "key1")
	||	!check_lines_str(lang->keys, (size_t)1, "key2")
	||	!check_lines_str(lang->keys, (size_t)2, "key3")
	||	!check_lines_str(lang->keys, (size_t)3, "key4")
	||	!check_lines_str(lang->keys, (size_t)4, " key5 ")
	||	!check_lines_str(lang->keys, (size_t)5, "key6")
	||	!check_lines_str(lang->keys, (size_t)6, "key7\tkey7")
	||	!check_lines_integrity(lang->values, (size_t)7)
	||	!check_lines_str(lang->values, (size_t)0, "value1")
	||	!check_lines_str(lang->values, (size_t)1,
			"value2 text text")
	||	!check_lines_str(lang->values, (size_t)2,
			"value3 text text text")
	||	!check_lines_str(lang->values, (size_t)3,
			"value4 text text text text")
	||	!check_lines_str(lang->values, (size_t)4, " ")
	||	!check_lines_str(lang->values, (size_t)5,
			"value6\ttext\t text\ntext\ntext text text")
	||	!check_lines_str(lang->values, (size_t)6,
			"value7 text..."))
	{
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(0 != fseek(file, 0L, SEEK_SET))
	{
		print_fail_file_seek(FNAME_LANG);
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!gleip_lang_read_file(lang, file, buf, & valid))
	{
		print_fail_alloc();
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(ferror(file))
	{
		print_fail_file_error();
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lang_valid(lang, valid))
	{
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity(lang->keys, (size_t)7)
	||	!check_lines_str(lang->keys, (size_t)0, "key1")
	||	!check_lines_str(lang->keys, (size_t)1, "key2")
	||	!check_lines_str(lang->keys, (size_t)2, "key3")
	||	!check_lines_str(lang->keys, (size_t)3, "key4")
	||	!check_lines_str(lang->keys, (size_t)4, " key5 ")
	||	!check_lines_str(lang->keys, (size_t)5, "key6")
	||	!check_lines_str(lang->keys, (size_t)6, "key7\tkey7")
	||	!check_lines_integrity(lang->values, (size_t)7)
	||	!check_lines_str(lang->values, (size_t)0, "value1")
	||	!check_lines_str(lang->values, (size_t)1,
			"value2 text text")
	||	!check_lines_str(lang->values, (size_t)2,
			"value3 text text text")
	||	!check_lines_str(lang->values, (size_t)3,
			"value4 text text text text")
	||	!check_lines_str(lang->values, (size_t)4, " ")
	||	!check_lines_str(lang->values, (size_t)5,
			"value6\ttext\t text\ntext\ntext text text")
	||	!check_lines_str(lang->values, (size_t)6,
			"value7 text..."))
	{
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	gleip_lang_unreg(lang, "key1");
	valid	= gleip_lang_valid(lang);
	if	(!check_lang_valid(lang, valid))
	{
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity(lang->keys, (size_t)6)
	||	!check_lines_str(lang->keys, (size_t)0, "key2")
	||	!check_lines_str(lang->keys, (size_t)1, "key3")
	||	!check_lines_str(lang->keys, (size_t)2, "key4")
	||	!check_lines_str(lang->keys, (size_t)3, " key5 ")
	||	!check_lines_str(lang->keys, (size_t)4, "key6")
	||	!check_lines_str(lang->keys, (size_t)5, "key7\tkey7")
	||	!check_lines_integrity(lang->values, (size_t)6)
	||	!check_lines_str(lang->values, (size_t)0,
			"value2 text text")
	||	!check_lines_str(lang->values, (size_t)1,
			"value3 text text text")
	||	!check_lines_str(lang->values, (size_t)2,
			"value4 text text text text")
	||	!check_lines_str(lang->values, (size_t)3, " ")
	||	!check_lines_str(lang->values, (size_t)4,
			"value6\ttext\t text\ntext\ntext text text")
	||	!check_lines_str(lang->values, (size_t)5,
			"value7 text..."))
	{
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(0 != fseek(file, 0L, SEEK_SET))
	{
		print_fail_file_seek(FNAME_LANG);
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!gleip_lang_read_file(lang, file, buf, & valid))
	{
		print_fail_alloc();
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(ferror(file))
	{
		print_fail_file_error();
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(valid)
	{
		(void)fputs("Expected file to be invalid\n", stderr);
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity(lang->keys, (size_t)6)
	||	!check_lines_str(lang->keys, (size_t)0, "key2")
	||	!check_lines_str(lang->keys, (size_t)1, "key3")
	||	!check_lines_str(lang->keys, (size_t)2, "key4")
	||	!check_lines_str(lang->keys, (size_t)3, " key5 ")
	||	!check_lines_str(lang->keys, (size_t)4, "key6")
	||	!check_lines_str(lang->keys, (size_t)5, "key7\tkey7")
	||	!check_lines_integrity(lang->values, (size_t)6)
	||	!check_lines_str(lang->values, (size_t)0,
			"value2 text text")
	||	!check_lines_str(lang->values, (size_t)1,
			"value3 text text text")
	||	!check_lines_str(lang->values, (size_t)2,
			"value4 text text text text")
	||	!check_lines_str(lang->values, (size_t)3, " ")
	||	!check_lines_str(lang->values, (size_t)4,
			"value6\ttext\t text\ntext\ntext text text")
	||	!check_lines_str(lang->values, (size_t)5,
			"value7 text..."))
	{
		(void)fclose(file);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(EOF == fclose(file))
	{
		print_fail_file_close(FNAME_LANG);
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(trim)
	{
		if	(verbose)
		{
			(void)puts("\ttrimming...");
		}
		if	(!gleip_lang_mem_trim(lang)
		||	!gleip_lines_mem_trim(buf))
		{
			print_fail_alloc();
			retval	= 0;
			goto	RETURN_VAL;
		}
		/*
		 * Content check omitted.
		 */
		if	(!check_lines_integrity(lang->keys, (size_t)6)
		||	!check_lines_trimmed(lang->keys)
		||	!check_lines_integrity(lang->values, (size_t)6)
		||	!check_lines_trimmed(lang->values)
		||	!check_lines_integrity(buf, 0)
		||	!check_lines_trimmed(buf))
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		if	(verbose)
		{
			print_lines_full(lang->keys,
						"\tkeys:\t", stdout);
			print_lines_full(lang->values,
						"\tvalues:\t", stdout);
			print_lines_full(buf,	"\tbuf:\t", stdout);
		}
	}

	cpy	= gleip_lang_alloc_cpy(lang);
	if	(NULL == cpy)
	{
		print_fail_alloc();
		retval	= 0;
		goto	RETURN_VAL;
	}

	if	(!check_lines_integrity(cpy->keys, (size_t)6)
	||	!check_lines_str(cpy->keys, (size_t)0, "key2")
	||	!check_lines_str(cpy->keys, (size_t)1, "key3")
	||	!check_lines_str(cpy->keys, (size_t)2, "key4")
	||	!check_lines_str(cpy->keys, (size_t)3, " key5 ")
	||	!check_lines_str(cpy->keys, (size_t)4, "key6")
	||	!check_lines_str(cpy->keys, (size_t)5, "key7\tkey7")
	||	!check_lines_integrity(cpy->values, (size_t)6)
	||	!check_lines_str(cpy->values, (size_t)0,
			"value2 text text")
	||	!check_lines_str(cpy->values, (size_t)1,
			"value3 text text text")
	||	!check_lines_str(cpy->values, (size_t)2,
			"value4 text text text text")
	||	!check_lines_str(cpy->values, (size_t)3, " ")
	||	!check_lines_str(cpy->values, (size_t)4,
			"value6\ttext\t text\ntext\ntext text text")
	||	!check_lines_str(cpy->values, (size_t)5,
			"value7 text..."))
	{
		retval	= 0;
		goto	RETURN_VAL;
	}

	RETURN_VAL:
	if	(verbose)
	{
		(void)puts("test_lang_read() FINI");
	}
	if	(NULL != buf)
	{
		gleip_lines_free(buf);
	}
	if	(NULL != lang)
	{
		gleip_lang_free(lang);
	}
	if	(NULL != cpy)
	{
		gleip_lang_free(cpy);
	}
	return	retval;
}

int
test_lang (
	const int	verbose
	)
{
	return	test_lang_read(verbose,	0)
	&&	test_lang_read(verbose,	1);
}

/*@=boolint@*/

