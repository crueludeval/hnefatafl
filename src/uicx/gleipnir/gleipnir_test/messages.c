/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <stdio.h>	/* fputs */
#include <string.h>	/* strcmp */

#include "messages.h"

/*@+boolint@*/

void
print_line_full (
	const char	* const GLEIP_RSTR line,
	const char	* const GLEIP_RSTR prefix,
	FILE		* const out
	)
{
	size_t	i,
		mem;

	assert	(NULL != line);
	assert	(NULL != out);

	mem	= gleip_line_mem(line);

	if	(NULL != prefix)
	{
		(void)fputs(prefix, out);
	}
	(void)fputc('[', out);
	for	(i = 0; i < mem; ++i)
	{
		const char	ch	= line[i];
		if	('\0' == ch)
		{
			(void)fputs("\\0", out);
		}
		else if	('\n' == ch)
		{
			(void)fputs("\\n", out);
		}
		else
		{
			(void)fputc(ch, out);
		}
	}
	(void)fprintf(out, "] (mem=%lu)", (unsigned long)mem);
}

void
print_lines_full (
	const struct gleip_lines	* const GLEIP_RSTR lines,
	const char			* const GLEIP_RSTR prefix,
	FILE				* const GLEIP_RSTR out
	)
{
	size_t	i;

	assert	(NULL != lines);
	assert	(NULL != lines->arr);
	assert	(NULL != out);

	if	(NULL != prefix)
	{
		(void)fputs(prefix, out);
	}

	(void)fprintf(out, "LINES BEGIN size=%lu (mem=%lu)\n",
		(unsigned long)lines->size,
		(unsigned long)lines->mem);
	for	(i = 0; i < lines->mem; ++i)
	{
		const char * const GLEIP_RSTR line = lines->arr[i];

		if	(NULL != prefix)
		{
			(void)fputs(prefix, out);
		}
		(void)fprintf(out, "%6lu%s\t", (unsigned long)i,
			(i < lines->size ? "" : "*"));

		if	(NULL == line)
		{
			(void)fputs("NULL", out);
		}
		else
		{
			print_line_full(line, NULL, out);
		}

		assert	(NULL != line
		||	i >= lines->size);

		(void)fputs("\n", out);
	}
	if	(NULL != prefix)
	{
		(void)fputs(prefix, out);
	}
	(void)fputs("LINES END (*=garbage)\n", out);
}

int
check_line_mem (
	const char	* const GLEIP_RSTR line,
	const size_t	mem_expected
	)
{
	size_t	mem;

	assert	(NULL != line);
	assert	(mem_expected >= (size_t)2);

	mem	= gleip_line_mem(line);
	if	(mem_expected == mem)
	{
		return	1;
	}
	else
	{
		(void)fprintf(stderr,
			"Expected memory %lu, got %lu [%s]\n",
			(unsigned long)mem_expected,
			(unsigned long)mem,
			line);
		return	0;
	}
}

int
check_line_str (
	const char	* const GLEIP_RSTR line,
	const char	* const GLEIP_RSTR expected
	)
{
	assert	(NULL != line);
	assert	(NULL != expected);

	if	(0 == strcmp(line, expected))
	{
		return	1;
	}
	else
	{
		(void)fprintf(stderr, "Expected [%s], got [%s]\n",
			line, expected);
		return	0;
	}
}

int
check_lines_integrity (
	const struct gleip_lines	* const GLEIP_RSTR lines,
	const size_t			size_expected
	)
{
	size_t	i;

	assert	(NULL != lines);

	if	(size_expected != lines->size)
	{
		(void)fprintf(stderr,
			"Expected size=%lu, got size=%lu\n",
			(unsigned long)size_expected,
			(unsigned long)lines->size);
		print_lines_full(lines, NULL, stderr);
		return	0;
	}

	if	(lines->mem < lines->size)
	{
		(void)fprintf(stderr,
			"Mem < size. Mem=%lu, size=%lu\n",
			(unsigned long)lines->mem,
			(unsigned long)lines->size);
		print_lines_full(lines, NULL, stderr);
		return	0;
	}

	for	(i = lines->size; i < lines->mem; ++i)
	{
		const char	* const GLEIP_RSTR line = lines->arr[i];
		if	(NULL != line
		&&	strlen(line) > 0)
		{
			(void)fprintf(stderr,
				"Non-empty garbage line i=%lu [%s]\n",
				(unsigned long)i,
				line);
			print_lines_full(lines, NULL, stderr);
			return	0;
		}
	}

	return	1;
}

int
check_lines_str (
	const struct gleip_lines	* const GLEIP_RSTR lines,
	const size_t			index,
	const char			* const GLEIP_RSTR expected
	)
{
	assert	(NULL != lines);
	assert	(NULL != expected);

	if	(index >= lines->size)
	{
		(void)fprintf(stderr,
			"Index %lu is out of bounds (size=%lu) [%s]\n",
			(unsigned long)index,
			(unsigned long)lines->size,
			expected);
		print_lines_full(lines, NULL, stderr);
		return	0;
	}

	if	(0 != strcmp(expected, lines->arr[index]))
	{
		(void)fprintf(stderr,
			"Expected[%s], got[%s], index=%lu\n",
			expected,
			lines->arr[index],
			(unsigned long)index);
		print_lines_full(lines, NULL, stderr);
		return	0;
	}

	return	1;
}

int
check_lines_mem (
	const struct gleip_lines	* const GLEIP_RSTR lines,
	const size_t			index,
	const size_t			mem_expected
	)
{
	size_t	mem;

	assert	(NULL != lines);
	assert	(0 == mem_expected	/* NULL */
	||	mem_expected > (size_t)1);

	if	(index >= lines->mem)
	{
		(void)fprintf(stderr,
			"Index %lu is out of bounds "
			"(mem=%lu, expected=%lu)\n",
			(unsigned long)index,
			(unsigned long)lines->mem,
			(unsigned long)mem_expected);
		print_lines_full(lines, NULL, stderr);
		return	0;
	}

	if	(NULL == lines->arr[index])
	{
		if	(0 == mem_expected)
		{
			return	1;
		}
		else
		{
			(void)fprintf(stderr,
				"Expected mem=%lu, got NULL (i=%lu)\n",
				(unsigned long)mem_expected,
				(unsigned long)index);
			print_lines_full(lines, NULL, stderr);
			return	0;
		}
	}

	mem	= gleip_line_mem(lines->arr[index]);
	if	(mem < (size_t)2)
	{
		(void)fprintf(stderr,
			"Invalid string mem=%lu "
			"(expected %lu, i=%lu)\n",
			(unsigned long)mem,
			(unsigned long)mem_expected,
			(unsigned long)index);
		print_lines_full(lines, NULL, stderr);
		return	0;
	}

	if	(mem_expected != mem)
	{
		(void)fprintf(stderr,
			"Expected %lu, got mem=%lu (i=%lu)\n",
			(unsigned long)mem_expected,
			(unsigned long)mem,
			(unsigned long)index);
		print_lines_full(lines, NULL, stderr);
		return	0;
	}

	return	1;
}

int
check_lines_trimmed (
	const struct gleip_lines	* const GLEIP_RSTR lines
	)
{
	size_t	i;

	assert	(NULL != lines);

	/*
	 * Memory must be > 0 because we can't allocate 0 memory. So mem
	 * will always be >= 1. If size is 0 and the struct is trimmed,
	 * then size will be 0 and mem will be 1 (the minimum).
	 */
	if	(lines->size != lines->mem
	&&	!(	0 == lines->size
		&&	(size_t)1 == lines->mem))
	{
		(void)fprintf(stderr,
			"(size != mem) or (size == 0 but mem != 1); "
			"size=%lu, mem=%lu\n",
			(unsigned long)lines->size,
			(unsigned long)lines->mem);
		print_lines_full(lines, NULL, stderr);
		return	0;
	}

	for	(i = 0; i < lines->mem; ++i)
	{
		const char	* const GLEIP_RSTR line	= lines->arr[i];
		if	(NULL == line)
		{
			if	(i < lines->size)
			{
				(void)fprintf(stderr,
					"NULL ptr among non-garbage; "
					"i=%lu, size=%lu, mem=%lu\n",
					(unsigned long)i,
					(unsigned long)lines->size,
					(unsigned long)lines->mem);
				print_lines_full(lines, NULL, stderr);
				return	0;
			}
		}
		else
		{
			const size_t	len	= strlen(line),
					mem	= gleip_line_mem(line);
			if	(i >= lines->size
			&&	!(	0 == lines->size
				&&	(size_t)1 == lines->mem))
			{
				/*
				 * This is all
				 */
				(void)fprintf(stderr,
					"!NULL ptr among garbage "
					"(should be NULL) "
					"i=%lu, size=%lu, mem=%lu\n",
					(unsigned long)i,
					(unsigned long)lines->size,
					(unsigned long)lines->mem);
				print_lines_full(lines, NULL, stderr);
				return	0;
			}
			if	(mem != len + 2)
			{
				(void)fprintf(stderr,
					"Line is not trimmed; "
					"len=%lu, line_mem=%lu [%s], "
					"i=%lu, size=%lu, mem=%lu\n",
					(unsigned long)len,
					(unsigned long)mem,
					line,
					(unsigned long)i,
					(unsigned long)lines->size,
					(unsigned long)lines->mem);
				print_lines_full(lines, NULL, stderr);
				return	0;
			}
		}
	}
	return	1;
}

int
check_lines_equal (
	const struct gleip_lines	* const GLEIP_RSTR l1,
	const struct gleip_lines	* const GLEIP_RSTR l2,
	const int			exact
	)
{
	size_t	i;

	assert	(NULL != l1);
	assert	(NULL != l2);
	assert	(l1 != l2);

	if	(l1->size != l2->size)
	{
		(void)fprintf(stderr, "size(%lu) != size(%lu)\n",
			(unsigned long)l1->size,
			(unsigned long)l2->size);
		print_lines_full(l1, "l1\t", stderr);
		print_lines_full(l2, "l2\t", stderr);
		return	0;
	}

	if	(exact
	&&	l1->mem != l2->mem)
	{
		(void)fprintf(stderr, "mem(%lu) != mem(%lu)\n",
			(unsigned long)l1->mem,
			(unsigned long)l2->mem);
		print_lines_full(l1, "l1\t", stderr);
		print_lines_full(l2, "l2\t", stderr);
		return	0;
	}

	for	(i = 0; i < l1->mem; ++i)
	{
		const char	* GLEIP_RSTR str1	= NULL,
				* GLEIP_RSTR str2	= NULL;
		size_t		len1,
				len2;
		if	(i >= l1->size
		&&	!exact)
		{
			break;
		}

		str1	= l1->arr[i];
		str2	= l2->arr[i];
		if	(NULL == str1
		&&	NULL == str2)
		{
			continue;
		}
		else if	(NULL == str1)
		{
			(void)fprintf(stderr,
				"str1=NULL, str2=[%s] (i=%lu)\n",
				str2,
				(unsigned long)i);
			print_lines_full(l1, "l1\t", stderr);
			print_lines_full(l2, "l2\t", stderr);
			return	0;
		}
/*@+tmpcomments@*/
/*@t1@*/\
		else if	(NULL == str2)
/*@=tmpcomments@*/
		{
			(void)fprintf(stderr,
				"str1=[%s], str2=NULL (i=%lu)\n",
				str1,
				(unsigned long)i);
			print_lines_full(l1, "l1\t", stderr);
			print_lines_full(l2, "l2\t", stderr);
			return	0;
		}
		assert	(NULL != str1);
		assert	(NULL != str2);

		len1	= strlen(str1);
		len2	= strlen(str2);
		if	(len1 != len2)
		{
			(void)fprintf(stderr,
				"strlen1=%lu [%s], "
				"strlen2=%lu [%s] (i=%lu)\n",
				(unsigned long)len1,
				str1,
				(unsigned long)len2,
				str2,
				(unsigned long)i);
			print_lines_full(l1, "l1\t", stderr);
			print_lines_full(l2, "l2\t", stderr);
			return	0;
		}

		if	(exact)
		{
			const size_t	mem1	= gleip_line_mem(str1),
					mem2	= gleip_line_mem(str2);
			if	(mem1 != mem2)
			{
				(void)fprintf(stderr,
					"strmem1=%lu [%s], "
					"strmem2=%lu [%s] (i=%lu)\n",
					(unsigned long)mem1,
					str1,
					(unsigned long)mem2,
					str2,
					(unsigned long)i);
				print_lines_full(l1, "l1\t", stderr);
				print_lines_full(l2, "l2\t", stderr);
				return	0;
			}
			if	(0 != memcmp(str1, str2,
					sizeof(* str1) * mem1))
			{
				(void)fprintf(stderr,
					"[%s] != [%s] (i=%lu, "
					"exact)\n",
					str1,
					str2,
					(unsigned long)i);
				print_lines_full(l1, "l1\t", stderr);
				print_lines_full(l2, "l2\t", stderr);
				return	0;
			}
		}
		else
		{
			if	(0 != strcmp(str1, str2))
			{
				(void)fprintf(stderr,
					"[%s] != [%s] (i=%lu, "
					"inexact)\n",
					str1,
					str2,
					(unsigned long)i);
				print_lines_full(l1, "l1\t", stderr);
				print_lines_full(l2, "l2\t", stderr);
				return	0;
			}
		}
	}
	return	1;
}

void
print_fail_file_close (
	const char	* const GLEIP_RSTR fname
	)
{
	assert	(NULL != fname);

	(void)fprintf(stderr, "Failed to close file [%s]\n", fname);
}

void
print_fail_file_error (void)
{
	(void)fputs("Error reading from file\n", stderr);
}

void
print_fail_file_open (
	const char	* const GLEIP_RSTR fname
	)
{
	assert	(NULL != fname);

	(void)fprintf(stderr, "Failed to open file [%s]\n", fname);
}

void
print_fail_file_seek (
	const char	* const GLEIP_RSTR fname
	)
{
	assert	(NULL != fname);

	(void)fprintf(stderr, "Failed to seek file [%s]\n", fname);
}

void
print_fail_alloc (void)
{
	(void)fputs("Failed to allocate\n", stderr);
}

/*@=boolint@*/

