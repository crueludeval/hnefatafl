/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef	GLEIP_LINEREAD_H
#define	GLEIP_LINEREAD_H

#include <stdio.h>	/* FILE */

#include "gleipnir_lines.h"	/* gleip_lines */

#if	defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#define GLEIP_RSTR restrict
#else
#define	GLEIP_RSTR
#endif

/*@unchecked@*/
/*@unused@*/
extern
const long	GLEIP_LINEREAD_VERSION;

#define	GLEIP_LINEREAD_COMMENT	'#'
#define	GLEIP_LINEREAD_ESCAPE	'\\'
#define	GLEIP_LINEREAD_QUOTED	'"'
#define	GLEIP_LINEREAD_QUOTES	'\''

/*@unused@*/
extern
int
gleip_lineread_raw (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char	* * const dst,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	void	* const data,
/*@=protoparamname@*/
/*@notnull@*/
	int	(*) (void *),
/*@null@*/
	int	(*) (int)
	)
/*@modifies * dst, * * dst, * data@*/
;

/*@unused@*/
extern
int
gleip_lineread_raw_eof (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	void			* const data,
/*@=protoparamname@*/
/*@notnull@*/
	int			(*) (void *),
/*@null@*/
	int			(*) (int)
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem, * data@*/
;

/*@unused@*/
extern
int
gleip_lineread_raw_file (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char	* * const dst,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	FILE	* const file,
/*@=protoparamname@*/
/*@null@*/
	int	(*) (int)
	)
/*@modifies * dst, * * dst, * file@*/
;

/*@unused@*/
extern
int
gleip_lineread_raw_file_nl (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char	* * const dst,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	FILE	* const file
/*@=protoparamname@*/
	)
/*@modifies * dst, * * dst, * file@*/
;

/*@unused@*/
extern
int
gleip_lineread_cb_data_next_cstr (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	const char	* * const src
/*@=protoparamname@*/
	)
/*@modifies * src@*/
;

/*@unused@*/
extern
int
gleip_lineread_raw_cstr (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char		* * const dst,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	const char	* * const src,
/*@=protoparamname@*/
/*@null@*/
	int		(*) (int)
	)
/*@modifies * dst, * * dst, * src@*/
;

/*@unused@*/
extern
int
gleip_lineread_raw_cstr_nl (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char		* * const dst,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	const char	* * const src
/*@=protoparamname@*/
	)
/*@modifies * dst, * * dst, * src@*/
;

/*@unused@*/
extern
int
gleip_lineread_raw_eof_file (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	FILE			* const file,
/*@=protoparamname@*/
/*@null@*/
	int			(*) (int)
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem, * file@*/
;

/*@unused@*/
extern
int
gleip_lineread_raw_eof_file_nl (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	FILE			* const file
/*@=protoparamname@*/
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem, * file@*/
;

/*@unused@*/
extern
int
gleip_lineread_raw_eof_cstr (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	const char		* * const src,
/*@=protoparamname@*/
/*@null@*/
	int			(*) (int)
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem, * src@*/
;

/*@unused@*/
extern
int
gleip_lineread_raw_eof_cstr_nl (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	const char		* * const src
/*@=protoparamname@*/
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem, * src@*/
;

/*@unused@*/
extern
int
gleip_lineread_interp (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	void			* const data,
/*@=protoparamname@*/
	const size_t,
	const char,
/*@notnull@*/
	int			(*) (void *),
/*@null@*/
	int			(*) (int),
/*@null@*/
	int			(*) (int)
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem, * data@*/
;

/*@unused@*/
extern
int
gleip_lineread_interp_def (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	void			* const data,
/*@=protoparamname@*/
	const size_t,
/*@notnull@*/
	int			(*) (void *)
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem, * data@*/
;

/*@unused@*/
extern
int
gleip_lineread_interp_file (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	FILE			* const file,
/*@=protoparamname@*/
	const size_t,
	const char,
/*@null@*/
	int			(*) (int),
/*@null@*/
	int			(*) (int)
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem, * file@*/
;

/*@unused@*/
extern
int
gleip_lineread_interp_file_def (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	FILE			* const file,
/*@=protoparamname@*/
	const size_t
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem, * file@*/
;

/*@unused@*/
extern
int
gleip_lineread_interp_cstr (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	const char		* * const src,
/*@=protoparamname@*/
	const size_t,
	const char,
/*@null@*/
	int			(*) (int),
/*@null@*/
	int			(*) (int)
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem, * src@*/
;

/*@unused@*/
extern
int
gleip_lineread_interp_cstr_def (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	const char		* * const src,
/*@=protoparamname@*/
	const size_t
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem, * src@*/
;

#endif

