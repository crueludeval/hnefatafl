/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#ifndef	S_SPLINT_S	/* SPLint bug */
#include <ctype.h>	/* isspace */
#endif	/* S_SPLINT_S */
#include <stdlib.h>	/* free */
#include <string.h>	/* strchr */

#include "gleipnir_line.h"	/* gleip_line_* */
#include "gleipnir_lines.h"	/* gleip_lines_* */

#include "gleipnir_lineread.h"

const long	GLEIP_LINEREAD_VERSION		= 140816L;

/*@unchecked@*/
static
const char	GLEIP_LINEREAD_CONDENSE_SYM_DEF	= ' ';

int
gleip_lineread_raw (
	char	* * const dst,
	void	* const data,
	int	(* callback_data_next) (void *),
	int	(* callback_tkn_delim) (int)
	)
{
	int	chi;

	assert	(NULL != dst);
	assert	(NULL != * dst);
	assert	(NULL != data);
	assert	(NULL != callback_data_next);

	while	(EOF != (chi = callback_data_next(data)))
	{
		if	('\0' == (char)chi)
		{
			continue;
		}

		if	(NULL != callback_tkn_delim
/*@+tmpcomments@*/
/*@t1@*/\
		&&	callback_tkn_delim(chi))
/*@=tmpcomments@*/
		{
			break;
		}

/*@+tmpcomments@*/
/*@t1@*/\
		if	(!gleip_line_add_ch_back_b(dst, (char)chi))
/*@=tmpcomments@*/
		{
			return	0;
		}
	}

	return	EOF == chi
		? EOF
		: 1;
/*@+tmpcomments@*/
/*@t1@*/\
}
/*@=tmpcomments@*/

/*@in@*/
/*@null@*/
/*@only@*/
static
char *
gleip_lineread_buf_get (
/*@in@*/
/*@notnull@*/
	struct gleip_lines	* const GLEIP_RSTR lines
	)
/*@modifies * lines->arr@*/
{
	size_t	i,
		i_best		= 0,
		mem_best	= 0;

	assert	(NULL != lines);

	for	(i = lines->size; i < lines->mem; ++i)
	{
		char	* const GLEIP_RSTR line = lines->arr[i];
		if	(NULL != line)
		{
			const size_t	mem	= gleip_line_mem(line);
			if	(mem > mem_best)
			{
				i_best		= i;
				mem_best	= mem;
			}
		}
	}

	if	(mem_best > 0)
	{
		char	* GLEIP_RSTR line	= NULL;

		assert	(lines->mem > lines->size);
		assert	(i_best >= lines->size
		&&	i_best < lines->mem);

		line			= lines->arr[i_best];
		lines->arr[i_best]	= NULL;
/*@+tmpcomments@*/
/*@t1@*/\
		return	line;
	}
	else
	{
/*@t1@*/\
		return	NULL;
/*@=tmpcomments@*/
	}
}

/*@in@*/
/*@null@*/
/*@only@*/
static
char *
gleip_lineread_buf_save (
/*@in@*/
/*@notnull@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@in@*/
/*@notnull@*/
/*@only@*/
	char			* const buf
	)
/*@modifies * lines->arr@*/
{
	size_t	i,
		i_worst		= 0,
		mem_worst	= 0;

	assert	(NULL != lines);
	assert	(NULL != buf);

	for	(i = lines->size; i < lines->mem; ++i)
	{
		char	* const GLEIP_RSTR line	= lines->arr[i];
		if	(NULL == line)
		{
/*@+tmpcomments@*/
/*@t3@*/\
			lines->arr[i]	= buf;
/*@t1@*/\
			return		NULL;
/*@=tmpcomments@*/
		}
		else
		{
			const size_t	mem	= gleip_line_mem(line);
			if	(mem_worst < (size_t)1
			||	mem < mem_worst)
			{
				i_worst		= i;
				mem_worst	= mem;
			}
		}
	}

	if	(mem_worst > 0)
	{
		const size_t	mem_buf	= gleip_line_mem(buf);
		if	(mem_worst < mem_buf)
		{
			char	* const GLEIP_RSTR line =
					lines->arr[i_worst];

			assert	(NULL != line);

/*@+tmpcomments@*/
/*@t3@*/\
			lines->arr[i_worst]	= buf;
/*@=tmpcomments@*/
			return			line;
		}
		else
		{
/*@+tmpcomments@*/
/*@t1@*/\
			return	NULL;
/*@=tmpcomments@*/
		}
	}
	else
	{
/*@+tmpcomments@*/
/*@t1@*/\
		return	NULL;
/*@=tmpcomments@*/
	}
}

int
gleip_lineread_raw_eof (
	struct gleip_lines	* const GLEIP_RSTR lines,
	void			* const data,
	int			(* callback_data_next) (void *),
	int			(* callback_tkn_delim) (int)
	)
{
	int	chi,
		newline	= 1;
/*@only@*/
	char	* buf	= NULL;

	assert	(NULL != lines);
	assert	(NULL != callback_data_next);
	assert	(NULL != data);

	buf	= gleip_lineread_buf_get(lines);

	while	(EOF != (chi = callback_data_next(data)))
	{
		assert	(NULL == buf
/*@+tmpcomments@*/
/*@t1@*/\
		||	!gleip_line_empty(buf));
/*@=tmpcomments@*/

		if	('\0' == (char)chi)
		{
			continue;
		}

		if	(NULL != callback_tkn_delim
/*@+tmpcomments@*/
/*@t1@*/\
		&&	callback_tkn_delim(chi))
/*@=tmpcomments@*/
		{
			newline	= 1;
			continue;
		}

/*@+tmpcomments@*/
/*@t1@*/\
		if	(newline)
/*@=tmpcomments@*/
		{
			if	(NULL != buf
/*@+tmpcomments@*/
/*@t1@*/\
			&&	!gleip_line_empty(buf))
/*@=tmpcomments@*/
			{
				char	* const GLEIP_RSTR cpy =
					gleip_line_alloc_cpy(NULL == buf
								? ""
								: buf);
				if	(NULL == cpy)
				{
					goto	RETURN_FAIL;
				}
/*@+tmpcomments@*/
/*@t1@*/\
				if	(!gleip_lines_addref_back
					(lines, cpy))
/*@=tmpcomments@*/
				{
/*@+tmpcomments@*/
/*@t1@*/\
					free	(cpy);
/*@=tmpcomments@*/
					goto	RETURN_FAIL;
				}
				if	(NULL != buf)
				{
					gleip_line_rm(buf);
				}

				assert	(NULL == buf
/*@+tmpcomments@*/
/*@t1@*/\
				||	gleip_line_empty(buf));
/*@t1@*/\
				assert	(!gleip_lines_empty(lines));
/*@=tmpcomments@*/
			}
			newline	= 0;
		}

		if	(NULL == buf)
		{
			buf	= gleip_line_alloc_len((size_t)40);
			if	(NULL == buf)
			{
				goto	RETURN_FAIL;
			}
		}
		assert	(NULL != buf);

/*@+tmpcomments@*/
/*@t1@*/\
		if	(!gleip_line_add_ch_back_b(& buf, (char)chi))
/*@=tmpcomments@*/
		{
			goto	RETURN_FAIL;
		}
	}

	if	(NULL != buf)
	{
/*@+tmpcomments@*/
/*@t1@*/\
		assert	(!gleip_line_empty(buf));
/*@t1@*/\
		if	(!gleip_lines_addref_back(lines, buf))
/*@=tmpcomments@*/
		{
			goto	RETURN_FAIL;
		}
		buf	= NULL;
	}
	return	1;

	RETURN_FAIL:
	if	(NULL != buf)
	{
		char	* worst	= NULL;
		gleip_line_rm(buf);
		worst	= gleip_lineread_buf_save(lines, buf);
		if	(NULL != worst)
		{
			free	(worst);
		}
	}
	return	0;
/*@+tmpcomments@*/
/*@t1@*/\
}
/*@=tmpcomments@*/

int
gleip_lineread_raw_file (
	char	* * const dst,
	FILE	* const file,
	int	(* callback_tkn_delim) (int)
	)
{
	assert	(NULL != dst);
	assert	(NULL != * dst);
	assert	(NULL != file);

	return	gleip_lineread_raw(dst,
			file,
			(int (*) (void *))fgetc,
/*@+voidabstract@*/
			callback_tkn_delim);
/*@=voidabstract@*/
}

static
int
gleip_lineread_cb_token_delim_nl (
	const int	ch
	)
/*@*/
{
/*@+tmpcomments@*/
/*@t1@*/\
	return	(int)'\n' == ch;
/*@=tmpcomments@*/
}

int
gleip_lineread_raw_file_nl (
	char	* * const dst,
	FILE	* const file
	)
{
	assert	(NULL != dst);
	assert	(NULL != * dst);
	assert	(NULL != file);

	return	gleip_lineread_raw_file(dst, file,
			gleip_lineread_cb_token_delim_nl);
}

int
gleip_lineread_cb_data_next_cstr (
	const char	* * const src
	)
{
	char	ch;

	assert	(NULL != src);

	ch	= * * src;
	if	('\0' == ch)
	{
		return	EOF;
	}
	else
	{
		++(* src);
		return	(int)ch;
	}
}

int
gleip_lineread_raw_cstr (
	char		* * const dst,
	const char	* * const src,
	int		(* callback_tkn_delim) (int)
	)
{
	assert	(NULL != dst);
	assert	(NULL != * dst);
	assert	(NULL != src);
	assert	(NULL != * src);

	return	gleip_lineread_raw(dst, src,
		(int (*) (void *))gleip_lineread_cb_data_next_cstr,
			callback_tkn_delim);
}

int
gleip_lineread_raw_cstr_nl (
	char		* * const dst,
	const char	* * const src
	)
{
	assert	(NULL != dst);
	assert	(NULL != * dst);
	assert	(NULL != src);
	assert	(NULL != * src);

	return	gleip_lineread_raw_cstr(dst, src,
			gleip_lineread_cb_token_delim_nl);
}

int
gleip_lineread_raw_eof_file (
	struct gleip_lines	* const GLEIP_RSTR lines,
	FILE			* const file,
	int			(* callback_tkn_delim) (int)
	)
{
	assert	(NULL != lines);
	assert	(NULL != file);

	return	gleip_lineread_raw_eof(lines, file,
			(int (*) (void *))fgetc,
/*@+voidabstract@*/
			callback_tkn_delim);
/*@=voidabstract@*/
}

int
gleip_lineread_raw_eof_file_nl (
	struct gleip_lines	* const GLEIP_RSTR lines,
	FILE			* const file
	)
{
	assert	(NULL != lines);
	assert	(NULL != file);

	return	gleip_lineread_raw_eof_file(lines, file,
			gleip_lineread_cb_token_delim_nl);
}

int
gleip_lineread_raw_eof_cstr (
	struct gleip_lines	* const GLEIP_RSTR lines,
	const char		* * const src,
	int			(* callback_tkn_delim) (int)
	)
{
	assert	(NULL != lines);
	assert	(NULL != src);
	assert	(NULL != * src);

	return	gleip_lineread_raw_eof(lines, src,
		(int (*) (void *))gleip_lineread_cb_data_next_cstr,
		callback_tkn_delim);
}

int
gleip_lineread_raw_eof_cstr_nl (
	struct gleip_lines	* const GLEIP_RSTR lines,
	const char		* * const src
	)
{
	assert	(NULL != lines);
	assert	(NULL != src);
	assert	(NULL != * src);

	return	gleip_lineread_raw_eof_cstr(lines, src,
			gleip_lineread_cb_token_delim_nl);
}

int
gleip_lineread_interp (
	struct gleip_lines	* const GLEIP_RSTR lines,
	void			* const data,
	const size_t		tkns_max,
	const char		condense_sym,
	int			(* callback_data_next) (void *),
	int			(* callback_tkn_delim) (int),
	int			(* callback_line_delim) (int)
	)
{
	char	* buf	= NULL;
	int	chi,
		escape	= 0,
		quoted	= 0,
		quotes	= 0,
		tkn	= 0;
	size_t	tkns	= 0;

	assert	(NULL != lines);
	assert	('\0' != condense_sym);
	assert	(NULL != data);
	assert	(NULL != callback_data_next);
	assert	(tkns_max < (size_t)1
/*@+tmpcomments@*/
/*@t1@*/\
	||	gleip_lines_empty(lines));
/*@=tmpcomments@*/

	buf	= gleip_lineread_buf_get(lines);

	while	(EOF != (chi = callback_data_next(data)))
	{
		if	('\0' == (char)chi)
		{
			continue;
		}

/*@+tmpcomments@*/
		if	(NULL != callback_line_delim
/*@t1@*/\
		&&	callback_line_delim(chi)
/*@t1@*/\
		&&	!quoted
/*@t1@*/\
		&&	!quotes)
		{
/*@t1@*/\
			if	(escape)
/*@=tmpcomments@*/
			{
				escape	= 0;
				continue;
			}
			else
			{
				break;
			}
		}
/*@+tmpcomments@*/
/*@t1@*/\
		else if	(NULL != callback_tkn_delim
/*@t1@*/\
		&&	callback_tkn_delim(chi)
/*@t1@*/\
		&&	!escape
/*@t1@*/\
		&&	!quoted
/*@t1@*/\
		&&	!quotes)
/*@=tmpcomments@*/
		{
			tkn	= 1;
			continue;
		}

/*@+tmpcomments@*/
/*@t1@*/\
		if	(escape)
/*@=tmpcomments@*/
		{
			switch	((char)chi)
			{
				case	'a':
					chi	= (int)'\a';
/*@switchbreak@*/
					break;
				case	'b':
					chi	= (int)'\b';
/*@switchbreak@*/
					break;
				case	'f':
					chi	= (int)'\f';
/*@switchbreak@*/
					break;
				case	'n':
					chi	= (int)'\n';
/*@switchbreak@*/
					break;
				case	'r':
					chi	= (int)'\r';
/*@switchbreak@*/
					break;
				case	't':
					chi	= (int)'\t';
/*@switchbreak@*/
					break;
				case	'v':
					chi	= (int)'\v';
/*@switchbreak@*/
					break;
				default:
/*@switchbreak@*/
					break;
			}
			escape	= 0;
		}
/*@+tmpcomments@*/
/*@t1@*/\
		else if	(quoted)
/*@=tmpcomments@*/
		{
			switch	((char)chi)
			{
				case	GLEIP_LINEREAD_ESCAPE:
					escape	= 1;
					goto	CONTINUE;
				case	GLEIP_LINEREAD_QUOTED:
					quoted	= 0;
					goto	CONTINUE;
				default:
/*@switchbreak@*/
					break;
			}
		}
/*@+tmpcomments@*/
/*@t1@*/\
		else if	(quotes)
/*@=tmpcomments@*/
		{
			switch	((char)chi)
			{
				case	GLEIP_LINEREAD_QUOTES:
					quotes	= 0;
					goto	CONTINUE;
				default:
/*@switchbreak@*/
					break;
			}
		}
		else
		{
			switch	((char)chi)
			{
				case	GLEIP_LINEREAD_COMMENT:
					while	(EOF != (chi =
					callback_data_next(data)))
					{
						if (NULL !=
						callback_line_delim
/*@+tmpcomments@*/
/*@t1@*/\
						&& callback_line_delim
							(chi))
/*@=tmpcomments@*/
						{
/*@innerbreak@*/					break;
						}
					}
					goto	BREAK;
				case	GLEIP_LINEREAD_ESCAPE:
					escape	= 1;
					goto	CONTINUE;
				case	GLEIP_LINEREAD_QUOTED:
					quoted	= 1;
					goto	CONTINUE;
				case	GLEIP_LINEREAD_QUOTES:
					quotes	= 1;
					goto	CONTINUE;
				default:
/*@switchbreak@*/
					break;
			}
		}

		if	(NULL == buf)
		{
			buf	= gleip_line_alloc_len((size_t)40);
			if	(NULL == buf)
			{
				goto	RETURN_FAIL;
			}
		}
		assert	(NULL != buf);

/*@+tmpcomments@*/
/*@t1@*/\
		if	(tkn)
/*@=tmpcomments@*/
		{
			if	(tkns_max < (size_t)1
			||	tkns + 1 < tkns_max)
			{
/*@+tmpcomments@*/
/*@t1@*/\
				if	(!gleip_line_empty(buf))
				{
/*@t1@*/\
					if (!gleip_lines_addcpy_back
/*@=tmpcomments@*/
							(lines, buf))
					{
						goto	RETURN_FAIL;
					}
					gleip_line_rm	(buf);
					++tkns;
				}
			}
			else
			{
/*@+tmpcomments@*/
/*@t1@*/\
				if	(!gleip_line_add_ch_back_b
						(& buf, condense_sym))
/*@=tmpcomments@*/
				{
					goto	RETURN_FAIL;
				}
			}
			tkn	= 0;
		}

/*@+tmpcomments@*/
/*@t1@*/\
		if	(!gleip_line_add_ch_back_b(& buf, (char)chi))
/*@=tmpcomments@*/
		{
			goto	RETURN_FAIL;
		}
		CONTINUE:;	/* GCC */
	}
	BREAK:

	if	(NULL != buf)
	{
/*@+tmpcomments@*/
/*@t1@*/\
		if	(gleip_line_empty(buf))
/*@=tmpcomments@*/
		{
			char	* const worst =
				gleip_lineread_buf_save(lines, buf);
			if	(NULL != worst)
			{
				free	(worst);
			}
			buf	= NULL;
		}
		else
		{
			assert	(tkns_max < (size_t)1
			||	tkns < tkns_max);
			assert	(NULL != buf);
/*@+tmpcomments@*/
/*@t1@*/\
			if	(gleip_lines_addref_back(lines, buf))
/*@=tmpcomments@*/
			{
				buf	= NULL;
				/* ++tkns */
			}
			else
			{
				goto	RETURN_FAIL;
			}
		}
	}
	assert	(NULL == buf);

	return	EOF == chi
		? EOF
		: 1;

	RETURN_FAIL:
	if	(NULL != buf)
	{
		char	* worst	= NULL;
		gleip_line_rm	(buf);
		worst	= gleip_lineread_buf_save(lines, buf);
		if	(NULL != worst)
		{
			free	(worst);
		}
	}
	return	0;
/*@+tmpcomments@*/
/*@t1@*/\
}
/*@=tmpcomments@*/

int
gleip_lineread_interp_def (
	struct gleip_lines	* const GLEIP_RSTR lines,
	void			* const data,
	const size_t		tkns_max,
	int			(* callback_data_next) (void *)
	)
{
	assert	(NULL != lines);
	assert	(NULL != data);
	assert	(NULL != callback_data_next);

	return	gleip_lineread_interp(lines, data, tkns_max,
			GLEIP_LINEREAD_CONDENSE_SYM_DEF,
			callback_data_next,
/*@+tmpcomments@*/
/*@t1@*/\
			isspace,
/*@=tmpcomments@*/
			gleip_lineread_cb_token_delim_nl);
}

int
gleip_lineread_interp_file (
	struct gleip_lines	* const GLEIP_RSTR lines,
	FILE			* const file,
	const size_t		tkns_max,
	const char		condense_sym,
	int			(* callback_tkn_delim) (int),
	int			(* callback_line_delim) (int)
	)
{
	assert	(NULL != lines);
	assert	(NULL != file);
	assert	('\0' != condense_sym);

	return	gleip_lineread_interp(lines, file, tkns_max,
			condense_sym,
			(int (*) (void *))fgetc,
/*@+voidabstract@*/
			callback_tkn_delim,
			callback_line_delim);
/*@=voidabstract@*/
}

int
gleip_lineread_interp_file_def (
	struct gleip_lines	* const GLEIP_RSTR lines,
	FILE			* const file,
	const size_t		tkns_max
	)
{
	assert	(NULL != lines);
	assert	(NULL != file);

	return	gleip_lineread_interp_file(lines, file,
			tkns_max, GLEIP_LINEREAD_CONDENSE_SYM_DEF,
/*@+tmpcomments@*/
/*@t1@*/\
			isspace,
/*@=tmpcomments@*/
			gleip_lineread_cb_token_delim_nl);
}

int
gleip_lineread_interp_cstr (
	struct gleip_lines	* const GLEIP_RSTR lines,
	const char		* * const src,
	const size_t		tkns_max,
	const char		condense_sym,
	int			(* callback_tkn_delim) (int),
	int			(* callback_line_delim) (int)
	)
{
	assert	(NULL != lines);
	assert	(NULL != src);
	assert	(NULL != * src);
	assert	('\0' != condense_sym);

	return	gleip_lineread_interp(lines, src, tkns_max,
			condense_sym,
		(int (*) (void *))gleip_lineread_cb_data_next_cstr,
			callback_tkn_delim,
			callback_line_delim);
}

int
gleip_lineread_interp_cstr_def (
	struct gleip_lines	* const GLEIP_RSTR lines,
	const char		* * const src,
	const size_t		tkns_max
	)
{
	assert	(NULL != lines);
	assert	(NULL != src);
	assert	(NULL != * src);

	return	gleip_lineread_interp_cstr(lines, src,
			tkns_max, GLEIP_LINEREAD_CONDENSE_SYM_DEF,
/*@+tmpcomments@*/
/*@t1@*/\
			isspace,
/*@=tmpcomments@*/
			gleip_lineread_cb_token_delim_nl);
}

