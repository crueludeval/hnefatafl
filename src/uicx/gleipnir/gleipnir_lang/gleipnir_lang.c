/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <stdlib.h>	/* malloc, free */
#include <string.h>	/* strcmp */

#include "gleipnir_line.h"	/* gleip_line_* */
#include "gleipnir_lineread.h"	/* gleip_lineread_* */

#include "gleipnir_lang.h"

const long	GLEIP_LANG_VERSION	= 140816L;

/*@unchecked@*/
static
const size_t	GLEIP_LANG_MEM_DEF	= (size_t)9 * (size_t)3;

size_t
gleip_lang_size (
	const struct gleip_lang	* const GLEIP_RSTR lang
	)
{
	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));

	return	gleip_lines_size(lang->keys);
}

const char *
gleip_lang_key (
	const struct gleip_lang	* const GLEIP_RSTR lang,
	const size_t		i
	)
{
	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
	assert	(i < gleip_lang_size(lang));

	return	gleip_lines_get_ro(lang->keys, i);
}

const char *
gleip_lang_value (
	const struct gleip_lang	* const GLEIP_RSTR lang,
	const size_t		i
	)
{
	const char	* value	= NULL;

	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
	assert	(i < gleip_lang_size(lang));

	value	= gleip_lines_get_ro(lang->values, i);
/*@+tmpcomments@*/
/*@t1@*/\
	return	gleip_line_empty(value)
/*@=tmpcomments@*/
		? NULL
		: value;
}

int
gleip_lang_mem_trim (
	struct gleip_lang	* const GLEIP_RSTR lang
	)
{
	int	success;

	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));

	success	= gleip_lines_mem_trim(lang->keys);
/*@+tmpcomments@*/
/*@t2@*/\
	return	gleip_lines_mem_trim(lang->values)
		&&	success;
/*@=tmpcomments@*/
}

static
size_t
gleip_lang_key_index (
/*@in@*/
/*@notnull@*/
	const struct gleip_lang	* const GLEIP_RSTR lang,
/*@in@*/
/*@notnull@*/
	const char		* const GLEIP_RSTR key
	)
/*@*/
{
	size_t	i;

	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
	assert	(NULL != key);

	for	(i = 0; i < gleip_lang_size(lang); ++i)
	{
		if	(0 == strcmp(key,
			gleip_lines_get_ro(lang->keys, i)))
		{
			return	i;
		}
	}
	return	gleip_lang_size(lang);
}

const char *
gleip_lang_get (
	const struct gleip_lang	* const GLEIP_RSTR lang,
	const char		* const GLEIP_RSTR key
	)
{
	const char	* value	= NULL;
	size_t		i;

	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
	assert	(NULL != key);
/*@+tmpcomments@*/
/*@t1@*/\
	assert	(gleip_lang_isreg(lang, key));
/*@=tmpcomments@*/

	i	= gleip_lang_key_index(lang, key);
	assert	(i < gleip_lang_size(lang));

	value	= gleip_lines_get_ro(lang->values, i);

/*@+tmpcomments@*/
/*@t1@*/\
	return	gleip_line_empty(value)
/*@=tmpcomments@*/
		? NULL
		: value;
}

const char *
gleip_lang_getany (
	const struct gleip_lang	* const GLEIP_RSTR lang,
	const char		* const GLEIP_RSTR key
	)
{
	const char	* GLEIP_RSTR value	= NULL;

	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
	assert	(NULL != key);

	value	= gleip_lang_get(lang, key);
	return	NULL == value
		? key
		: value;
}

const char *
gleip_lang_invalid_why (
	const struct gleip_lang	* const GLEIP_RSTR lang
	)
{
	size_t	i;

	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));

	for	(i = 0; i < gleip_lang_size(lang); ++i)
	{
		const char * const GLEIP_RSTR value =
			gleip_lines_get_ro(lang->values, i);

		assert	(NULL != value);

/*@+tmpcomments@*/
/*@t1@*/\
		if	(gleip_line_empty(value))
/*@=tmpcomments@*/
		{
			return	gleip_lines_get(lang->keys, i);
		}
	}
	return	NULL;
}

int
gleip_lang_valid (
	const struct gleip_lang	* const GLEIP_RSTR lang
	)
{
	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));

/*@+tmpcomments@*/
/*@t1@*/\
	return	NULL == gleip_lang_invalid_why(lang);
/*@=tmpcomments@*/
}

void
gleip_lang_clear (
	struct gleip_lang	* const GLEIP_RSTR lang
	)
{
	size_t	i;

	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));

	for	(i = 0; i < gleip_lang_size(lang); ++i)
	{
		char	* const GLEIP_RSTR value =
			gleip_lines_get(lang->values, i);
		assert	(NULL != value);
		gleip_line_rm(value);
	}
}

int
gleip_lang_read (
	struct gleip_lang	* const GLEIP_RSTR lang,
	void			* const data,
	struct gleip_lines	* GLEIP_RSTR buf,
	int			* const GLEIP_RSTR valid,
	int			(* callback_data_next) (void *)
	)
{
	int	owned,
		retval	= 1,
		readval	= 1;

	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
	assert	(NULL != data);
	assert	(NULL != callback_data_next);

	gleip_lang_clear(lang);

	if	(NULL != valid)
	{
		* valid	= 1;
	}

/*@+tmpcomments@*/
/*@t1@*/\
	owned	= NULL == buf;
/*@=tmpcomments@*/
	if	(NULL == buf)
	{
		buf	= gleip_lines_alloc_mem((size_t)3);
		if	(NULL == buf)
		{
			return	0;
		}
	}

	do
	{
		char		* key		= NULL,
				* value		= NULL;
		size_t		i_key;
		gleip_lines_rm	(buf);
		readval		= gleip_lineread_interp_def
			(buf, data, (size_t)2, callback_data_next);
/*@+tmpcomments@*/
/*@t1@*/\
		if	(!readval)
/*@=tmpcomments@*/
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
		assert	(gleip_lines_size(buf) <= (size_t)2);

/*@+tmpcomments@*/
/*@t1@*/\
		if	(gleip_lines_empty(buf))
/*@=tmpcomments@*/
		{
			continue;
		}

		key	= gleip_lines_get(buf, 0);
		value	= gleip_lines_get(buf, (size_t)1);
		assert	(NULL != key);
		assert	(NULL != value);

		if	(gleip_lines_size(buf) < (size_t)2
/*@+tmpcomments@*/
/*@t1@*/\
		||	gleip_line_empty(key)
/*@t1@*/\
		||	gleip_line_empty(value))
/*@=tmpcomments@*/
		{
			if	(NULL != valid)
			{
				* valid	= 0;
			}
			continue;
		}
/*@+tmpcomments@*/
/*@t1@*/\
		assert	(!gleip_line_empty(key));
/*@t1@*/\
		assert	(!gleip_line_empty(value));
/*@=tmpcomments@*/

		i_key	= gleip_lang_key_index(lang, key);
		if	(i_key >= gleip_lang_size(lang))
		{
			if	(NULL != valid)
			{
				* valid	= 0;
			}
			continue;
		}

/*@+tmpcomments@*/
/*@t1@*/\
		if	(!gleip_line_empty(gleip_lines_get(lang->values,
							i_key))
/*@=tmpcomments@*/
		&&	NULL != valid)
		{
			* valid	= 0;
		}

/*@+tmpcomments@*/
/*@t1@*/\
		if	(!gleip_line_cpy_b
			(gleip_lines_get_ptr(lang->values, i_key),
						value))
/*@=tmpcomments@*/
		{
			retval	= 0;
			goto	RETURN_VAL;
		}
	}
	while	(EOF != readval);

	if	(NULL != valid
/*@+tmpcomments@*/
/*@t2@*/\
	&&	* valid)
/*@=tmpcomments@*/
	{
		* valid	= gleip_lang_valid(lang);
	}

	RETURN_VAL:
/*@+tmpcomments@*/
/*@t1@*/\
	if	(owned)
	{
		assert	(NULL != buf);
/*@t3@*/\
		gleip_lines_free(buf);
/*@t4@*/\
	}
/*@=tmpcomments@*/
	return	retval;
}

int
gleip_lang_read_file (
	struct gleip_lang	* const GLEIP_RSTR lang,
	FILE			* const file,
	struct gleip_lines	* GLEIP_RSTR buf,
	int			* const GLEIP_RSTR valid
	)
{
	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
	assert	(NULL != file);

	return	gleip_lang_read(lang, file, buf, valid,
/*@+voidabstract@*/
			(int (*)(void *))fgetc);
/*@=voidabstract@*/
}

int
gleip_lang_read_cstr (
	struct gleip_lang	* const GLEIP_RSTR lang,
	const char		* * const src,
	struct gleip_lines	* GLEIP_RSTR buf,
	int			* const GLEIP_RSTR valid
	)
{
	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
	assert	(NULL != src);
	assert	(NULL != * src);

	return	gleip_lang_read(lang, src, buf, valid,
		(int (*)(void *))gleip_lineread_cb_data_next_cstr);
}

int
gleip_lang_isreg (
	const struct gleip_lang	* const GLEIP_RSTR lang,
	const char		* const key
	)
{
	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
	assert	(NULL != key);
/*@+tmpcomments@*/
/*@t1@*/\
	assert	(!gleip_line_empty(key));

/*@t1@*/\
	return	gleip_lang_key_index(lang, key)
		< gleip_lang_size(lang);
/*@=tmpcomments@*/
}

int
gleip_lang_regcpy (
	struct gleip_lang	* const GLEIP_RSTR lang,
	const char		* const key
	)
{
	char	* GLEIP_RSTR val	= NULL;

	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
	assert	(NULL != key);
/*@+tmpcomments@*/
/*@t1@*/\
	assert	(!gleip_line_empty(key));
/*@t1@*/\
	assert	(!gleip_lang_isreg(lang, key));
/*@=tmpcomments@*/

	val	= gleip_line_alloc_len((size_t)9 * (size_t)3);
	if	(NULL == val)
	{
		return	0;
	}
/*@+tmpcomments@*/
/*@t1@*/\
	if	(!gleip_lines_addref_back(lang->values, val))
	{
/*@t1@*/\
		free	(val);
/*@=tmpcomments@*/
		return	0;
	}
	assert	(gleip_lines_size(lang->values) > 0);
	assert	(gleip_lines_size(lang->values) > 0);

/*@+tmpcomments@*/
/*@t1@*/\
	if	(gleip_lines_addcpy_back(lang->keys, key))
/*@=tmpcomments@*/
	{
		assert	(	gleip_lines_size(lang->keys)
			==	gleip_lines_size(lang->values));
		return	1;
	}
	else
	{
		assert	(gleip_lines_size(lang->values) > 0);
		gleip_lines_rm_line_back(lang->values);

		assert	(	gleip_lines_size(lang->keys)
			==	gleip_lines_size(lang->values));
		return	0;
	}
}

int
gleip_lang_regref (
	struct gleip_lang	* const GLEIP_RSTR lang,
	char			* const ref
	)
{
	char	* GLEIP_RSTR val	= NULL;

	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
	assert	(NULL != ref);
/*@+tmpcomments@*/
/*@t1@*/\
	assert	(!gleip_line_empty(ref));
/*@t1@*/\
	assert	(!gleip_lang_isreg(lang, ref));
/*@=tmpcomments@*/

	val	= gleip_line_alloc_len((size_t)9 * (size_t)3);
	if	(NULL == val)
	{
/*@+tmpcomments@*/
/*@t1@*/\
		return	0;
	}
/*@t1@*/\
	if	(!gleip_lines_addref_back(lang->values, val))
	{
/*@t1@*/\
		free	(val);
/*@t1@*/\
		return	0;
/*@=tmpcomments@*/
	}
	assert	(gleip_lines_size(lang->values) > 0);

/*@+tmpcomments@*/
/*@t1@*/\
	if	(gleip_lines_addref_back(lang->keys, ref))
/*@=tmpcomments@*/
	{
		assert	(	gleip_lines_size(lang->keys)
			==	gleip_lines_size(lang->values));
		return	1;
	}
	else
	{
		assert	(gleip_lines_size(lang->values) > 0);
		gleip_lines_rm_line_back(lang->values);

		assert	(	gleip_lines_size(lang->keys)
			==	gleip_lines_size(lang->values));
		return	0;
	}
}

void
gleip_lang_unreg_i (
	struct gleip_lang	* const GLEIP_RSTR lang,
	const size_t		i
	)
{
	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
	assert	(i < gleip_lang_size(lang));

	gleip_lines_rm_line(lang->keys,		i);
	gleip_lines_rm_line(lang->values,	i);

	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
}

void
gleip_lang_unreg (
	struct gleip_lang	* const GLEIP_RSTR lang,
	const char		* const key
	)
{
	size_t	i;

	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
/*@+tmpcomments@*/
/*@t1@*/\
	assert	(!gleip_line_empty(key));
/*@=tmpcomments@*/

	i	= gleip_lang_key_index(lang, key);
	assert	(i < gleip_lang_size(lang));

	gleip_lang_unreg_i(lang, i);

	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
}

void
gleip_lang_unreg_all (
	struct gleip_lang	* const GLEIP_RSTR lang
	)
{
	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));

	gleip_lines_rm(lang->keys);
	gleip_lines_rm(lang->values);

	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
}

int
gleip_lang_print (
	const struct gleip_lang	* const GLEIP_RSTR lang,
	FILE			* const GLEIP_RSTR out,
	const char		sep
	)
{
	size_t	i;

	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
	assert	(NULL != out);

	for	(i = 0; i < gleip_lang_size(lang); ++i)
	{
		const char * const GLEIP_RSTR key =
			gleip_lines_get_ro(lang->keys, i);
		assert	(NULL != key);
/*@+tmpcomments@*/
/*@t1@*/\
		assert	(!gleip_line_empty(key));
/*@=tmpcomments@*/
		if	(EOF == fprintf(out, "%s%c", key, sep))
		{
			return	0;
		}
	}
	return	1;
}

int
gleip_lang_cpy (
	struct gleip_lang	* const GLEIP_RSTR lang,
	const struct gleip_lang	* const GLEIP_RSTR orig
	)
{
	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));
	assert	(NULL != orig);
	assert	(NULL != orig->keys);
	assert	(NULL != orig->values);
	assert	(orig->keys != orig->values);
	assert	(	gleip_lines_size(orig->keys)
		==	gleip_lines_size(orig->values));
	assert	(lang != orig);

	gleip_lang_unreg_all(lang);
	assert	(gleip_lang_size(lang)	< (size_t)1);
	assert	(lang->keys->size	< (size_t)1);
	assert	(lang->values->size	< (size_t)1);

/*@+tmpcomments@*/
/*@t1@*/\
	if	(!gleip_lines_cpy_back(lang->keys, orig->keys))
/*@=tmpcomments@*/
	{
		assert	(gleip_lang_size(lang)	< (size_t)1);
		assert	(lang->keys->size	< (size_t)1);
		assert	(lang->values->size	< (size_t)1);
		return	0;
	}
/*@+tmpcomments@*/
/*@t1@*/\
	if	(!gleip_lines_cpy_back(lang->values, orig->values))
/*@=tmpcomments@*/
	{
		gleip_lines_rm(lang->keys);
		assert	(gleip_lang_size(lang)	< (size_t)1);
		assert	(lang->keys->size	< (size_t)1);
		assert	(lang->values->size	< (size_t)1);
		return	0;
	}

	assert	(gleip_lang_size(lang) == gleip_lang_size(orig));
	return	1;
}

void
gleip_lang_free (
	struct gleip_lang	* const GLEIP_RSTR lang
	)
{
	assert	(NULL != lang);
	assert	(NULL != lang->keys);
	assert	(NULL != lang->values);
	assert	(lang->keys != lang->values);
	assert	(	gleip_lines_size(lang->keys)
		==	gleip_lines_size(lang->values));

	gleip_lines_free	(lang->keys);
	gleip_lines_free	(lang->values);
	free			(lang);
}

struct gleip_lang *
gleip_lang_alloc_mem (
	const size_t	mem
	)
{
	struct gleip_lang	* GLEIP_RSTR lang	= NULL;

	assert	(mem > 0);

	lang	= malloc(sizeof(* lang));
	if	(NULL == lang)
	{
		return	NULL;
	}

	lang->keys	= gleip_lines_alloc_mem(mem);
	if	(NULL == lang->keys)
	{
		free	(lang);
		return	NULL;
	}

	lang->values	= gleip_lines_alloc_mem(mem);
	if	(NULL == lang->values)
	{
		gleip_lines_free	(lang->keys);
		free			(lang);
		return			NULL;
	}

	return	lang;
}

struct gleip_lang *
gleip_lang_alloc_cpy (
	const struct gleip_lang	* const GLEIP_RSTR orig
	)
{
	struct gleip_lang	* GLEIP_RSTR lang	= NULL;

	assert	(NULL != orig);
	assert	(NULL != orig->keys);
	assert	(NULL != orig->values);
	assert	(orig->keys != orig->values);
	assert	(	gleip_lines_size(orig->keys)
		==	gleip_lines_size(orig->values));

	lang	= gleip_lang_alloc_mem(gleip_lang_size(orig));
	if	(NULL == lang)
	{
		return	NULL;
	}

/*@+tmpcomments@*/
/*@t1@*/\
	if	(gleip_lang_cpy(lang, orig))
/*@=tmpcomments@*/
	{
		assert	(NULL != lang);
		assert	(NULL != lang->keys);
		assert	(NULL != lang->values);
		assert	(lang->keys != lang->values);
		assert	(	gleip_lines_size(lang->keys)
			==	gleip_lines_size(lang->values));
		assert	(	gleip_lang_size(lang)
			==	gleip_lang_size(orig));
		return	lang;
	}
	else
	{
		gleip_lang_free(lang);
		return	NULL;
	}
}

struct gleip_lang *
gleip_lang_alloc (void)
{
	return	gleip_lang_alloc_mem(GLEIP_LANG_MEM_DEF);
}

