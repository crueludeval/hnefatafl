/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef	GLEIP_LINE_H
#define	GLEIP_LINE_H

#include <stddef.h>	/* size_t */

#if	defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#define GLEIP_RSTR restrict
#else
#define	GLEIP_RSTR
#endif

/*@unchecked@*/
/*@unused@*/
extern
const long	GLEIP_LINE_VERSION;

/*@unused@*/
extern
int
gleip_line_empty (
/*@in@*/
/*@notnull@*/
	const char	* const GLEIP_RSTR
	)
/*@*/
;

/*@unused@*/
extern
size_t
gleip_line_mem (
/*@in@*/
/*@notnull@*/
	const char	* const GLEIP_RSTR
	)
/*@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
char *
gleip_line_mem_grow (
/*@in@*/
/*@notnull@*/
/*@only@*/
/*@returned@*/
/*@-protoparamname@*/
	char		* line,
/*@=protoparamname@*/
	const size_t
	)
/*@modifies * line@*/
;

/*@unused@*/
extern
int
gleip_line_mem_grow_b (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char		* * const line,
/*@=protoparamname@*/
	const size_t
	)
/*@modifies * * line, * line@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
char *
gleip_line_mem_trim (
/*@in@*/
/*@notnull@*/
/*@only@*/
/*@returned@*/
/*@-protoparamname@*/
	char	* line
/*@=protoparamname@*/
	)
/*@modifies * line@*/
;

/*@unused@*/
extern
int
gleip_line_mem_trim_b (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char	* * const line
/*@=protoparamname@*/
	)
/*@modifies * * line, * line@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
char *
gleip_line_add_str (
/*@in@*/
/*@notnull@*/
/*@only@*/
/*@returned@*/
/*@-protoparamname@*/
	char		* line,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
	const char	* const,
	const size_t
	)
/*@modifies * line@*/
;

/*@unused@*/
extern
int
gleip_line_add_str_b (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char		* * const line,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
	const char	* const,
	const size_t
	)
/*@modifies * * line, * line@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
char *
gleip_line_add_str_back (
/*@in@*/
/*@notnull@*/
/*@only@*/
/*@returned@*/
/*@-protoparamname@*/
	char		* line,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
	const char	* const
	)
/*@modifies * line@*/
;

/*@unused@*/
extern
int
gleip_line_add_str_back_b (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char		* * const line,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
	const char	* const
	)
/*@modifies * * line, * line@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
char *
gleip_line_add_ch (
/*@in@*/
/*@notnull@*/
/*@only@*/
/*@returned@*/
/*@-protoparamname@*/
	char		* line,
/*@=protoparamname@*/
	const char,
	const size_t
	)
/*@modifies * line@*/
;

/*@unused@*/
extern
int
gleip_line_add_ch_b (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char		* * const line,
/*@=protoparamname@*/
	const char,
	const size_t
	)
/*@modifies * * line, * line@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
char *
gleip_line_add_ch_back (
/*@in@*/
/*@notnull@*/
/*@only@*/
/*@returned@*/
/*@-protoparamname@*/
	char		* line,
/*@=protoparamname@*/
	const char
	)
/*@modifies * line@*/
;

/*@unused@*/
extern
int
gleip_line_add_ch_back_b (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char		* * const line,
/*@=protoparamname@*/
	const char
	)
/*@modifies * * line, * line@*/
;

/*@unused@*/
extern
void
gleip_line_rm_str (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char		* GLEIP_RSTR line,
/*@=protoparamname@*/
	const size_t,
	const size_t
	)
/*@modifies * line@*/
;

/*@unused@*/
extern
void
gleip_line_rm_str_last (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char		* GLEIP_RSTR line,
/*@=protoparamname@*/
	const size_t
	)
/*@modifies * line@*/
;

/*@unused@*/
extern
void
gleip_line_rm_ch (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char		* GLEIP_RSTR line,
/*@=protoparamname@*/
	const size_t
	)
/*@modifies * line@*/
;

/*@unused@*/
extern
void
gleip_line_rm_ch_last (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char	* GLEIP_RSTR line
/*@=protoparamname@*/
	)
/*@modifies * line@*/
;

/*@unused@*/
extern
void
gleip_line_rm_from (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char		* GLEIP_RSTR line,
/*@=protoparamname@*/
	const size_t
	)
/*@modifies * line@*/
;

/*@unused@*/
extern
void
gleip_line_rm_to (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char		* GLEIP_RSTR line,
/*@=protoparamname@*/
	const size_t
	)
/*@modifies * line@*/
;

/*@unused@*/
extern
void
gleip_line_rm (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char	* GLEIP_RSTR line
	)
/*@modifies * line@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
char *
gleip_line_cpy (
/*@in@*/
/*@notnull@*/
/*@only@*/
/*@returned@*/
/*@-protoparamname@*/
	char		* line,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
	const char	* const
	)
/*@modifies * line@*/
;

/*@unused@*/
extern
int
gleip_line_cpy_b (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	char		* * const line,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
	const char	* const
	)
/*@modifies * * line, * line@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
char *
gleip_line_alloc_mem (
	const size_t
	)
/*@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
char *
gleip_line_alloc_len (
	const size_t
	)
/*@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
char *
gleip_line_alloc (void)
/*@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
char *
gleip_line_alloc_cpy (
/*@in@*/
/*@notnull@*/
	const char	* const
	)
/*@*/
;

#endif

