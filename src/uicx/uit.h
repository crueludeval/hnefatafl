/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_UI_UIT_H
#define HNEF_UI_UIT_H

#include "gleipnir_lang.h"	/* gleip_lang */
#include "gleipnir_line.h"	/* gleip_line */

#include "hnefatafl.h"

#include "controlt.h"	/* hnef_control */
#include "filefindt.h"	/* hnef_filefind */

#ifdef	HNEFATAFL_UI_XLIB
#include "uixt.h"	/* hnef_uix */
#endif	/* HNEFATAFL_UI_XLIB */

#ifdef	HNEFATAFL_UI_AIM
/*
 * Only used to keep track of AI thinking progress.
 */
struct hnef_uic
{

	unsigned int	progress_line,
			progress_linec;

	int		chars_printed,
			chars_print_max;

};
#endif	/* HNEFATAFL_UI_AIM */

struct hnef_ui
{

	/*
	 * Return value of `setlocale()`.
	 */
/*@null@*/
/*@observer@*/
	const char		* setlocale_return;

	/*
	 * `game_tmp` is used for operations that destroy the current
	 * ruleset, i.e. loading a new ruleset. This lets us preserve
	 * the old ruleset in case it doesn't work.
	 */
/*@notnull@*/
/*@owned@*/
	struct hnef_game	* game,
				* game_tmp;

	/*
	 * Array of controllers.
	 */
/*@notnull@*/
/*@owned@*/
	struct hnef_control	* * controlv;

	/*
	 * Length of `controlv`, this is always `HNEF_PLAYERS_MAX`.
	 */
	unsigned short		controlc;

	/*
	 * `lang_tmp` works like `game_tmp` but for loading language
	 * files.
	 */
/*@notnull@*/
/*@owned@*/
	struct gleip_lang	* lang,
				* lang_tmp;

/*@notnull@*/
/*@owned@*/
	struct hnef_filefind	* filefind;

#ifdef	HNEFATAFL_UI_AIM
	struct hnef_uic		uic;
#endif	/* HNEFATAFL_UI_AIM */

#ifdef	HNEFATAFL_UI_XLIB
	/*
	 * This is lazily allocated iff X is started and will be `NULL`
	 * until that, or if it fails to initialize X.
	 */
/*@null@*/
/*@owned@*/
	struct hnef_uix		* uix;
#endif	/* HNEFATAFL_UI_XLIB */

	/*
	 * `quit` is true when the program should quit. When this is set
	 * to true, there's no going back.
	 */
	HNEF_BOOL		quit;

	/*
	 * List of possible moves as returned by
	 * `hnef_game_moves_get_pos()`. The CLI and XLib can use this to
	 * avoid having to allocate a list all the time.
	 */
/*@notnull@*/
/*@owned@*/
	struct hnef_listm	* opt_movelist;

	/*
	 * Line used for reading ruleset so we don't have to allocate a
	 * new one all the time.
	 */
/*@notnull@*/
/*@owned@*/
	struct gleip_lines	* opt_rulesetline;

};

#endif

