/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <stdio.h>	/* fgetc */
#include <stdlib.h>	/* malloc */
#include <string.h>	/* strlen */

#include "line.h"
#include "linet.h"	/* HNEF_PARSE_* */

/*@unchecked@*/
static
const size_t	HNEF_LINELEN_DEF	= (size_t)80;

static
HNEF_BOOL
hnef_is_comment (
	const char	ch
	)
/*@*/
{
/*@observer@*/
	const char * HNEF_RSTR ptr = HNEF_PARSE_COMMENTS;
	assert	('\0' != ch);
	while	('\0' != * ptr)
	{
		if	(* ptr == ch)
		{
			return HNEF_TRUE;
		}
		++ptr;
	}
	return HNEF_FALSE;
}

/*
 * Reads a whole line (terminated by '\n') from `in`.
 *
 * Newlines and other characters can be escaped, however escaping
 * characters has no effect.
 *
 * `line` may be `NULL`. If not, `line` is used for reading and is then
 * returned. If it is `NULL`, then a new buffer is allocated for reading
 * and is then returned. `mem` is the amount of chars allocated in
 * `line`. The returned buffer has `mem` allocated chars and must be
 * freed by the caller.
 *
 * Note that the returned buffer will be `NULL` if there is nothing to
 * read from `in`.
 */
char *
hnef_line_read (
	FILE		* const HNEF_RSTR in,
	char		* line,
	size_t		* const HNEF_RSTR mem,
	enum HNEF_FR	* const HNEF_RSTR fr
	)
{
	int		chi;
	size_t		i	= 0;
	HNEF_BOOL	escape	= HNEF_FALSE;

	assert	(NULL != in);
	assert	(NULL != mem);
	assert	(NULL != fr);

	* fr	= HNEF_FR_SUCCESS;

	if	(NULL != line)
	{
		line[0]	= '\0';
	}

	while	(EOF != (chi = fgetc(in)))
	{
		if	('\n' == (char)chi)
		{
			if	(escape)
			{
				escape = HNEF_FALSE;
				continue;
			}
			else
			{
				break;
			}
		}

		if	(escape)
		{
			escape = HNEF_FALSE;
		}
		else if	(HNEF_PARSE_ESCAPE == (char)chi)
		{
			escape = HNEF_TRUE;
			continue;
		}
/*@i1@*/\
		else if	(hnef_is_comment((char)chi))
		{
			while	(EOF != (chi = fgetc(in))
			&&	'\n' != (char)chi)
			{
				/*
				 * Skip to EOL.
				 */
			}
			break;
		}

		if	(NULL == line)
		{
			* mem	= HNEF_LINELEN_DEF + 1;
			line	= malloc(* mem);
			if	(NULL == line)
			{
				* mem	= 0;
				* fr	= HNEF_FR_FAIL_ALLOC;
				return	NULL;
			}
		}
/*@i2@*/\
		else if	(i + 2 >= * mem)
		{
/*@owned@*/
			char * line_new = NULL;
			size_t mem_new =
				(size_t)(* mem * 1.5 + 0.00001);
			if	(mem_new <= * mem)
			{
				mem_new = * mem + 1;
			}
			assert	(mem_new > 0);
			line_new = realloc
				(line, mem_new * sizeof(* line));
			if	(NULL == line_new)
			{
				* fr	= HNEF_FR_FAIL_ALLOC;
/*@i1@*/\
				return	line;
			}
			line	= line_new;
			* mem	= mem_new;
			assert	(NULL != line);
/*@i1@*/\
		}

		line[i]		= (char)chi;
		line[++i]	= '\0';
/*@i1@*/\
	}

/*@i1@*/\
	if	(ferror(in))
	{
		* fr	= HNEF_FR_FAIL_IO_FILE_READ;
	}
	return	line;
}

/*
 * Allocates a copy of `str`. The copy must be freed. An interface that
 * uses Gleipnir can treat the returned string as a growable C-string.
 */
char *
hnef_line_alloc_cpy (
	const char	* const HNEF_RSTR str
	)
{
	char	* HNEF_RSTR cpy	= NULL;
	size_t	len;

	assert	(NULL != str);

	len	= strlen(str);
	cpy	= malloc((len + 2) * sizeof(* cpy));
	if	(NULL == cpy)
	{
		return	NULL;
	}
	memcpy	(cpy, str, len + 1);

	/*
	 * This lets Gleipnir treat it as a growable C-string in uicx.
	 * The `NULM` is after '\0' so `strlen` and other functions
	 * shouldn't be affected in other interfaces, since the char
	 * after '\0' is usually unallocated garbage that should never
	 * be read.
	 */
	cpy[len + 1] = '\n';

	return	cpy;
}

HNEF_BOOL
hnef_line_empty (
	const char	* const HNEF_RSTR str
	)
{
	assert	(NULL != str);
	return	'\0' == str[0];
}

