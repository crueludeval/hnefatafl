/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <stdlib.h>	/* NULL */

#include "num.h"	/* hnef_single_bit */
#include "playert.h"	/* HNEF_PLAYER_UNINIT */
#include "types.h"	/* HNEF_TYPEC_* */
#include "type_piece.h"

/*
 * Checks if tp can capture pbit.
 */
HNEF_BOOL
hnef_type_piece_can_capture (
	const struct hnef_type_piece	* const HNEF_RSTR tp,
	const HNEF_BIT_U8		pbit
	)
{
	return	((unsigned int)pbit & (unsigned int)tp->captures)
		== (unsigned int)pbit;
}

/*
 * Checks if tp->occupies allows the piece to occupy a square of type
 * sqbit.
 *
 * HNEF_BIT_U8_EMPTY always returns false.
 */
HNEF_BOOL
hnef_type_piece_can_occupy (
	const struct hnef_type_piece	* const HNEF_RSTR tp,
	const HNEF_BIT_U8		sqbit
	)
{
	if	(HNEF_BIT_U8_EMPTY != sqbit
	&&	((unsigned int)sqbit & (unsigned int)tp->occupies)
		== (unsigned int)sqbit)
	{
		return	HNEF_TRUE;
	}
	else
	{
		return	HNEF_FALSE;
	}
}

/*
 * Checks if tp->noreturn allows the piece to move from pos to dest.
 *
 * This is not allowed if you are moving a from a non-noreturn square to
 * a noreturn square.
 *
 * Ignores if pos or dest is HNEF_BIT_U8_EMPTY.
 */
HNEF_BOOL
hnef_type_piece_can_return (
	const struct hnef_type_piece	* const HNEF_RSTR tp,
	const HNEF_BIT_U8		pos,
	const HNEF_BIT_U8		dest
	)
{
	if	(((unsigned int)pos & (unsigned int)tp->noreturn)
			!= (unsigned int)pos
	&&	((unsigned int)dest & (unsigned int)tp->noreturn)
			== (unsigned int)dest)
	{
		return	HNEF_FALSE;
	}
	else
	{
		return	HNEF_TRUE;
	}
}

/*
 * Initializes to invalid / default values.
 *
 * owner and bit are invalid.
 */
void
hnef_type_piece_init (
	struct hnef_type_piece	* const HNEF_RSTR tp
	)
{
	tp->bit			= HNEF_BIT_U8_EMPTY;
	tp->captures		= HNEF_BIT_U8_EMPTY;
	tp->capt_edge		= HNEF_FALSE;
	tp->capt_loss		= HNEF_FALSE;
	tp->capt_sides		= HNEF_TPIECE_CAPT_SIDES_DEF;
	tp->custodial		= HNEF_FALSE;
	tp->dbl_trap		= HNEF_FALSE;
	tp->dbl_trap_capt	= HNEF_FALSE;
	tp->dbl_trap_compl	= HNEF_FALSE;
	tp->dbl_trap_edge	= HNEF_FALSE;
	tp->dbl_trap_encl	= HNEF_FALSE;
	tp->dbl_trap_squares	= HNEF_BIT_U8_EMPTY;
	tp->escape		= HNEF_FALSE;
	tp->noreturn		= HNEF_BIT_U8_EMPTY;
	tp->occupies		= HNEF_BIT_U8_EMPTY;
	tp->owner		= HNEF_PLAYER_UNINIT;
	tp->ui_bit		= HNEF_BIT_U8_EMPTY;
}

/*
 * Retrieves type_square with matching bit, or an invalid piece type if
 * none is found.
 */
struct hnef_type_piece *
hnef_type_piece_get (
	struct hnef_ruleset	* const HNEF_RSTR rules,
	const HNEF_BIT_U8	bit
	)
{
	return	& rules->type_pieces[hnef_type_index_get(bit)];
}

/*
 * Exactly like type_square_set, but sets a piece type.
 */
enum HNEF_FR
hnef_type_piece_set (
	struct hnef_ruleset	* const HNEF_RSTR rules,
	const HNEF_BIT_U8	bit
	)
{
	unsigned short		index,
				i;
	struct hnef_type_piece	* HNEF_RSTR ts	= NULL;

	assert	(hnef_single_bit((unsigned int)bit)
	||	bit <= (HNEF_BIT_U8)128);

	index	= hnef_type_index_get(bit);
	if	(HNEF_TYPE_INDEX_INVALID == index)
	{
		return	HNEF_FR_FAIL_ILL_ARG;
	}
/*@i1@*/\
	else if	(index > rules->type_piecec)
	{
		return	HNEF_FR_FAIL_NULLPTR;
	}

	for	(i = (unsigned short)0; i < index; ++i)
	{
		if	(HNEF_BIT_U8_EMPTY == rules->type_pieces[i].bit)
		{
			/*
			 * Wrong order: some previous piece not defined.
			 */
			return	HNEF_FR_FAIL_NULLPTR;
		}
	}

	ts	= & rules->type_pieces[index];
	if	(HNEF_BIT_U8_EMPTY != ts->bit)
	{
		/*
		 * This piece type has already been defined.
		 */
		return	HNEF_FR_FAIL_ILL_STATE;
	}

	ts->bit	= bit;
	++rules->type_piecec;
	return	HNEF_FR_SUCCESS;
}

