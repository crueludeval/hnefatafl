/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_RREAD_H
#define HNEF_CORE_RREAD_H

#include "config.h"	/* HNEF_RSTR */
#include "funct.h"	/* HNEF_FR */
#include "gamet.h"	/* game */

/*@-protoparamname@*/
/*@unused@*/
extern
enum HNEF_FR
hnef_game_parseline (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	char			* const line,
/*@out@*/
/*@notnull@*/
	enum HNEF_RREAD		* const HNEF_RSTR fail_read
	)
/*@globals errno, internalState@*/
/*@modifies errno, internalState, * game, * line, * fail_read@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
extern
enum HNEF_FR
hnef_game_read_file (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	FILE			* const HNEF_RSTR file,
/*@out@*/
/*@notnull@*/
	enum HNEF_RREAD		* const HNEF_RSTR fail_read
	)
/*@globals errno, fileSystem, internalState@*/
/*@modifies errno, fileSystem, internalState@*/
/*@modifies * game, * file, * fail_read@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
extern
enum HNEF_FR
hnef_game_init_file (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	FILE			* const HNEF_RSTR file,
/*@out@*/
/*@notnull@*/
	enum HNEF_RREAD		* const HNEF_RSTR fail_read,
/*@out@*/
/*@notnull@*/
	enum HNEF_RVALID	* const HNEF_RSTR fail_valid
	)
/*@globals errno, fileSystem, internalState@*/
/*@modifies errno, fileSystem, internalState@*/
/*@modifies * game, * file, * fail_read, * fail_valid@*/
;
/*@=protoparamname@*/

#endif

