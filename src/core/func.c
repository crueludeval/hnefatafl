/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */

#include "func.h"

/*
 * Returns whether a `HNEF_RREAD_*` code indicates a non-failure.
 */
HNEF_BOOL
hnef_rread_good (
	const enum HNEF_RREAD	code
	)
{
	return HNEF_RREAD_SUCCESS == code;
}

/*
 * Returns whether a `HNEF_RVALID_*` code indicates a non-failure.
 */
HNEF_BOOL
hnef_rvalid_good (
	const enum HNEF_RVALID	code
	)
{
	return HNEF_RVALID_SUCCESS == code;
}

/*
 * Returns whether a `HNEF_FR_*` code indicates a non-failure.
 */
HNEF_BOOL
hnef_fr_good (
	const enum HNEF_FR	code
	)
{
	return HNEF_FR_SUCCESS == code;
}

/*
 * Checks if `code` is an I/O error. May not be called with
 * `HNEF_FR_SUCCESS`.
 */
HNEF_BOOL
hnef_fr_fail_io (
	const enum HNEF_FR	code
	)
{
	assert	(!hnef_fr_good(code));
	switch	(code)
	{
		case	HNEF_FR_FAIL_IO_FILE_CLOSE:
		case	HNEF_FR_FAIL_IO_FILE_OPEN:
		case	HNEF_FR_FAIL_IO_FILE_READ:
		case	HNEF_FR_FAIL_IO_FILE_WRITE:
		case	HNEF_FR_FAIL_IO_READ:
		case	HNEF_FR_FAIL_IO_WRITE:
			return	HNEF_TRUE;
		case	HNEF_FR_FAIL_ALLOC:
		case	HNEF_FR_FAIL_GAME_REPLAY:
		case	HNEF_FR_FAIL_GAME_SAVE_MALF:
		case	HNEF_FR_FAIL_ILL_ARG:
		case	HNEF_FR_FAIL_ILL_STATE:
		case	HNEF_FR_FAIL_MATH:
		case	HNEF_FR_FAIL_NULLPTR:
		case	HNEF_FR_FAIL_OTHER:
			return	HNEF_FALSE;
		case	HNEF_FR_SUCCESS:
		default:
			assert	(HNEF_FALSE);
			return	HNEF_FALSE;
	}
}

