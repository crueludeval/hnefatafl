/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <stdlib.h>	/* malloc, free */

#include "func.h"	/* hnef_fr_good */
#include "listm.h"

/*
 * Grow factor for realloc when the array needs to grow.
 */
/*@unchecked@*/
static
const float	HNEF_LISTM_MEMC_GROWF = 1.5f;

/*
 * Allocates the returned pointer and its elems variable.
 *
 * Upon returning NULL, nothing is allocated because we're out of
 * dynamic memory.
 *
 * Upon returning non-NULL, the listm pointed to by the returned pointer
 * and its elems variable are allocated, and must be freed by
 * free_listm.
 *
 * The returned elems are uninitialized and elemc is 0.
 * Returns NULL if capc is 0.
 */
struct hnef_listm *
hnef_alloc_listm (
	const size_t	capc
	)
{
	struct hnef_listm	* HNEF_RSTR list	= NULL;

	if	(capc < (size_t)1)
	{
		return	NULL;
	}

	list	= malloc(sizeof(* list));
	if	(NULL == list)
	{
		return	NULL;
	}

	list->elems	= malloc(sizeof(* list->elems) * capc);
	if	(NULL == list->elems)
	{
		free	(list);
		list	= NULL;
		return	NULL;
	}

	list->elemc	= 0;
	list->capc	= capc;

	return list;
}

/*
 * Frees everything in l and l itself.
 */
void
hnef_free_listm (
	struct hnef_listm	* const HNEF_RSTR list
	)
{
	if	(NULL != list->elems)
	{
		free	(list->elems);
	}
	free	(list);
}

/*
 * Upon HNEF_FR_FAIL_ALLOC, the old list->elems is still valid and
 * unchanged, and list->elemc and list->capc are unchanged as well.
 */
static
enum HNEF_FR
hnef_listm_grow (
/*@in@*/
/*@notnull@*/
	struct hnef_listm	* const list
	)
/*@modifies * list@*/
{
	struct hnef_move	* elems_new	= NULL;
	size_t capc_new	= (size_t)
		((float)list->capc * HNEF_LISTM_MEMC_GROWF);
	if	(capc_new <= list->capc)
	{
		capc_new = list->capc + 1;
	}

	elems_new	= realloc(list->elems, sizeof(* list->elems)
			* capc_new);
	if	(NULL == elems_new)
	{
/* splint realloc */ /*@i2@*/\
		return	HNEF_FR_FAIL_ALLOC;
	}
	list->elems	= elems_new;
	list->capc	= capc_new;

/* splint realloc */ /*@i1@*/\
	return	HNEF_FR_SUCCESS;
}

/*
 * Grows if needed.
 */
enum HNEF_FR
hnef_listm_add (
	struct hnef_listm	* const list,
	const unsigned short	pos,
	const unsigned short	dest
	)
{
	assert	(NULL != list);
	assert	(NULL != list->elems);

	if	(list->elemc + 1 > list->capc)
	{
		const enum HNEF_FR	fr	= hnef_listm_grow(list);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}
	}
	list->elems[list->elemc].pos	= pos;
	list->elems[list->elemc].dest	= dest;
	list->elemc++;
	return	HNEF_FR_SUCCESS;
}

/*
 * Clears the list from elements, but does not reduce capc.
 */
void
hnef_listm_clear (
	struct hnef_listm	* const HNEF_RSTR list
	)
{
	list->elemc	= 0;
}

/*
 * Moves list->elems[index] to list->elems[0], and moves all elements
 * that were previously at list->elems[0] to list->elems[index - 1] one
 * index forward.
 *
 * Returns HNEF_FR_FAIL_ILL_ARG if index >= list->elemc.
 */
enum HNEF_FR
hnef_listm_swaptofr
	(
	struct hnef_listm	* const HNEF_RSTR list,
	const size_t		index
	)
{
	size_t			i,
				iprev;
	struct hnef_move	mtmp;

	assert	(NULL != list);

	if	(index >= list->elemc)
	{
		return	HNEF_FR_FAIL_ILL_ARG;
	}

	for	(i = (size_t)index; i > 0; --i)
	{
		iprev	= i - 1;

		mtmp.pos		= list->elems[iprev].pos;
		mtmp.dest		= list->elems[iprev].dest;

		list->elems[iprev].pos	= list->elems[i].pos;
		list->elems[iprev].dest	= list->elems[i].dest;

		list->elems[i].pos	= mtmp.pos;
		list->elems[i].dest	= mtmp.dest;
	}
	return	HNEF_FR_SUCCESS;
}

/*
 * Removes the element at index.
 *
 * *	All elements after index in the list are offset by -1 backwards
 *	(so the element that was at index + 1 will now be at index, and
 *	so on).
 *
 * *	list->elemc decreases by -1.
 *
 * Returns HNEF_FR_FAIL_ILL_ARG if index >= list->elemc.
 */
enum HNEF_FR
hnef_listm_remove
	(
	struct hnef_listm	* const HNEF_RSTR list,
	const size_t		index
	)
{
	size_t			i,
				inext;
	struct hnef_move	mtmp;

	assert	(NULL != list);

	if	(index >= list->elemc)
	{
		return	HNEF_FR_FAIL_ILL_ARG;
	}

	for	(i = (size_t)index; i + 1 < list->elemc; ++i)
	{
		inext	= i + 1;

		mtmp.pos		= list->elems[inext].pos;
		mtmp.dest		= list->elems[inext].dest;

		list->elems[inext].pos	= list->elems[i].pos;
		list->elems[inext].dest	= list->elems[i].dest;

		list->elems[i].pos	= mtmp.pos;
		list->elems[i].dest	= mtmp.dest;
	}

	--list->elemc;

	return	HNEF_FR_SUCCESS;
}

