/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_TYPE_PIECE_T_H
#define HNEF_CORE_TYPE_PIECE_T_H

#include "boardt.h"	/* HNEF_BIT_U8 */
#include "boolt.h"	/* HNEF_BOOL */

/*@exposed@*/
struct hnef_type_piece
{

	/*
	 * bit is the piece's (single) bit.
	 *
	 * captures is a piece bitmask of the pieces that this piece is
	 * hostile to. No piece in this bitmask may belong to owner.
	 *
	 * dbl_trap_squares are the squares that this is double-trapped
	 * on. If the piece is not standing on any square in this
	 * bitmask, then double trap does not trigger for it.
	 *
	 * noreturn is a square bitmask of the squares that the piece
	 * can occupy on the start of the game, but not return to once
	 * he has left them.
	 *
	 * occupies is a square bitmask of the squares that the piece
	 * can occupy (normally).
	 *
	 * `ui_bit` is the bit which the piece looks like in the
	 * interface. It has no other function. If it is
	 * `HNEF_BIT_U8_EMPTY`, then `ui_bit` is disabled. See the
	 * `ui_bit_piece` and `ui_bit_square` functions in `ui.c`.
	 */
	HNEF_BIT_U8	bit,
			captures,
			dbl_trap_squares,
			noreturn,
			occupies,
			ui_bit;

	/*
	 * Owning player's index.
	 *
	 * capt_sides is the amount of sides that the piece has to be
	 * surrounded on to be captured.
	 */
	unsigned short	owner,
			capt_sides;

	/*
	 * capt_edge is true if capturing rules should be relaxed along
	 * board edges to make capturing possible, even though it
	 * shouldn't be because there are not enough on-board squares
	 * adjacent to the piece. Requires capt_sides >= 2.
	 *
	 * capt_loss is true if the game is lost for the owning player
	 * if he loses all pieces of this type.
	 *
	 * custodial is true if the piece has to be captured
	 * custodially. This can be relaxed if capt_edge is true.
	 * Requires capt_sides >= 2.
	 *
	 * dbl_trap is whether this piece can trigger double trap. For
	 * this piece to be captured by a double trap, dbl_trap_capt
	 * also has to be true.
	 *
	 * dbl_trap_capt is whether this piece is captured by double
	 * trap. If double trap is triggered by two pieces capable of
	 * triggering it, then this piece is captured.
	 *
	 * dbl_trap_compl is whether this piece is a complement to a
	 * double trap trigger, and requires a non-complement piece to
	 * be trapped with it for the trap to trigger. Two complement
	 * pieces can not trigger a double trap.
	 *
	 * dbl_trap_edge is whether double trap can trigger along the
	 * edges of the board for this piece, id est if the capt_sides
	 * and custodial requirements may be dropped due to off-board
	 * squares. This works similarly to capt_edge.
	 *
	 * dbl_trap_encl is whether a full enclosure is required to
	 * double trap the piece. This effectively sets capt_sides to 4
	 * and custodial to false when determining whether the piece
	 * triggers double trap.
	 *
	 * escape is true if this piece can escape to a type_square with
	 * escape to win the game.
	 */
	HNEF_BOOL	capt_edge,
			capt_loss,
			custodial,
			dbl_trap,
			dbl_trap_capt,
			dbl_trap_compl,
			dbl_trap_edge,
			dbl_trap_encl,
			escape;

};

#endif

