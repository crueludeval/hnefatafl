/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_LINE_H
#define HNEF_CORE_LINE_H

#include "boolt.h"	/* HNEF_BOOL */
#include "config.h"	/* HNEF_RSTR */
#include "funct.h"	/* HNEF_FR */

/*@-protoparamname@*/
/*@in@*/
/*@null@*/
/*@only@*/
char *
hnef_line_read (
/*@in@*/
/*@notnull@*/
	FILE		* const HNEF_RSTR in,
/*@in@*/
/*@null@*/
/*@only@*/
	char		* line,
/*@in@*/
/*@notnull@*/
	size_t		* const HNEF_RSTR mem,
/*@out@*/
/*@notnull@*/
	enum HNEF_FR	* const HNEF_RSTR fr
	)
/*@globals errno, fileSystem, internalState@*/
/*@modifies errno, fileSystem, internalState@*/
/*@modifies * in, * line, * mem, * fr@*/
;
/*@=protoparamname@*/

/*@in@*/
/*@null@*/
/*@only@*/
extern
char *
hnef_line_alloc_cpy (
/*@in@*/
/*@notnull@*/
	const char	* const HNEF_RSTR
	)
/*@*/
;

extern
HNEF_BOOL
hnef_line_empty (
/*@in@*/
/*@notnull@*/
	const char	* const HNEF_RSTR
	)
/*@*/
;

#endif

