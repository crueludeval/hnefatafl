/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <stdlib.h>	/* malloc, free */

#include "func.h"	/* hnef_fr_good */
#include "listmh.h"

/*
 * Grow factor for realloc when the array needs to grow.
 */
/*@unchecked@*/
static
const float	HNEF_LISTMH_MEMC_GROWF = 1.5f;

/*
 * Allocates the returned pointer and its elems variable.
 *
 * Upon returning NULL, nothing is allocated because we're out of
 * dynamic memory.
 *
 * Upon returning non-NULL, the listm pointed to by the returned pointer
 * and its elems variable are allocated, and must be freed by
 * free_listm.
 *
 * The returned elems are uninitialized and elemc is 0.
 * Returns NULL if capc is 0.
 */
struct hnef_listmh *
hnef_alloc_listmh (
	const size_t capc
	)
{
	struct hnef_listmh	* HNEF_RSTR list	= NULL;

	if	(capc < (size_t)1)
	{
		return	NULL;
	}

	list	= malloc(sizeof(* list));
	if	(NULL == list)
	{
		return	NULL;
	}

	list->elems	= malloc(sizeof(* list->elems) * capc);
	if (NULL == list->elems)
	{
		free	(list);
		return	NULL;
	}

	list->elemc	= 0;
	list->capc	= capc;

	return	list;
}

/*
 * Frees everything in list and list itself.
 */
void
hnef_free_listmh (
	struct hnef_listmh	* const HNEF_RSTR list
	)
{
	if	(NULL != list->elems)
	{
		free	(list->elems);
	}
	free	(list);
}

/*
 * Upon HNEF_FR_FAIL_ALLOC, the old list->elems is still valid and
 * unchanged, and list->elemc and list->capc are unchanged as well.
 */
static
enum HNEF_FR
hnef_listmh_grow (
/*@in@*/
/*@notnull@*/
	struct hnef_listmh	* const list,
	size_t			capc_new
	)
/*@modifies * list@*/
{
	struct hnef_moveh	* elems_new	= NULL;
	if	(capc_new <= list->capc)
	{
		capc_new = list->capc + 1;
	}

	elems_new	= realloc(list->elems, sizeof(* list->elems)
			* capc_new);
	if	(NULL == elems_new)
	{
/* splint realloc */ /*@i2@*/\
		return HNEF_FR_FAIL_ALLOC;
	}
	list->elems	= elems_new;
	list->capc	= capc_new;

/* splint realloc */ /*@i1@*/\
	return	HNEF_FR_SUCCESS;
}

/*
 * Grows if needed.
 */
enum HNEF_FR
hnef_listmh_add (
	struct hnef_listmh	* const list,
	const unsigned short	pos,
	const unsigned short	dest,
	const HNEF_BOOL		irrev
	)
{
	assert	(NULL != list);
	assert	(NULL != list->elems);

	if	(list->elemc + 1 > list->capc)
	{
		const enum HNEF_FR fr = hnef_listmh_grow(list, (size_t)
			((float)list->capc * HNEF_LISTMH_MEMC_GROWF));
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}
	}
	list->elems[list->elemc].irrev		= irrev;
	list->elems[list->elemc].pos		= pos;
	list->elems[list->elemc].dest		= dest;
	list->elemc++;
	return	HNEF_FR_SUCCESS;
}

/*
 * Copies src into dest, possibly growing dest if dest->capc isn't large
 * enough to fit all elements (elemc) in src.
 *
 * Does not copy the garbage elements that are beyond src->elemc but
 * within src->capc, and does not allocate space for them.
 */
enum HNEF_FR
hnef_listmh_copy (
	const struct hnef_listmh	* const	HNEF_RSTR src,
	struct hnef_listmh		* const	HNEF_RSTR dest
	)
{
	size_t	i;
	if	(src->elemc > dest->capc)
	{
		const enum HNEF_FR fr	= hnef_listmh_grow
					(dest, src->elemc);
		if	(!hnef_fr_good(fr))
		{
			return	fr;
		}
	}
	dest->elemc	= src->elemc;
	for	(i = (size_t)0; i < src->elemc; ++i)
	{
		dest->elems[i].pos	= src->elems[i].pos;
		dest->elems[i].dest	= src->elems[i].dest;
		dest->elems[i].irrev	= src->elems[i].irrev;
	}
	return	HNEF_FR_SUCCESS;
}

