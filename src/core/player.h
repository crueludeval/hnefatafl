/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_PLAYER_H
#define HNEF_CORE_PLAYER_H

#include "boolt.h"	/* HNEF_BOOL */
#include "config.h"	/* HNEF_RSTR */
#include "funct.h"	/* HNEF_FR */
#include "gamet.h"	/* hnef_game */
#include "playert.h"	/* hnef_player */

/*@-protoparamname@*/
extern
void
hnef_player_initopt (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	struct hnef_player	* const HNEF_RSTR player
	)
/*@modifies * player@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
void
hnef_player_clear (
/*@in@*/
/*@notnull@*/
	struct hnef_player	* const HNEF_RSTR player
	)
/*@modifies * player@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
HNEF_BOOL
hnef_player_init (
	struct hnef_player	* const HNEF_RSTR player,
	const unsigned short	index
	)
/*@modifies * player@*/
;
/*@=protoparamname@*/

/*@unused@*/
extern
HNEF_BOOL
hnef_player_index_valid (
	const unsigned short
	)
/*@modifies nothing@*/
;

/*@null@*/
/*@only@*/
/*@partial@*/
extern
struct hnef_player *
hnef_alloc_player (void)
/*@modifies nothing@*/
;

/*@-protoparamname@*/
extern
void
hnef_free_player (
/*@notnull@*/
/*@owned@*/
/*@special@*/
	struct hnef_player	* const player
	)
/*@modifies player@*/
/*@releases player@*/
;
/*@=protoparamname@*/

#endif

