/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_LISTM_T_H
#define HNEF_CORE_LISTM_T_H

#include "movet.h"	/* hnef_move */

/*@concrete@*/
struct hnef_listm
{

	/*
	 * An array of moves that contains elemc move structs.
	 *
	 * You can access elems[0] to elems[elemc - 1]. Accessing other
	 * elements may be out of array bounds, point to garbage, point
	 * to an old removed value, or other behavior.
	 */
/*@notnull@*/
/*@owned@*/
	struct hnef_move	* elems;

	/*
	 * Length of elems. Don't change.
	 *
	 * capc is the amount of move structs allocated. This may exceed
	 * elemc. Don't change or use.
	 */
	size_t		elemc,
			capc;

};

#endif

