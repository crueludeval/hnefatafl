/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_FUNC_T_H
#define HNEF_CORE_FUNC_T_H

/*
 * Fail detail for invalid rulesets (always non-fatal).
 *
 * This is why the a ruleset (after parsing) is not valid.
 */
enum HNEF_RVALID
{

	/*
	 * Uninitialized (or non-failure) value.
	 */
	HNEF_RVALID_SUCCESS	= 0,

	/*
	 * Found a bitmask where a single bit or 0 was expected.
	 */
	HNEF_RVALID_FAIL_BITMASK,

	/*
	 * Invalid board size.
	 */
	HNEF_RVALID_FAIL_BSIZE,

	/*
	 * Some pieces owned by a player has dbl_trap, but no piece
	 * owned by the same player has dbl_trap_capt.
	 */
	HNEF_RVALID_FAIL_DTRAP_NOCAPT,

	/*
	 * Some piece owned by a player has dbl_trap_compl, but no piece
	 * owned by the same player is without dbl_trap_compl.
	 */
	HNEF_RVALID_FAIL_DTRAP_ONLYCOMPL,

	/*
	 * `dbl_trap` without `dbl_trap_squares`.
	 */
	HNEF_RVALID_FAIL_DTRAP_NOSQUARES,

	/*
	 * The piece has some `dbl_trap_*` setting, but not `dbl_trap`.
	 */
	HNEF_RVALID_FAIL_DTRAP_INCONSISTENT,

	/*
	 * A piece xor square has escape.
	 */
	HNEF_RVALID_FAIL_ESCAPE_NO_PIECE_XOR_SQUARE,

	/*
	 * Ruleset id is missing or 0-length.
	 */
	HNEF_RVALID_FAIL_ID,

	/*
	 * Ruleset name is missing.
	 */
	HNEF_RVALID_FAIL_NAME,

	/*
	 * `pieces` array is `NULL`, i.e. has not been declared.
	 */
	HNEF_RVALID_FAIL_NULL_PIECES,

	/*
	 * `squares` array is `NULL`, i.e. has not been declared.
	 */
	HNEF_RVALID_FAIL_NULL_SQUARES,

	/*
	 * A player has no pieces on the board.
	 */
	HNEF_RVALID_FAIL_PIECE_BOARD_NONE,

	/*
	 * A piece captures a friendly piece.
	 */
	HNEF_RVALID_FAIL_PIECE_CAPTOWN,

	/*
	 * capt_sides < 2 and custodial.
	 */
	HNEF_RVALID_FAIL_PIECE_CAPTSIDES_CUST,

	/*
	 * capt_sides < 2 and capt_edge.
	 */
	HNEF_RVALID_FAIL_PIECE_CAPTSIDES_EDGE,

	/*
	 * Some piece starts on escape square.
	 */
	HNEF_RVALID_FAIL_PIECE_ESCAPESTART,

	/*
	 * A piece type references (by `noreturn` or similar) a square
	 * which it can not occupy.
	 */
	HNEF_RVALID_FAIL_PIECE_REFINOCCUP,

	/*
	 * Some piece starts on a square which it can not occupy.
	 */
	HNEF_RVALID_FAIL_PIECE_STARTNOOCCUP,

	/*
	 * Invalid owner index.
	 */
	HNEF_RVALID_FAIL_PIECE_OWNER,

	/*
	 * Invalid capt_sides for some piece.
	 */
	HNEF_RVALID_FAIL_PIECE_CAPTSIDES,

	/*
	 * Reference to undefined piece type.
	 */
	HNEF_RVALID_FAIL_PIECE_UNDEF,

	/*
	 * A piece is defined but doesn't appear in `pieces`.
	 */
	HNEF_RVALID_FAIL_PIECE_UNUSED,

	/*
	 * Invalid capt_sides for some square.
	 */
	HNEF_RVALID_FAIL_SQUARE_CAPTSIDES,

	/*
	 * Overrides capt_sides for a piece, but the piece can't occupy
	 * the square.
	 */
	HNEF_RVALID_FAIL_SQUARE_CAPTSIDES_NOOCCUP,

	/*
	 * capt_sides defined but capt_sides_pieces does not indicate
	 * any piece.
	 */
	HNEF_RVALID_FAIL_SQUARE_CAPTSIDES_NOPIECE,

	/*
	 * Like `HNEF_RVALID_FAIL_PIECE_UNDEF` but for squares.
	 */
	HNEF_RVALID_FAIL_SQUARE_UNDEF,

	/*
	 * Like `HNEF_RVALID_FAIL_PIECE_UNUSED` but for squares.
	 */
	HNEF_RVALID_FAIL_SQUARE_UNUSED

};

/*
 * Fail detail for malformed ruleset files (always non-fatal).
 *
 * This is why the a ruleset file can not be parsed.
 */
enum HNEF_RREAD
{

	/*
	 * Success.
	 */
	HNEF_RREAD_SUCCESS	= 0,

	/*
	 * Some argument was (apparently) encountered twice. The ruleset
	 * reader may be able to figure this out by determining that a
	 * value has already been set, when it expects the value to not
	 * have been set.
	 */
	HNEF_RREAD_FAIL_ARG_DUP,

	/*
	 * Encountered more arguments than expected.
	 */
	HNEF_RREAD_FAIL_ARG_EXCESS,

	/*
	 * Some argument is missing.
	 */
	HNEF_RREAD_FAIL_ARG_MISS,

	/*
	 * Some argument is not recognized.
	 *
	 * This will probably happen a lot when people are translating
	 * "lang.c" but forget to translate the entries in the ruleset
	 * files.
	 */
	HNEF_RREAD_FAIL_ARG_UNK,

	/*
	 * When reading "width" or "height", "pieces" or "squares" has
	 * already been read.
	 */
	HNEF_RREAD_FAIL_ARRB_PRESIZE,

	/*
	 * Encountered "pieces" or "squares", but it contains too many
	 * or too few tokens (it doesn't match width * height).
	 */
	HNEF_RREAD_FAIL_ARRB_SIZE,

	/*
	 * Some number was not a single bit.
	 */
	HNEF_RREAD_FAIL_BIT_SINGLE,

	/*
	 * Some number was out of bounds.
	 */
	HNEF_RREAD_FAIL_NOOB,

	/*
	 * Board size is unknown when `pieces` or `squares` arrays are
	 * encountered.
	 */
	HNEF_RREAD_FAIL_NOPREL_BSIZE,

	/*
	 * Some string could not be converted to a number.
	 */
	HNEF_RREAD_FAIL_STRTOL,

	/*
	 * A type_piece or type_square has been defined twice.
	 */
	HNEF_RREAD_FAIL_TYPE_DUP,

	/*
	 * Wrong declaration order.
	 *
	 * This happens if type_piece or type_square are not declared in
	 * the right order (from 1 to 128).
	 */
	HNEF_RREAD_FAIL_TYPE_ORDER,

	/*
	 * Too many type_pieces or type_squares defined.
	 */
	HNEF_RREAD_FAIL_TYPE_OVERFLOW,

	/*
	 * A type_piece or type_square has not been defined before being
	 * referred to.
	 */
	HNEF_RREAD_FAIL_TYPE_UNDEF

};

/*
 * Generic function return (FR) code.
 *
 * This may be accompanied by more information, specifically the enums
 * in this file (the reason is because otherwise this enum would be huge
 * and the switch statement would be too big):
 *
 * `HNEF_FAIL_*`
 *
 * Whether it's fatal for the program depends on the type of error.
 */
enum HNEF_FR
{

	/*
	 * No error. You can check
	 * "if (HNEF_FR_SUCCESS != return_value)" to determine if a
	 * function failed (or `hnef_fr_good()`).
	 */
	HNEF_FR_SUCCESS	= 0,

	/*
	 * Any dynamic memory allocation (malloc, calloc, realloc)
	 * failed.
	 *
	 * If a function returns an allocated value, which will be NULL
	 * upon failure to allocate, and also returns an HNEF_FR value,
	 * then that value sould be HNEF_FR_FAIL_ALLOC even though NULL
	 * means failed allocation. This only applies if that function
	 * happens to return an HNEF_FR value as well -- otherwise it's
	 * fine to just return NULL like malloc.
	 */
	HNEF_FR_FAIL_ALLOC,

	/*
	 * Failed to replay a game from move history (either loaded from
	 * a previously saved game or by undoing moves -- in the latter
	 * case, it implies a bug in the program since undone moves must
	 * be replayable).
	 */
	HNEF_FR_FAIL_GAME_REPLAY,

	/*
	 * Malformed save file -- couldn't even be parsed as a save, let
	 * alone replayed. This is never fatal.
	 */
	HNEF_FR_FAIL_GAME_SAVE_MALF,

	/*
	 * Argument passed to function is illegal somehow.
	 */
	HNEF_FR_FAIL_ILL_ARG,

	/*
	 * Program's state is invalid in some way.
	 */
	HNEF_FR_FAIL_ILL_STATE,

	/*
	 * Failed to close a file after opening it.
	 */
	HNEF_FR_FAIL_IO_FILE_CLOSE,

	/*
	 * Failed to open file.
	 */
	HNEF_FR_FAIL_IO_FILE_OPEN,

	/*
	 * Failed to read from a file that has been successfully opened.
	 */
	HNEF_FR_FAIL_IO_FILE_READ,

	/*
	 * Failed to write to a file that has been successfully opened.
	 */
	HNEF_FR_FAIL_IO_FILE_WRITE,

	/*
	 * Failed to read from `stdin`.
	 */
	HNEF_FR_FAIL_IO_READ,

	/*
	 * Failed to write to `stderr`, `stdout` or a `char *` buffer.
	 */
	HNEF_FR_FAIL_IO_WRITE,

	/*
	 * Something wrong with some equation or math function, like
	 * division by 0 or having errno set after a `math.h` function
	 * call.
	 */
	HNEF_FR_FAIL_MATH,

	/*
	 * NULL encountered when not allowed.
	 */
	HNEF_FR_FAIL_NULLPTR,

	/*
	 * Some other failure. Implementing programs can use this value
	 * when a `HNEF_FR` return value is required but no other
	 * defined enum member fits.
	 */
	HNEF_FR_FAIL_OTHER

};

#endif

