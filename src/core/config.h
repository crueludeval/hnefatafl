/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_CONFIG_H
#define HNEF_CORE_CONFIG_H

#include <stddef.h>	/* size_t */
#include <stdio.h>	/* FILE */

/*
 * `restrict` was added in C99.
 */
#if	defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#define	HNEF_RSTR restrict
#else
#define	HNEF_RSTR
#endif

#include "boolt.h"	/* HNEF_BOOL */

/*@unchecked@*/
/*@unused@*/
extern
const long	HNEFATAFL_CORE_VERSION;

/*@in@*/
/*@notnull@*/
/*@observer@*/
/*@unused@*/
extern
const char *
hnef_core_id (void)
/*@modifies nothing@*/
;

/*@unused@*/
extern
size_t
hnef_core_license_arrc (void)
/*@*/
;

/*@in@*/
/*@notnull@*/
/*@observer@*/
/*@unused@*/
extern
const char * const *
hnef_core_license_arrv (void)
/*@*/
;

/*@-protoparamname@*/
/*@unused@*/
extern
HNEF_BOOL
hnef_core_license_print (
/*@in@*/
/*@notnull@*/
	FILE	* const HNEF_RSTR out
	)
/*@globals fileSystem@*/
/*@modifies fileSystem, * out@*/
;
/*@=protoparamname@*/

#endif

