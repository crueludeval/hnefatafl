/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_GAMEUTIL_H
#define HNEF_CORE_GAMEUTIL_H

#include <stdio.h>	/* FILE */

#include "boardt.h"	/* HNEF_BIT_U8 */
#include "config.h"	/* HNEF_RSTR */
#include "funct.h"	/* HNEF_FR */
#include "gamet.h"	/* hnef_game */

/*@unused@*/
extern
HNEF_BIT_U8
hnef_game_uibit_piece (
/*@notnull@*/
	const struct hnef_type_piece	* const HNEF_RSTR
	)
/*@modifies nothing@*/
;

/*@unused@*/
extern
HNEF_BIT_U8
hnef_game_uibit_square (
/*@notnull@*/
	const struct hnef_type_square	* const HNEF_RSTR
	)
/*@modifies nothing@*/
;

/*@-protoparamname@*/
/*@unused@*/
extern
enum HNEF_FR
hnef_game_save_file (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const g,
/*@in@*/
/*@notnull@*/
	FILE			* const file
	)
/*@globals fileSystem@*/
/*@modifies fileSystem, * g, file@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
extern
enum HNEF_FR
hnef_game_save (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const g,
/*@in@*/
/*@notnull@*/
	const char		* const filename
	)
/*@globals errno, fileSystem@*/
/*@modifies errno, fileSystem, * g@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
extern
enum HNEF_FR
hnef_game_load (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const g,
/*@in@*/
/*@notnull@*/
	const char		* const filename,
/*@out@*/
/*@notnull@*/
	HNEF_BOOL		* const id_good
	)
/*@globals errno, fileSystem, internalState@*/
/*@modifies errno, fileSystem, internalState, * g, id_good@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
extern
enum HNEF_FR
hnef_game_undo (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const g,
/*@out@*/
/*@notnull@*/
	HNEF_BOOL		* const success,
	const size_t		undoc
	)
/*@modifies * g, * success@*/
;
/*@=protoparamname@*/

#endif

