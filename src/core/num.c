/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <errno.h>	/* errno */
#include <limits.h>	/* *_MAX */
#include <stdlib.h>	/* strtoul */

#include "line.h"	/* hnef_line_empty */
#include "num.h"
#include "numt.h"	/* SIZET_MAX */

int
hnef_max_int (
	const int	num1,
	const int	num2
	)
{
	return	num1 > num2
		? num1
		: num2;
}

int
hnef_min_int (
	const int	num1,
	const int	num2
	)
{
	return	num1 > num2
		? num2
		: num1;
}

unsigned short
hnef_max_ushrt (
	const unsigned short	num1,
	const unsigned short	num2
	)
{
	return	num1 > num2
		? num1
		: num2;
}

unsigned short
hnef_min_ushrt (
	const unsigned short	num1,
	const unsigned short	num2
	)
{
	return	num1 > num2
		? num2
		: num1;
}

/*
 * Returns HNEF_TRUE if bitmask is a single bit, and not 0.
 */
HNEF_BOOL
hnef_single_bit (
	const unsigned int	bitmask
	)
{
	return	0 != bitmask
	&&	0 == (bitmask & (bitmask - 1));
}

/*
 * Sets num to INT_MIN upon failure.
 */
HNEF_BOOL
hnef_texttoint (
	const char	* const HNEF_RSTR str_num,
	int		* const HNEF_RSTR num
	)
{
	char *	str_end;
	long	n;
	int	errno_old	= errno;
		errno		= 0;

	assert	(NULL != str_num);
	assert	(NULL != num);

	n	= strtol(str_num, & str_end, 10);
	if	(0 != errno
	||	!hnef_line_empty(str_end)
	||	LONG_MAX == n
	||	LONG_MIN == n
	||	n > (long)INT_MAX
	||	n < (long)INT_MIN)
	{
		errno	= errno_old;
		* num	= INT_MIN;
		return	HNEF_FALSE;
	}
	* num	= (int)n;
	return	HNEF_TRUE;
}

/*
 * Sets num to USHRT_MAX upon failure.
 */
HNEF_BOOL
hnef_texttoushort (
	const char	* const HNEF_RSTR str_num,
	unsigned short	* const HNEF_RSTR num
	)
{
	char *		str_end;
	unsigned long	n;
	int		errno_old	= errno;
			errno		= 0;

	assert	(NULL != str_num);
	assert	(NULL != num);

	n	= strtoul(str_num, & str_end, 10);
	if	(0 != errno
	||	!hnef_line_empty(str_end)
	||	ULONG_MAX == n
	||	n > (unsigned long)USHRT_MAX)
	{
		errno	= errno_old;
		* num	= USHRT_MAX;
		return	HNEF_FALSE;
	}
	* num	= (unsigned short)n;
	return	HNEF_TRUE;
}

/*
 * Sets num to SIZET_MAX upon failure.
 */
HNEF_BOOL
hnef_texttosize (
	const char	* const HNEF_RSTR str_num,
	size_t		* const HNEF_RSTR num
	)
{
	char *		str_end;
	unsigned long	n;
	int		errno_old	= errno;
			errno		= 0;

	assert	(NULL != str_num);
	assert	(NULL != num);

	n	= strtoul(str_num, & str_end, 10);
	if	(0 != errno
	||	!hnef_line_empty(str_end)
	||	ULONG_MAX == n)
	{
		errno	= errno_old;
		* num	= SIZET_MAX;
		return	HNEF_FALSE;
	}
	* num	= (size_t)n;
	return	HNEF_TRUE;
}

