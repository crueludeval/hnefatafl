/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_LISTM_H
#define HNEF_CORE_LISTM_H

#include "config.h"	/* HNEF_RSTR */
#include "funct.h"	/* HNEF_FR */
#include "listmt.h"	/* hnef_listm */

/*@null@*/
/*@only@*/
/*@partial@*/
/*@unused@*/
extern
struct hnef_listm *
hnef_alloc_listm (
	const size_t
	)
/*@modifies nothing@*/
;

/*@-protoparamname@*/
/*@unused@*/
extern
void
hnef_free_listm (
/*@notnull@*/
/*@owned@*/
/*@special@*/
	struct hnef_listm	* const HNEF_RSTR list
	)
/*@modifies list->elems, list@*/
/*@releases list->elems, list@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_listm_add (
/*@notnull@*/
/*@partial@*/
	struct hnef_listm	* const list,
	const unsigned short	pos,
	const unsigned short	dest
	)
/*@modifies * list@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
extern
void
hnef_listm_clear (
/*@notnull@*/
	struct hnef_listm	* const HNEF_RSTR list
	)
/*@modifies * list@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
extern
enum HNEF_FR
hnef_listm_swaptofr (
	struct hnef_listm	* const HNEF_RSTR list,
	const size_t		index
	)
/*@modifies * list@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
extern
enum HNEF_FR
hnef_listm_remove (
	struct hnef_listm	* const HNEF_RSTR list,
	const size_t		index
	)
/*@modifies * list@*/
;
/*@=protoparamname@*/

#endif

