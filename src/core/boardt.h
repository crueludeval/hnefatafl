/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_BOARD_T_H
#define HNEF_CORE_BOARD_T_H

/*
 * 8 bit integer data types for single bits or bitmasks.
 *
 * A bit is a piece or square. For every single HNEF_BIT_U8 there is a
 * corresponding piece or square type. Thus there can be at most eight
 * piece or square types in any ruleset.
 *
 * This should be enforced in case unsigned char is greater than 1 byte.
 */
typedef unsigned char	HNEF_BIT_U8;

/*
 * Value of a nonexistent bit. This can also refer to "no bits", id est
 * decimal 0 (or binary 00000000).
 */
/*@unchecked@*/
extern
const HNEF_BIT_U8	HNEF_BIT_U8_EMPTY;

/*@exposed@*/
struct hnef_board
{

	/*
	 * Array of HNEF_BIT_U8. This works like the HNEF_BIT_U8 array
	 * in ruleset and, in fact, is just a copy of it initially
	 * (before the game begins).
	 */
/*@notnull@*/
/*@owned@*/
	HNEF_BIT_U8	* pieces;

	/*
	 * Current player's index in game->players array.
	 */
	unsigned short	turn;

	/*
	 * Stores last `capt_loss` piece position found in
	 * `board_game_over_captloss`.
	 */
	unsigned short	opt_captloss_piece_lastseen;

};

#endif

