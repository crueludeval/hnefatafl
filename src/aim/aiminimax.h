/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_AIM_MINIMAX_H
#define HNEF_AIM_MINIMAX_H

#include "aiminimaxt.h"	/* hnef_aiminimax */

/*@unused@*/
extern
HNEF_BOOL
hnef_aiminimax_depth_max_valid (
	const unsigned short
	)
/*@modifies nothing@*/
;

/*@-protoparamname@*/
/*@null@*/
/*@only@*/
/*@partial@*/
/*@unused@*/
extern
struct hnef_aiminimax *
hnef_alloc_aiminimax (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const game,
	const unsigned short	p_index,
	const unsigned short	depth_max,
/*@in@*/
/*@notnull@*/
	enum HNEF_FR		* const HNEF_RSTR fr
#ifdef	HNEFATAFL_AIM_ZHASH
	,
/*@dependent@*/
/*@in@*/
/*@null@*/
	struct hnef_zhashtable	* const old_tab,
	const size_t		mem_tab,
	const size_t		mem_col
#endif	/* HNEFATAFL_AIM_ZHASH */
	)
#ifdef	HNEFATAFL_AIM_ZHASH
/*@globals errno, internalState@*/
#else	/* HNEFATAFL_AIM_ZHASH */
/*@globals internalState@*/
#endif	/* HNEFATAFL_AIM_ZHASH */
/*@modifies * fr@*/
#ifdef	HNEFATAFL_AIM_ZHASH
/*@modifies errno, internalState@*/
#endif	/* HNEFATAFL_AIM_ZHASH */
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
extern
void
hnef_free_aiminimax (
/*@notnull@*/
/*@owned@*/
/*@special@*/
	struct hnef_aiminimax	* const aim
	)
/*@modifies aim@*/
/*@releases aim@*/
;
/*@=protoparamname@*/

#endif

