#
# Hnefatafl 140117
# Vimose game, as given in Reconstructing Hnefatafl by Damian Walker
#
# Rule set by Damian Walker
# Created: 24-Jan-2014
#

# ID
id		recon_vimose_v1_2
name		Vimose Hnefatafl
ui_theme	20

# Board dimensions
width	19
height	19

# Piece layout at the start of the game
pieces \
0 0 0 2 2 2 2 2 2 2 2 2 2 2 2 2 0 0 0 \
0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 \
0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 \
2 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 2 \
2 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 2 \
2 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 2 \
2 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 2 \
2 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 2 \
2 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 2 \
2 2 1 1 1 1 1 1 1 4 1 1 1 1 1 1 1 2 2 \
2 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 2 \
2 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 2 \
2 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 2 \
2 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 2 \
2 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 2 \
2 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 2 \
0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 \
0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 \
0 0 0 2 2 2 2 2 2 2 2 2 2 2 2 2 0 0 0

# Board layout at the start of the game
squares \
2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 \
2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 \
2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 \
2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 \
2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 \
2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 \
2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 \
2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 \
2 1 1 1 1 1 1 1 1 8 1 1 1 1 1 1 1 1 2 \
2 1 1 1 1 1 1 1 8 4 8 1 1 1 1 1 1 1 2 \
2 1 1 1 1 1 1 1 1 8 1 1 1 1 1 1 1 1 2 \
2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 \
2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 \
2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 \
2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 \
2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 \
2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 \
2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 \
2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2

# Rules against drawishness
forbid_repeat

#
# Capabilities of the pieces
#

# Defender
piece bit 1
piece 1 captures 2
piece 1 custodial
piece 1 dbl_trap
piece 1 dbl_trap_capt
piece 1 dbl_trap_compl
piece 1 dbl_trap_squares 8
piece 1 occupies 11
piece 1 owner 1

# Attacker
piece bit 2
piece 2 captures 5
piece 2 custodial
piece 2 occupies 11
piece 2 owner 0

# King
piece bit 4
piece 4 capt_loss
piece 4 capt_sides 2
piece 4 captures 2
piece 4 dbl_trap
piece 4 dbl_trap_squares 4
piece 4 escape
piece 4 noreturn 4
piece 4 occupies 15
piece 4 owner 1

#
# Properties of the board squares
#

# Ordinary
square bit 1

# Corner
square bit 2
square 2 escape
square 2 ui_bit 1

# Castle
square bit 4
square 4 captures 7
square 4 capt_sides 4
square 4 capt_sides_pieces 4

# Beside the castle
square bit 8
square 8 ui_bit 1
square 8 capt_sides 4
square 8 capt_sides_pieces 4
